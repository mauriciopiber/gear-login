-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: gear-login-mysql    Database: gear_login
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl_role`
--

DROP TABLE IF EXISTS `acl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_role`
--

LOCK TABLES `acl_role` WRITE;
/*!40000 ALTER TABLE `acl_role` DISABLE KEYS */;
INSERT INTO `acl_role` VALUES (1,'master',NULL),(2,'member',NULL),(3,'admin',NULL);
/*!40000 ALTER TABLE `acl_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_user_role`
--

DROP TABLE IF EXISTS `acl_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_user_role`
--

LOCK TABLES `acl_user_role` WRITE;
/*!40000 ALTER TABLE `acl_user_role` DISABLE KEYS */;
INSERT INTO `acl_user_role` VALUES (4,1),(19,3),(27,1),(28,1),(29,1),(30,1),(31,2);
/*!40000 ALTER TABLE `acl_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `id_action` int(11) NOT NULL AUTO_INCREMENT,
  `id_controller` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_action`),
  KEY `id_controller` (`id_controller`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `action_ibfk_1` FOREIGN KEY (`id_controller`) REFERENCES `controller` (`id_controller`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `action_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `action_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controller`
--

DROP TABLE IF EXISTS `controller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controller` (
  `id_controller` int(11) NOT NULL AUTO_INCREMENT,
  `id_module` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `invokable` varchar(150) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_controller`),
  KEY `id_module` (`id_module`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `controller_ibfk_1` FOREIGN KEY (`id_module`) REFERENCES `module` (`id_module`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `controller_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `controller_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controller`
--

LOCK TABLES `controller` WRITE;
/*!40000 ALTER TABLE `controller` DISABLE KEYS */;
/*!40000 ALTER TABLE `controller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `remetente` varchar(255) NOT NULL,
  `destino` varchar(255) NOT NULL,
  `assunto` varchar(255) NOT NULL,
  `mensagem` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_email`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `email_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `email_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (20190118110101,'CreateUserControl','2019-01-18 12:18:50','2019-01-18 12:18:50',0);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `id_module` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_module`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `module_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `module_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0187947693537d388ff509df642d365fcb1de604','pibernetwork','2','2018-11-20 13:33:55','openid profile email offline_access'),('03f763a03187ab3a93d6a02f644ee287a9af6951','pibernetwork','1','2018-11-14 19:38:00','openid profile email offline_access'),('05a862018566313d58e80cd39dc969b194c02963','pibernetwork','2','2018-11-23 02:44:57','openid profile email offline_access'),('079b9a77acfad9616f5258884ecc28a85758b221','pibernetwork',NULL,'2018-11-12 20:46:54',NULL),('09a8bceb8be27452e709c07b6562ffaaf4afaaae','pibernetwork','2','2018-11-30 12:22:26','openid profile email offline_access'),('09ee007d5bf569f892fc7554a067d4b18f8e2775','pibernetwork','2','2018-11-20 18:02:03','openid profile email offline_access'),('0fa3e5fbaf334eed6a83ba34b8af4386af245366','pibernetwork','2','2018-11-23 16:27:41','openid profile email offline_access'),('0ff1771f70b1fd036004e97e4a3fdf2de0c91962','pibernetwork','2','2018-11-28 19:21:14','openid profile email offline_access'),('1233b03f587cbff4cd2e5eb95307f2d7226aa138','pibernetwork','2','2018-11-24 10:52:12','openid profile email offline_access'),('133b800b7aed3353f5bca8dd0250ea4070e734ca','pibernetwork','2','2018-11-19 17:45:27','openid profile email offline_access'),('1641cf81dffb6a5167a5d437de6de82639d175d8','pibernetwork','2','2018-11-29 22:41:30','openid profile email offline_access'),('1f0a1a5edeae6b5ae69ebf10d05038b27523cff8','pibernetwork','mauriciopiber','2033-06-26 20:17:42',NULL),('1f43c8ff2f436e62fadeb7ec6d45c9c1355b70ae','pibernetwork','2','2018-11-17 23:09:59','openid profile email offline_access'),('27aa96280746c278308495447ad41051c9294daf','pibernetwork','2','2018-11-19 00:15:33','openid profile email offline_access'),('2a6c3f492747106d16b82408aef18ec4c470208a','pibernetwork','2','2018-12-03 17:32:26','openid profile email offline_access'),('2d687dc43ad693b00bc70a4c4613ece7a6f17186','pibernetwork','2','2018-11-20 11:39:39','openid profile email offline_access'),('321c4b1234322fb327cf0aafaa264c66650e7e32','pibernetwork','2','2018-11-18 12:09:35','openid profile email offline_access'),('32350103b95c0ef9958a4f6defcc1f50ae331d62','pibernetwork','2','2018-11-18 12:11:40','openid profile email offline_access'),('3622432a593f7ddfafe68dac9d794c4d1d53b041','pibernetwork','mauriciopiber','2018-11-14 18:05:36','openid profile email offline_access'),('37df10c086c7d4acfe2beb47192d60bffc10bd9b','pibernetwork',NULL,'2018-11-12 20:45:47',NULL),('3a42d8882692208a00aa52eff06297e68e3b9aa1','pibernetwork','2','2018-11-16 23:20:28','openid profile email offline_access'),('3ab88e89c8b93b94617d06e9d4dc9380d63af1e2','pibernetwork','mauriciopiber','2033-06-28 17:37:04',NULL),('405bb709d3217b78e293f15cce903a284fa395ba','pibernetwork','2','2018-11-16 23:10:40','openid profile email offline_access'),('409af074f6176dad4b6d015e78aa54d540d39b12','pibernetwork',NULL,'2018-11-12 19:40:39',NULL),('41c01c4d523a5076661e4922c16f9496b2757487','pibernetwork','2','2018-11-18 12:12:00','openid profile email offline_access'),('42b0ccd4c54ee01b039f7693f41404b43b01e521','pibernetwork','2','2018-12-05 21:08:46','openid profile email offline_access'),('43da09105983e346b517711660ea40fa2a60d217','testclient','mauriciopiber','2018-11-12 21:10:05',NULL),('49d9d1384212fe868d158a57857f1d2410f52291','pibernetwork','2','2018-11-16 23:11:13','openid profile email offline_access'),('4e6ba06d37ae080f46785e29ee4b025dc31650f2','pibernetwork','2','2018-12-05 20:07:50','openid profile email offline_access'),('4fe09ca1b4fc97e2a77bdadce7e0c32b9d46160e','pibernetwork','mauriciopiber','2033-06-26 20:18:23',NULL),('53a1ee1390f98dc34a6c2b6d009bcf4a2cd42268','pibernetwork','mauriciopiber','2033-06-26 20:13:26',NULL),('54c394e8458b8c3c15a75c6dfc19bdace96492e5','pibernetwork','2','2018-11-19 00:15:03','openid profile email offline_access'),('572eaa0d5935f3a7987aaaba8bebd789d8934396','pibernetwork','mauriciopiber','2033-06-26 20:15:58',NULL),('601a954eee0fa3833027758cad26660c3fae6af3','pibernetwork','2','2018-12-03 21:19:57','openid profile email offline_access'),('62787da3ab8815eeaa48db44dc303e64d2c876a6','pibernetwork',NULL,'2018-11-12 20:44:27',NULL),('64d5d997eebf411a56eaeb3912e076e9ab2d2c13','pibernetwork','3','2018-11-16 17:17:52','openid profile email offline_access'),('66668a7d60f496c79c591eba5128fc8b2131cb97','pibernetwork','2','2018-11-29 17:26:32','openid profile email offline_access'),('69b4ce8a391dd31081938c3e9b26910242bd8a36','pibernetwork','2','2018-11-16 23:21:29','openid profile email offline_access'),('6a1b3add43d0336d0d07df5a1c44ef4681755cf3','pibernetwork','2','2018-11-17 23:11:50','openid profile email offline_access'),('713a70326a8f60879bcd54a0d1ddd6059bd3e04a','pibernetwork','2','2018-11-16 23:19:34','openid profile email offline_access'),('7281643d342fc758218a738815bf02f43878e26a','pibernetwork','2','2018-11-29 21:40:49','openid profile email offline_access'),('769c19f40f34d2d4aa89ef7a63175f385bdb9ddd','pibernetwork','2','2018-11-21 02:49:13','openid profile email offline_access'),('76dfde7266b9213173cb3ca1907a7dfb822d7c3b','pibernetwork','mauriciopiber','2033-06-26 20:19:26','openid profile'),('7ab3c6cdd12fbd0cece5f552be64cd13e2e98bf1','pibernetwork','2','2018-12-05 21:09:17','openid profile email offline_access'),('7c52c122f7d01b75376eec8bda6112b63395ae85','pibernetwork',NULL,'2018-11-12 19:40:01',NULL),('7c7406aa95eb72ba6a11d3b6dd4d32f37e4c9958','pibernetwork',NULL,'2018-11-12 19:40:57',NULL),('7d48ab4a52490a41484b7673f466db104d318996','pibernetwork','2','2018-11-18 11:56:04','openid profile email offline_access'),('84e84356b89588d533be714e7b191da5756dd693','pibernetwork','2','2018-12-03 19:53:21','openid profile email offline_access'),('856297e822a340dfd599edd2afab06f8c661a7e9','pibernetwork','2','2018-11-30 17:19:46','openid profile email offline_access'),('870f53d65f02b08c03f986d211a2a33447e82f6a','pibernetwork','2','2018-11-21 18:39:29','openid profile email offline_access'),('8c64f88dc404f7c72091320e12888742fc646b7b','pibernetwork','2','2018-11-30 14:53:12','openid profile email offline_access'),('8e0e46dad61c8d1f0b382357c7c3762e8eccedce','pibernetwork','mauriciopiber','2033-06-26 20:19:28','openid profile'),('8ee5c6d495ab4518861d71fcf2cd531395ff9f58','pibernetwork',NULL,'2033-06-26 20:07:59',NULL),('9163cb2375e6fefc518e700105f9d72ccd10216b','pibernetwork','mauriciopiber','2033-06-28 17:37:56','openid profile'),('933283c30ca5687a7b160e5f10cc8dfd39196a00','pibernetwork','mauriciopiber','2033-06-28 17:38:18','openid profile email'),('943a84127ea3e752b8aada229de977aa21c311cc','pibernetwork','1','2018-11-14 19:36:14','openid profile email offline_access'),('948642ae0e6c31b831f72ee412e2cb53e1633a66','testclient','mauriciopiber','2018-11-10 16:40:34',NULL),('9c1a59375f63f86cc623b86b481674e9169905aa','pibernetwork','2','2018-11-18 12:08:02','openid profile email offline_access'),('9c28f010d64b0f80d15fbbfc84b0747f594cdc15','pibernetwork',NULL,'2033-06-26 19:51:02',NULL),('9d8610c9a1c95eb4c79501eefd09acd6f5c7d822','pibernetwork',NULL,'2018-11-12 20:47:26',NULL),('9ea729459c5f1c59ee538d0679af9581e5324d97','pibernetwork','2','2018-11-18 12:10:24','openid profile email offline_access'),('a49d45334e3d71da0f671af0201ddf54da2120c9','pibernetwork','2','2018-11-30 13:39:24','openid profile email offline_access'),('ae27e1e15b622ec95ab24299d38c96c7f999cedc','pibernetwork','2','2018-11-29 23:42:10','openid profile email offline_access'),('b20d8708f496b2d7b4ec0e4a734fc8150f9aac57','pibernetwork','2','2018-11-18 11:56:17','openid profile email offline_access'),('b44ef7fc1894c065263ce01d6f86bb8278714f20','pibernetwork','2','2018-11-16 22:16:42','openid profile email offline_access'),('bdf0cd8d79f6954acdc711c5307e484936b419da','pibernetwork','2','2018-11-30 23:13:00','openid profile email offline_access'),('c0144e8c38470cdd879a28c3dffe26f42bfba2a3','pibernetwork','1','2018-11-14 19:37:05','openid profile email offline_access'),('c077aaa19d13a473f1d1093752f8ebbc02155477','pibernetwork',NULL,'2018-11-12 20:45:50',NULL),('c335d4e1d4eba383eac620079e0c1663d6430cb2','pibernetwork','2','2018-11-23 03:47:34','openid profile email offline_access'),('c38c61168c52fe135d192d5ef6a1f8e006ac070d','pibernetwork','2','2018-11-18 12:09:43','openid profile email offline_access'),('cd1e2840c0578aacbd098f98ce21883252fd6072','pibernetwork',NULL,'2033-06-26 20:08:03',NULL),('cd851f01f352e7214e9de9d8ad3fbfa2b659a860','pibernetwork','2','2018-11-16 21:59:27','openid profile email offline_access'),('d15a196b1585d6b00d1ab3d52946d461f0975139','pibernetwork','mauriciopiber','2018-11-10 16:41:40','openid profile email offline_access'),('d952aca3aee2349598bec8ef0f29e163797cd1c8','pibernetwork',NULL,'2018-11-12 19:39:35',NULL),('ddd8f47b1a8ff463829088f4562c16451c2a09b7','pibernetwork','2','2018-11-21 00:32:59','openid profile email offline_access'),('df805f9d76cdd8e3324955a06a88088d254cc990','pibernetwork','2','2018-11-16 21:59:50','openid profile email offline_access'),('e09308d127d095f51dc0f9b23f8de75bf5c1e9de','pibernetwork','2','2018-12-04 23:35:35','openid profile email offline_access'),('e2f3f2611d2d973215ab679e51bde465abcacc93','pibernetwork','2','2018-11-18 12:10:10','openid profile email offline_access'),('ebc85e8f2a706cf3f3f11a68a05906d0be25f405','pibernetwork','mauriciopiber','2018-11-12 21:07:37','openid profile email offline_access'),('ebcbb7fe01bdc3840e646b582946ccad8e6b07b6','pibernetwork','2','2018-11-18 12:09:55','openid profile email offline_access'),('ef32724ef5cc90bb894d612dbec8d7e3601b8d48','testclient',NULL,'2018-11-10 16:43:58',NULL),('f258dd406406faf0cceb3ae86be4e25219014cf3','pibernetwork','2','2018-11-22 16:01:29','openid profile email offline_access'),('f56a52fcaee8dafcc88478efd1757c0229af4828','pibernetwork','2','2018-11-23 13:43:16','openid profile email offline_access'),('f839760dad5e1eee6be7d8f3a7c4b1b7f95b0f32','pibernetwork',NULL,'2018-11-12 20:48:29',NULL),('f8b6c38a114b8cceb9cbb1e4fcbf461c2fc00272','pibernetwork','2','2018-11-21 12:26:00','openid profile email offline_access'),('f95f23afa4cbbb914e519d1a1486a0bf4a33df45','pibernetwork','2','2018-11-29 18:59:11','openid profile email offline_access'),('fa987fc86bbaf090b72d4994d0b27bda9668884a','pibernetwork','2','2018-11-30 19:16:05','openid profile email offline_access'),('fbba5cab694077e14c2619fe7c80c5948f25e034','pibernetwork','1','2018-11-14 19:37:26','openid profile email offline_access'),('fe48dafee3b7d189995516e5f0c8f0fb34d07f4f','pibernetwork','2','2018-12-01 00:52:15','openid profile email offline_access');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  `id_token` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
INSERT INTO `oauth_authorization_codes` VALUES ('088ea179af3efd6297cc47f8c2ce49ae0aaa0e14','pibernetwork','2','http://login.local/authorize','2018-11-16 22:19:18','openid profile email offline_access','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6OTA5MCIsInN1YiI6IjIiLCJhdWQiOiJwaWJlcm5ldHdvcmsiLCJpYXQiOjE1NDI0MDY3MjgsImV4cCI6MTU0MjQxMDMyOCwiYXV0aF90aW1lIjoxNTQyNDA2NzI4fQ.MPBugd6zIfKueRST2NAL4fjOps_9KEJDXShjIZDnssEu8Dr786NMQENgS9tg77Ry3z5zyXPIP9NFe0BINy1PhZ27g7Y-zMvgeCF4HFcXDxd3V-LGsZDsUiRAPj887nOHdFABOACMTctQVoHOA3bzSxxOxLW2lfwKSDFqdUyy-Q3ttGPQORn9p1eTuaMw0QWPfOTDuHSO-uq-pC3qU05ruBwAlzQunpquKUIbTsz0GAeFi09_M0xeF2iPANzABOqhs3czAL_NVSkZxCWedg2lNIF2HliX-N6Ut7bLeYU6_RJJqBciIxVATIUbuy3MnC2OZ8c1zo5rubehtpmS-YpCHQ'),('5c40720f16febb18569ebd98392fa41e07568525','pibernetwork','2','http://login.local/authorize','2018-11-16 22:10:11','openid profile email offline_access','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6OTA5MCIsInN1YiI6IjIiLCJhdWQiOiJwaWJlcm5ldHdvcmsiLCJpYXQiOjE1NDI0MDYxODEsImV4cCI6MTU0MjQwOTc4MSwiYXV0aF90aW1lIjoxNTQyNDA2MTgxfQ.umiliuGz_TxN_tjblInsAH4iLF9QJ_wqoHwrvhNgil9yhB0yU46iD4Kj4zDa7IX0z7KH4iyzpUuNYrqk4t9MVCwrYvMrolIfSXvTo1j_P9PpQpDmRS80UQW0ud2_xWAC6G-eIqnTeraFPldGjQHq7DdkqiV6jSPO75uokVs4GTSn6-39gw48ziDJEgqOANN1yK25_uujnHfKFDDv6Ex_HWUuDf-mVX7TiMrIDiVSs5GjQrkxy35tscdLAPQZc3qm3hXM4phJdtYZV0PCDysDo_6g16bGXDQlrUWd97M_KpnhY6kAOQBRlNe1YYduRYXAacE8b2ypNvpHys5t1EV3Og'),('62424356593e58dbe7e1805565048777a6dc890d','pibernetwork','mauriciopiber','http://login.local/authorize','2018-11-14 18:33:14','openid profile email offline_access','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6OTA5MCIsInN1YiI6Im1hdXJpY2lvcGliZXIiLCJhdWQiOiJwaWJlcm5ldHdvcmsiLCJpYXQiOjE1NDIyMjAzNjQsImV4cCI6MTU0MjIyMzk2NCwiYXV0aF90aW1lIjoxNTQyMjIwMzY0fQ.roxUzVdCgpcErzTVNpwIhdZt4HNhQxVdOTFYkfIWK6FPS1Ql8RPPy5d77UCBZ1YJUl8JHMcXEM6MVCHNI5u7lOtANd6lG270YCuLlokx_pXFUuul-eIk7KyNRJfRmr0MLWeeFBJDRV0oIqyHDDupEqtrxuha7dUmLzfXJdv-kYNH8dbZRYXuzMOSS3qoTQ-bXJHTuLjQl7rufzTie4qyQRVCR4kokSDCcpNWstqBXyc8ZdWD3FYAa6NsoHhF_6ioeMbj6gMxtrFF58He7BIaJt5m8XNpNOaaZ9Vjq2v_x6YVD5Su3qe6a5ATUv4jdIVeawmLdzgLRyNy-0Rwey9XwQ'),('fa651f03f7c14a3ad26f056515302c4be0c2c1e6','pibernetwork','2','http://login.local/authorize','2018-11-18 23:12:33','openid profile email offline_access','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6OTA5MCIsInN1YiI6IjIiLCJhdWQiOiJwaWJlcm5ldHdvcmsiLCJpYXQiOjE1NDI1ODI3MjMsImV4cCI6MTU0MjU4NjMyMywiYXV0aF90aW1lIjoxNTQyNTgyNzIzfQ.KJuRO2fmM-JGMLoeD-Xk2ycsbENYclDQF_5eHegmV30_o6ip1p4W8dDe3OvJM1sw8sKa1i5qv2u2mORhjrionm-2tNdQezxOgLecu_Vb126TvkqD7e1mQzhVBa_U7QgDIjzlDDLicKILEx2N6_wmgMSWs3IjD80QZQRRNgcQvk1_LnnWBNPSaeE_IYbC1xXPusBpK9aWzDfDFmYk-fbFNICaZxn1PKMaSFShhJ6cW1dsp_QP7Ox4q6k4sfizz-XN3SBnMtgIn42-9xRJFx5iwpDjfohmUkLN4he1o65Zp7U4ayCoYg-Hufd2wQq07k_TSU0tJJMwEl9wGZGA_Rsxxg');
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(2000) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('cursos-webflow',NULL,'http://login.local/authorize',NULL,'openid profile email create read',NULL),('login',NULL,'http://login.local/authorize',NULL,'openid profile email create read',NULL),('pibernetwork',NULL,'http://login.local/authorize',NULL,'openid profile email offline_access',NULL),('store01','$2y$14$eFG0hgIDw0Q1QKlJPN70YeqPCCvV20mC1rhJJuRDgDeaJDvWN6RTS','http://login.local/authorize',NULL,'create read',NULL),('testclient','$2y$14$f3qml4G2hG6sxM26VMq.geDYbsS089IBtVJ7DlD05BoViS9PFykE2','/oauth/receivecode',NULL,'create read',NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_public_keys`
--

DROP TABLE IF EXISTS `oauth_public_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_public_keys` (
  `client_id` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  `private_key` varchar(2000) DEFAULT NULL,
  `encryption_algorithm` varchar(100) DEFAULT 'RS256'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_public_keys`
--

LOCK TABLES `oauth_public_keys` WRITE;
/*!40000 ALTER TABLE `oauth_public_keys` DISABLE KEYS */;
INSERT INTO `oauth_public_keys` VALUES (NULL,'-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv1byVn/CcLMPYwed/AV6\nUasfPhAE0KJ3GoBsZ5E5f9rxLA39tIhNxeBKrUCsQwklfDg/IwJ7xvdxvnhT6Edk\nXt+ESIdmbaB97Flw4GxGi/VfjWblFR6SKpd/JE/H5PKOHrdvhKyQY6rEr171RwKG\nK42/oeEbsz94UOpQ12wyxt0ML+OBXhuSRuMu93JhfpVT82ndD0ck/nt+LUG2wulR\n5CUwDkoPRTGLaweY9nRxadzkCsZ6uz1KFYgRo78/hR4MMkCbF4Lp5aTws0T7Rs5f\nspjdkZUD4OHBhwRG8LcPHJ9JsR6L+Nt2kboexh0+XyDk/bAr8a/6VKgzIJYSdUsc\nNwIDAQAB\n-----END PUBLIC KEY-----','-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC/VvJWf8Jwsw9j\nB538BXpRqx8+EATQoncagGxnkTl/2vEsDf20iE3F4EqtQKxDCSV8OD8jAnvG93G+\neFPoR2Re34RIh2ZtoH3sWXDgbEaL9V+NZuUVHpIql38kT8fk8o4et2+ErJBjqsSv\nXvVHAoYrjb+h4RuzP3hQ6lDXbDLG3Qwv44FeG5JG4y73cmF+lVPzad0PRyT+e34t\nQbbC6VHkJTAOSg9FMYtrB5j2dHFp3OQKxnq7PUoViBGjvz+FHgwyQJsXgunlpPCz\nRPtGzl+ymN2RlQPg4cGHBEbwtw8cn0mxHov423aRuh7GHT5fIOT9sCvxr/pUqDMg\nlhJ1Sxw3AgMBAAECggEAYpdBjZKNNRb+wa9GF3sXGQfMtGT6pipgppYvxXnOH/RH\nusSGysRFFsXIlNczK+OFnik5VyfLca1Evbkwuwo2TLBtcBXckHZXDDbV87226B0K\nHEk9lxFGjY28ZJB7Vpwo4OjyHHHygHiYmiiax/kj+0Mx0A63ADifHUpvV2EYFOEh\nkjqtNeAVL9dRNjRidN+LvGY6VqZsh4U3ekHI8vjc/qa3WvG2/c7XE3Qj4p8nmcjX\nEfG9A4G/baxJeCk/Ah/zW871Kh4PpC7HLLByrji8SGgkMbC6k4wW2gUjoJEuStdZ\nwaS6IDY7RvQnc6ZqWOZQy5KOQ6qSwghp5W1MKP4WYQKBgQD7uIECHeeo8iAu0LzA\nWD7har0zx05XfGJDDSz+iIOgo6YmsmbS6C1Es9UeDlm0kBq3Y/3GPZEoHdS26cEc\ntS4qIHYZTKJW0b4modjrLTTxOYPphupylNwbhA/i7G2lvNhrV8zAGu5pquG3lZYc\nt2x0ORMdqiwQChjd13/FAw6ERwKBgQDCl6mUexLvGcskYF99duR6ldHuWB78imbO\nduVG1afllECdgA5AGrccZ39TRoJh31uB1RMU7QfBkrHSbQK6joGnIyQwgHIyvNLs\ns6Q2QdvCv7uXq2HfQ70YmKXDm1SWQrTrhFpL2yIse4D8BQWgbar7WRkoGSjCT/5+\nIAcD02RQkQKBgQCniDrEj+a4+L0Wq3KUiacAHCxko8euTmiPu9swZOdGKOTSaHYq\n9L/8M7wpzt9BX4+IcxJZuGw9yOzj+FvAoY1iGEzBtdFt+xaVW66lh2bvDZqP+G1d\naVm9Ln3sR/MKFc/sUXlCeionRcYIi5Rm9GVITRw96B5/Zlj6fQzasJgezQKBgHN4\nHZfPJGAfdSzjxyYjK9cWGhmNzu1aj0DKyqiq1dNkkqcjOtGNSMUhISvgNjQvnxQ+\nxPDw6l5SVxSfvTPIpmTwdGQXhCxZYew1eSb9E5PPxFhuyUf74TS8N/kHrjzv3n22\n7wH+SZwNuW9S55DYItK9fzENJOORJlnfVWpOwUMRAoGBAKt7csDB83ilQWYt2RK0\ncampKug9WsMWP9B3LCN/IWiICeMcv3ABlpaxlVaB+PLXwaW6czYWn+Z93PhZUdnz\nkbOxkvnLg4UuqLnnhmlLcP2G07b+ILiLsgU7PZ4IxFmzFkIu06ftQ8PPHUquzbZF\nDt4GStVFN52gOBy8hFC3O0AQ\n-----END PRIVATE KEY-----','RS256');
/*!40000 ALTER TABLE `oauth_public_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('08848b528d339390b8bc1d93752e577f933f8bb9','pibernetwork','2','2018-11-30 22:20:28','openid profile email offline_access'),('0bfe40863cf9233650a2a9f4e12c070d9e2f643c','pibernetwork','2','2018-12-19 20:09:17','openid profile email offline_access'),('0cd8360e0b37b2d3cfac1a11146e9484c19e959c','pibernetwork','mauriciopiber','2018-11-28 17:05:36','openid profile email offline_access'),('164be6c3af9850bf10c7e633ea7f2b3b225c7f35','pibernetwork','2','2018-12-08 09:52:12','openid profile email offline_access'),('1cf5d8ac8f3b85daf4888682938e124da740fcdd','pibernetwork','mauriciopiber','2018-11-26 20:07:37','openid profile email offline_access'),('20f8d7a014acac0561662bc8c052750d0c5179b4','pibernetwork','2','2018-12-02 23:15:03','openid profile email offline_access'),('23469bb6e8800489b0a8f6f0218b8307cf61b1ba','pibernetwork','2','2018-12-19 20:08:46','openid profile email offline_access'),('236362ec92c1603de69f38916a535803be504755','pibernetwork','2','2018-12-02 11:08:02','openid profile email offline_access'),('2bc1b4a1c1915c0981b74efc89b8f78c44d44f9e','pibernetwork','1','2018-11-28 18:38:00','openid profile email offline_access'),('2f790e92c3386189905b852345d90700631b01f8','pibernetwork','2','2018-12-02 10:28:25','openid profile email offline_access'),('3702e75d0fb89909464ad4a411c521bd3937f27b','pibernetwork','2','2018-12-02 11:12:00','openid profile email offline_access'),('37c83ae89d5d61fedbeb3ff98e7d2b23c3b458c2','pibernetwork','2','2018-11-30 20:59:50','openid profile email offline_access'),('39392ffd7a89aa48443053fb8042d776be893d40','pibernetwork','2','2018-12-02 11:10:24','openid profile email offline_access'),('49920cea74b02206d933f96f6908ec72eaab9cb4','pibernetwork','1','2018-11-28 18:36:14','openid profile email offline_access'),('626a3fb7d9199318c7b9b57f732406741d2699dc','pibernetwork','2','2018-12-01 22:09:59','openid profile email offline_access'),('649ac40e0da425a20fc18a0e02153044201fdbe3','pibernetwork','mauriciopiber','2018-11-24 15:41:40','openid profile email offline_access'),('7687ec91ff47d09323d4f5e094a05ce632dba04a','pibernetwork','2','2018-11-30 22:21:29','openid profile email offline_access'),('8aaf4fd958683d2d355a8aea087ba6bd7c45f576','pibernetwork','2','2018-11-30 22:19:34','openid profile email offline_access'),('939a429ed98b4cd1e80daef98e852e5417e06ce0','pibernetwork','3','2018-11-30 16:17:52','openid profile email offline_access'),('aca363f570c2974f5c2860e9add8ee63cc3b986c','pibernetwork','1','2018-11-28 18:37:26','openid profile email offline_access'),('c06352ac21d6d95f8cf7ddd3c036a67d7dfd20ef','pibernetwork','2','2018-12-02 10:56:04','openid profile email offline_access'),('c2beec61c2db5fdeea3c495eb2028a21eb8d5fcf','pibernetwork','2','2018-12-01 22:11:50','openid profile email offline_access'),('c8c7c83774ca58af6ecab733eec3a60567ca900e','pibernetwork','2','2018-11-30 22:11:13','openid profile email offline_access'),('de8aae2c3f6f47a8a484ced73183c27a773b6e3f','pibernetwork','2','2018-11-30 21:16:42','openid profile email offline_access'),('ef345fe76c40f20653c336716d653c08a90f0ba3','pibernetwork','2','2018-12-02 11:11:40','openid profile email offline_access'),('f07c30f1821e29b0050a75e53eceba7ed444a7d3','pibernetwork','2','2018-11-30 20:59:27','openid profile email offline_access'),('f0e3c5edad46b0d30fb9965ac1eb5159b0d34a1c','pibernetwork','2','2018-11-30 22:10:40','openid profile email offline_access'),('fd1c70c0fde3721585fd538327b4dc9409fe0ab9','pibernetwork','1','2018-11-28 18:37:05','openid profile email offline_access');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `type` varchar(255) NOT NULL DEFAULT 'supported',
  `scope` varchar(2000) DEFAULT NULL,
  `client_id` varchar(80) DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
INSERT INTO `oauth_scopes` VALUES ('supported','openid','login',NULL),('supported','first_name','login',NULL),('supported','last_name','login',NULL),('supported','create','login',NULL),('supported','read',NULL,NULL),('supported','read',NULL,NULL);
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `username` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(2000) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
INSERT INTO `oauth_users` VALUES (1,'$2y$14$CReqAM2CHk0TSaW2BjcEqekoCHjsUYgiqb1mrLTdxtanZcjTXzheW','asdfasd','sdfasdfsa','mauriciopiber@gmail.com',0),(2,'$2y$14$dWug5WXEFjqIuuNytPtt.OaiUiSVUlaGtLoFBQHQs0pvBYi3ejmr2','dfaf','adf','mothafuck@gmail.com',0),(4,'$2y$14$uCwuwq1CABXEsacekXWFtOL7J/dvXpYRxf6yusUtxVwlqPJtS932q',NULL,NULL,'user1@gmail.com',0),(7,'$2y$14$HDkRLrpyNSluchsEEufob.fVMhJCfxQV.nIfy9KptPr1S0mAbf.6.',NULL,NULL,'user6@gmail.com',0),(24,'$2y$14$4H6UpatsqZcjFOXQQsos8efh7K0GTTZ.JxIfKlLEeSRlEIYSXo54i',NULL,NULL,'piberpiber@gmail.com',0),(25,'$2y$14$QdeZN1UNhfAviHnlIf0nLOixdj7gYP8ngfjUVhoJZaOnytMqA5ZJO',NULL,NULL,'testeteste@gmail.com',0),(31,'$2y$14$DiXDS0PxYocSbrFug2rA/OO6NCOno1Ggt9qZBHIt8RG3oIDt1iLni',NULL,NULL,'dsopfksadpokf@gmail.com',0),(32,'$2y$14$IzLiqH6W7e2KHwknDmxd4O3OQ/imhpI8OIa6X8t3arF6fVJMAhdZO',NULL,NULL,'teste55@gmail.com',0);
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_role`),
  UNIQUE KEY `name` (`name`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `id_parent` (`id_parent`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_ibfk_3` FOREIGN KEY (`id_parent`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule`
--

DROP TABLE IF EXISTS `rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule` (
  `id_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_action` int(11) NOT NULL,
  `id_controller` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rule`),
  KEY `id_action` (`id_action`),
  KEY `id_controller` (`id_controller`),
  KEY `id_role` (`id_role`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `rule_ibfk_1` FOREIGN KEY (`id_action`) REFERENCES `action` (`id_action`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_2` FOREIGN KEY (`id_controller`) REFERENCES `controller` (`id_controller`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule`
--

LOCK TABLES `rule` WRITE;
/*!40000 ALTER TABLE `rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `username` varchar(50) NOT NULL,
  `state` int(11) NOT NULL,
  `uid` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'mptest1@gmail.com','mypassword','1',3,'2',NULL,NULL,NULL,NULL,NULL),(3,'mptest2@gmail.com','mypassword','1',3,'2',NULL,NULL,NULL,NULL,NULL),(4,'319mptest2@gmail.com','mypassword','65test',3,'2',NULL,NULL,NULL,NULL,NULL),(5,'194mptest2@gmail.com','mypassword','360test',3,'2',NULL,NULL,NULL,NULL,NULL),(6,'373mptest2@gmail.com','mypassword','433test',3,'2',NULL,NULL,NULL,NULL,NULL),(7,'241mptest2@gmail.com','mypassword','328test',3,'2',NULL,NULL,NULL,NULL,NULL),(8,'327mptest2@gmail.com','mypassword','214test',3,'2',NULL,NULL,NULL,NULL,NULL),(9,'testlogin1@gmail.com','$2y$14$RVMuN8zwzCGhvH27.5Cq.Ok4sacuamK1VJrn.WN/b9Z9VtWCFIwhC','52test',3,'2',NULL,NULL,NULL,NULL,NULL),(11,'testlogin3@gmail.com','$2y$14$zHvdO2cHq0nSmdJ23qO8veggD5RquQdic5MTm.g2k0O6X2Doc9Rri','484test',3,'2',NULL,NULL,NULL,NULL,NULL),(16,'testlogin4@gmail.com','$2y$14$3JkNfohOdw1y0V3h9zx2/espgmsvfkxzBgmX24pB0Zt0rzWnC/sBa','59test',3,'2',NULL,NULL,NULL,NULL,NULL),(17,'testlogin5@gmail.com','$2y$14$eN1//w6L/W/ZkOJwDUrsh.zF0VMTC1JQcmmL9h/tV2bWLfCpUo5bC','456test',3,'2',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_request`
--

DROP TABLE IF EXISTS `user_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_request` (
  `user_request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_request_type_id` int(11) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`user_request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_request`
--

LOCK TABLES `user_request` WRITE;
/*!40000 ALTER TABLE `user_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_request` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18 12:24:32
