<?php

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class Start extends AbstractMigration
{
    public function change()
    {
        $table = $this->table("oauth_access_tokens", ['id' => false, 'primary_key' => ["access_token"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('access_token', 'string', ['null' => false, 'limit' => 40, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('client_id', 'string', ['null' => false, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'access_token'])->save();
        $table->addColumn('user_id', 'string', ['null' => true, 'limit' => 255, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_id'])->save();
        $table->addColumn('expires', 'timestamp', ['null' => false, 'default' => "CURRENT_TIMESTAMP", 'update' => 'CURRENT_TIMESTAMP', 'after' => 'user_id'])->save();
        $table->addColumn('scope', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'expires'])->save();
        $table->save();

        $table = $this->table("oauth_authorization_codes", ['id' => false, 'primary_key' => ["authorization_code"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('authorization_code', 'string', ['null' => false, 'limit' => 40, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('client_id', 'string', ['null' => false, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'authorization_code'])->save();
        $table->addColumn('user_id', 'string', ['null' => true, 'limit' => 255, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_id'])->save();
        $table->addColumn('redirect_uri', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'user_id'])->save();
        $table->addColumn('expires', 'timestamp', ['null' => false, 'default' => "CURRENT_TIMESTAMP", 'update' => 'CURRENT_TIMESTAMP', 'after' => 'redirect_uri'])->save();
        $table->addColumn('scope', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'expires'])->save();
        $table->addColumn('id_token', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'scope'])->save();
        $table->save();

        $table = $this->table("oauth_jwt", ['id' => false, 'primary_key' => ["client_id"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('client_id', 'string', ['null' => false, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('subject', 'string', ['null' => true, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_id'])->save();
        $table->addColumn('public_key', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'subject'])->save();
        $table->save();

        $table = $this->table("oauth_public_keys", ['id' => false, 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('client_id', 'string', ['null' => true, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('public_key', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_id'])->save();
        $table->addColumn('private_key', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'public_key'])->save();
        $table->addColumn('encryption_algorithm', 'string', ['null' => true, 'default' => "RS256", 'limit' => 100, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'private_key'])->save();
        $table->save();

        $this->query(file_get_contents(__DIR__.'/../public_keys.sql'));

        $table = $this->table("oauth_refresh_tokens", ['id' => false, 'primary_key' => ["refresh_token"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('refresh_token', 'string', ['null' => false, 'limit' => 40, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('client_id', 'string', ['null' => false, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'refresh_token'])->save();
        $table->addColumn('user_id', 'string', ['null' => true, 'limit' => 255, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_id'])->save();
        $table->addColumn('expires', 'timestamp', ['null' => false, 'default' => "CURRENT_TIMESTAMP", 'update' => 'CURRENT_TIMESTAMP', 'after' => 'user_id'])->save();
        $table->addColumn('scope', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'expires'])->save();
        $table->save();

        $table = $this->table("oauth_scopes", ['id' => false, 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('type', 'string', ['null' => false, 'default' => "supported", 'limit' => 255, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('scope', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'type'])->save();
        $table->addColumn('client_id', 'string', ['null' => true, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'scope'])->save();
        $table->addColumn('is_default', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_SMALL, 'precision' => 5, 'after' => 'client_id'])->save();
        $table->save();

        $table = $this->table("oauth_clients", ['id' => false, 'primary_key' => ["client_id"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('client_id', 'string', ['null' => false, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8"])->save();
        $table->addColumn('client_secret', 'string', ['null' => true, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_id'])->save();
        $table->addColumn('redirect_uri', 'string', ['null' => false, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'client_secret'])->save();
        $table->addColumn('grant_types', 'string', ['null' => true, 'limit' => 80, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'redirect_uri'])->save();
        $table->addColumn('scope', 'string', ['null' => true, 'limit' => 2000, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'grant_types'])->save();
        $table->addColumn('user_id', 'string', ['null' => true, 'limit' => 255, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'scope'])->save();
        $table->save();

        $rows = [
            [
                'client_id' => 'pibernetwork',
                'client_secret' => null,
                'redirect_uri' => 'piber.network/authorize',
                'grant_types' => null,
                'scope' => 'email profile openid'
            ]
        ];

        $this->table('oauth_clients')->insert($rows)->save();

        $email = $this->table('user_request', ['id' => 'id_user_request']);

        $table->addColumn('user_id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable']);
        $table->addColumn('user_request_id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable']);
        $table->addColumn('uid', 'string', ['null' =>false]);
        $table->addColumn('created', 'datetime', ['null' => false]);
        $table->addColumn('expires', 'datetime', ['null' => false]);
        $table->addColumn('invalid', 'boolean', ['null' => false]);

        $table->save();

        $table = $this->table("oauth_users");
        $table->addIndex(['email'], ['name' => "email_UNIQUE", 'unique' => true])->save();


        $rows = [
            [
                'password' => '$2y$14$aWY1wgRdPgM5UA1YuCKD4OoNGGC0qPKacb.Cvjzm4erMQg0uNaWQu',
                'email' => 'admin@piber.network',
                'email_verified' => 1
            ]
        ];


        $this->table('aouth_users')->insert($rows)->save();




        $table = $this->table("acl_role", ['id' => false, 'primary_key' => ["role_id"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('role_id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'identity' => 'enable']);
        $table->addColumn('role', 'string', ['null' => true, 'limit' => 45, 'collation' => "utf8_general_ci", 'encoding' => "utf8", 'after' => 'role_id']);
        $table->addColumn('parent_id', 'integer', ['null' => true, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'role']);
        $table->addColumn('is_default', 'boolean', ['null' => true, 'limit' => MysqlAdapter::INT_TINY, 'precision' => 3, 'after' => 'email'])->save();
        $table->save();

        $rows = [
              [
                'role' => 'guest',
                'is_default' => true
              ],
              [
                'role' => 'member',
                'parent_id' => 1,
                'is_default' => true
              ],
              [
                'role' => 'admin',
                'parent_id' => 2,
                'is_default' => true
              ],
              [
                'role' => 'master',
                'is_default' => true,
                'parent' => 3
              ]
            ];

        ];

        $this->table('acl_role')->insert($rows)->save();

        $table = $this->table("acl_user_role", ['id' => false, 'primary_key' => ["user_id", "role_id"], 'engine' => "InnoDB", 'encoding' => "utf8", 'collation' => "utf8_general_ci", 'comment' => "", 'row_format' => "Dynamic"]);
        $table->addColumn('user_id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10]);
        $table->addColumn('role_id', 'integer', ['null' => false, 'limit' => MysqlAdapter::INT_REGULAR, 'precision' => 10, 'after' => 'user_id']);
        $table->save();


        $rows = [
            [
                'user_id' => 1,
                'role_id' => 4
            ]
        ];


        // $table = $this->table('acl_resource');
        // $table->save();
        //
        // $table = $this->table('acl_rule');
        // $table->save();
    }
}
