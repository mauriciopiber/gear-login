<?php
namespace GearLogin\User\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use GearLogin\User\Controller\UserController;
use GearLogin\User\Service\UserService;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package GearLogin/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserControllerFactory implements FactoryInterface
{
    /**
     * Create UserController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return UserController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        $factory = new UserController(
          $container->get(UserService::class)
        );
        return $factory;
    }
}
