<?php
namespace GearLogin\User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Serializer\Adapter\Json as JsonAdapter;
use GearLogin\User\Service\UserService;

/**
 * @OA\Info(title="My First API", version="0.1")
 */

class UserController extends AbstractRestfulController
{
    protected $jsonAdapter;

    public function __construct(UserService $userService)
    {
      $this->userService = $userService;
      $this->jsonAdapter = new JsonAdapter();
    }

  

    public function create($params)
    {
      $response = $this->getResponse();

      $create = $this->userService->create($params);
      $response->setContent($this->jsonAdapter->serialize(['payload' => $create]));
      $response->setStatusCode(201);
      return $response;
    }

    public function update($id, $params)
    {
      $response = $this->getResponse();

      $create = $this->userService->update($id, $params);
      $response->setContent($this->jsonAdapter->serialize(['payload' => $create]));
      $response->setStatusCode(201);
      return $response;
    }

    public function delete($id)
    {
      $response = $this->getResponse();

      $deleted = $this->userService->delete($id);

      $response->setContent($this->jsonAdapter->serialize(['payload' => $deleted]));
      $response->setStatusCode(201);
      return $response;

    }

    public function getList()
    {
      $response = $this->getResponse();

      $listUsers = $this->userService->listAll();

      $response->setContent($this->jsonAdapter->serialize(['payload' => $listUsers]));
      $response->setStatusCode(201);
      return $response;
    }

    /**
     * @OA\Get(
     *     path="/api/resource.json",
     *     @OA\Response(response="200", description="An example resource")
     * )
     */
    public function get($name)
    {
      $response = $this->getResponse();

      $listUsers = $this->userService->getById($name);

      $response->setContent($this->jsonAdapter->serialize(['payload' => $listUsers]));
      $response->setStatusCode(201);
      return $response;
    }
}
