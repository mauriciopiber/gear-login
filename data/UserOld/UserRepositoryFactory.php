<?php
namespace GearLogin\Repository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;

class UserRepositoryFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new UserRepository(
            $serviceLocator->get('Zend\Db\Adapter\Adapter')
        );
        unset($serviceLocator);
        return $factory;
    }

    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new UserRepository(
          $container->get('Zend\Db\Adapter\Adapter')
      );
      unset($serviceLocator);
      return $factory;
    }
}
