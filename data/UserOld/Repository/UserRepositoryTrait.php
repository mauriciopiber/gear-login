<?php
namespace GearLogin\User\Repository;

use GearLogin\User\Repository\UserRepositoryFactory;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package GearLogin/User/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
trait UserRepositoryTrait
{
    protected $userRepository;

    /**
     * Get User Repository
     *
     * @return UserRepository
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * Set User Repository
     *
     * @param UserRepository $userRepository User Repository
     *
     * @return UserRepository
     */
    public function setUserRepository(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
        return $this;
    }
}
