<?php
namespace GearLogin\User\Repository;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use GearLogin\User\Repository\UserRepository;
use Interop\Container\ContainerInterface;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package GearLogin/User/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRepositoryFactory implements FactoryInterface
{
    /**
     * Create UserRepository
     *
     * @param ServiceLocatorInterface $serviceLocator ServiceManager instance
     *
     * @return UserRepository
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new UserRepository(
          $serviceLocator->get('Zend\Db\Adapter\Adapter')
        );
        unset($serviceLocator);
        return $factory;
    }

    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new UserRepository(
        $container->get('Zend\Db\Adapter\Adapter')
      );
      //unset($serviceLocator);
      return $factory;
    }
}
