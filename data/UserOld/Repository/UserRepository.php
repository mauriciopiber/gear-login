<?php
namespace GearLogin\User\Repository;

use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package GearLogin/User/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRepository
{
  const TABLE = 'oauth_users';
    /**
     * Constructor
     *
     * @return UserRepository
     */
    public function __construct($dbAdapter)
    {
      $this->dbAdapter = $dbAdapter;
      return $this;
    }

    public function update($id, $data)
    {
      $sql = new Sql($this->dbAdapter);
      $update = $sql->update(self::TABLE);

      $bcrypt = new Bcrypt;
      $bcrypt->setCost(14);
      $password = $bcrypt->create($data['password']);

      $values = [
        'password' => $password,
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name']
      ];


      $update->set($values);

      $update->where(['username' => $id]);

      $statement = $sql->prepareStatementForSqlObject($update);
      $results = $statement->execute();

      if ($results instanceof \Zend\Db\Adapter\Driver\ResultInterface) {
        return 'Update ok';
      }

      return 'Error while register';
    }

    public function delete($id)
    {
      $sql = new Sql($this->dbAdapter);
      $delete = $sql->delete(self::TABLE);
      $delete->where(['username' => $id]);

      $statement = $sql->prepareStatementForSqlObject($delete);
      $results = $statement->execute();

      if ($results instanceof \Zend\Db\Adapter\Driver\ResultInterface) {
        return 'Delete ok';
      }

      return 'Error while delete';

    }


    public function insert($data)
    {
      $sql = new Sql($this->dbAdapter);
      $insert = $sql->insert(self::TABLE);

      $bcrypt = new Bcrypt;
      $bcrypt->setCost(14);
      $password = $bcrypt->create($data['password']);

      $values = [
        //'username' => $data['username'],
        'password' => $password,
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'email_verified' => 0
      ];

      //unset($values['username']);

      $insert->values($values);

      $statement = $sql->prepareStatementForSqlObject($insert);
      $results = $statement->execute();

      if ($results instanceof \Zend\Db\Adapter\Driver\ResultInterface) {
        return 'Register ok';
      }

      return 'Error while register';
    }

    public function selectAll()
    {
      $sql = new Sql($this->dbAdapter);
      $select = $sql->select();
      $select->from(self::TABLE);
      $select->columns(['username', 'first_name', 'last_name']);
      //$select->where(['email' => $email]);

      $statement = $sql->prepareStatementForSqlObject($select);
      $results = $statement->execute();

      $info = [];

      foreach ($results as $row) { $info[] = $row; }

      return $info;
    }

    public function findOneByEmail($email)
    {
      $sql = new Sql($this->dbAdapter);
      $select = $sql->select();
      $select->from(self::TABLE);
      $select->columns(['username', 'first_name', 'last_name', 'email', 'password', 'email_verified']);

      $select->where(['email' => $email]);

      $statement = $sql->prepareStatementForSqlObject($select);
      $results = $statement->execute();

      $info = [];

      foreach ($results as $row) { $info[] = $row; }

      if (empty($info) || count($info) > 1) {
        return false;
      }

      $user = $info[0];

      return $user;
    }

    public function findRoleById($id)
    {
      $sql = new Sql($this->dbAdapter);
      $select = $sql->select();
      $select->from(['ur' => 'oauth_user_role']);
      $select->join(['r' => 'oauth_role'], 'r.role_id = ur.role_id', ['role']);


      $select->where(['ur.role_id' => $id]);

      $statement = $sql->prepareStatementForSqlObject($select);
      $results = $statement->execute();

      $info = [];

      foreach ($results as $row) {
        $role = $row['role'];
        $info[] = $role;
      }

      //var_dump($info);

      return $info;
    }

    public function selectById($id)
    {
      $sql = new Sql($this->dbAdapter);
      $select = $sql->select();
      $select->from(self::TABLE);
      $select->columns(['username', 'first_name', 'last_name']);

      $select->where(['username' => $id]);

      $statement = $sql->prepareStatementForSqlObject($select);
      $results = $statement->execute();

      $info = [];

      foreach ($results as $row) { $info[] = $row; }

      if (empty($info) || count($info) > 1) {
        return false;
      }

      $user = $info[0];

      return $user;
    }
}
