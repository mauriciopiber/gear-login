<?php
namespace GearLogin\Repository;

use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;

class UserRepository
{
  protected $dbAdapter;

  public function __construct($dbAdapter)
  {
    $this->dbAdapter = $dbAdapter;
  }

  public function findAll() {
    $sql = new Sql($this->dbAdapter);
    $select = $sql->select();
    $select->from('user');
    $select->columns(['email', 'username']);
    //$select->where(['email' => $email]);

    $statement = $sql->prepareStatementForSqlObject($select);
    $results = $statement->execute();

    $info = [];

    foreach ($results as $row) { $info[] = $row; }

    return $info;
  }

  public function register($data)
  {
    $sql = new Sql($this->dbAdapter);
    $insert = $sql->insert('user');

    $bcrypt = new Bcrypt;
    $bcrypt->setCost(14);
    $password = $bcrypt->create($data['password']);

    $values = [
      'email' => $data['email'],
      'password' => $password,
      'username' => rand(1, 500).'test',
      'uid' => 2,
      'state' => 3
    ];

    $insert->values($values);

    $statement = $sql->prepareStatementForSqlObject($insert);
    $results = $statement->execute();

    if ($results instanceof \Zend\Db\Adapter\Driver\ResultInterface) {
      return 'Register ok';
    }

    return 'Error while register';
  }

  public function findByEmail($email) {
    $sql = new Sql($this->dbAdapter);
    $select = $sql->select();
    $select->from('user');
    $select->where(['email' => $email]);

    $statement = $sql->prepareStatementForSqlObject($select);
    $results = $statement->execute();

    $info = [];

    foreach ($results as $row) { $info[] = $row; }

    if (empty($info) || count($info) > 1) {
      return false;
    }

    $user = $info[0];

    return $user;
  }

  public function login($data) {
    $sql = new Sql($this->dbAdapter);
    $select = $sql->select();
    $select->from('user');
    $select->where($data);

    $statement = $sql->prepareStatementForSqlObject($select);
    $results = $statement->execute();

    $info = [];

    foreach ($results as $row) { $info[] = $row; }

    if (empty($info) || count($info) > 1) {
      return false;
    }

    $user = $info[0];

    return $user;
  }
}
