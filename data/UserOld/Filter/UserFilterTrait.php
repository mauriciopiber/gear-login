<?php
namespace GearLogin\User\Filter;

use GearLogin\User\Filter\UserFilterFactory;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package GearLogin/User/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
trait UserFilterTrait
{
    protected $userFilter;

    /**
     * Get User Filter
     *
     * @return UserFilter
     */
    public function getUserFilter()
    {
        return $this->userFilter;
    }

    /**
     * Set User Filter
     *
     * @param UserFilter $userFilter User Filter
     *
     * @return UserFilter
     */
    public function setUserFilter(
        UserFilter $userFilter
    ) {
        $this->userFilter = $userFilter;
        return $this;
    }
}
