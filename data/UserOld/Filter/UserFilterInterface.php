<?php
namespace GearLogin\Filter;

use GearLogin\Filter\UserFilter;

/**
 * PHP Version 5
 *
 * @category Interface
 * @package GearLogin/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
interface UserFilterInterface
{
    /**
     * Get User Filter
     *
     * @return null|GearLogin\Filter\UserFilter
     */
    public function getUserFilter();

    /**
     * Set User Filter
     *
     * @param UserFilter $userFilter User Filter
     *
     * @return self
     */
    public function setUserFilter(UserFilter $userFilter);
}
