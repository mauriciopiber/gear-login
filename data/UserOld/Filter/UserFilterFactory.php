<?php
namespace GearLogin\User\Filter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use GearLogin\User\Filter\UserFilter;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package GearLogin/User/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserFilterFactory implements FactoryInterface
{
    /**
     * Create UserFilter
     *
     * @param ServiceLocatorInterface $serviceLocator ServiceManager instance
     *
     * @return UserFilter
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new UserFilter(
        );
        unset($serviceLocator);
        return $factory;
    }
    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new UserFilter();
      return $factory;
    }
}
