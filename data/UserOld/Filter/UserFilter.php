<?php
namespace GearLogin\User\Filter;

use Zend\InputFilter\InputFilter;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package GearLogin/User/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserFilter extends InputFilter
{
    /**
     * Constructor
     *
     * @return UserFilter
     */
    public function __construct()
    {
        return $this;
    }

    /**
     * Create the Filter to Form Elements
     *
     * @return UserFilter
     *
     */
    public function getInputFilter()
    {
      $this->add(array(
        'name'       => 'password',
        'required'   => true,
      ));

      $this->add(array(
        'name'       => 'email',
        'required'   => true,
      ));


      $this->add(array(
        'name'       => 'first_name',
        'required'   => true,
      ));

      $this->add(array(
        'name'       => 'last_name',
        'required'   => true,
      ));
      return $this;
    }
}
