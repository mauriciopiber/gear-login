<?php
namespace GearLogin\User\Service;

use GearLogin\User\Service\UserServiceFactory;

/**
 * PHP Version 5
 *
 * @category Service
 * @package GearLogin/User/Service
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
trait UserServiceTrait
{
    protected $userService;

    /**
     * Get User Service
     *
     * @return UserService
     */
    public function getUserService()
    {
        return $this->userService;
    }

    /**
     * Set User Service
     *
     * @param UserService $userService User Service
     *
     * @return UserService
     */
    public function setUserService(
        UserService $userService
    ) {
        $this->userService = $userService;
        return $this;
    }
}
