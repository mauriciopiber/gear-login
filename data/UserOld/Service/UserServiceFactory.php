<?php
namespace GearLogin\User\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use GearLogin\User\Service\UserService;
use GearLogin\User\Filter\UserFilter;
use GearLogin\User\Repository\UserRepository;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package GearLogin/User/Service
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserServiceFactory implements FactoryInterface
{
    /**
     * Create UserService
     *
     * @param ServiceLocatorInterface $serviceLocator ServiceManager instance
     *
     * @return UserService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new UserService(
            $serviceLocator->get(UserFilter::class),
            $serviceLocator->get(UserRepository::class)
        );
        unset($serviceLocator);
        return $factory;
    }

    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new UserService(
          $container->get(UserFilter::class),
          $container->get(UserRepository::class)
      );
      //unset($serviceLocator);
      return $factory;
    }
}
