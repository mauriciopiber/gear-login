<?php
namespace GearLogin\User\Service;

use GearLogin\User\Filter\UserFilterTrait;
use GearLogin\User\Repository\UserRepositoryTrait;
use GearLogin\User\Filter\UserFilter;
use GearLogin\User\Repository\UserRepository;

/**
 * PHP Version 5
 *
 * @category Service
 * @package GearLogin/User/Service
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserService
{
    use UserFilterTrait;

    use UserRepositoryTrait;

    /**
     * Constructor
     *
     * @param UserFilter     $userFilter     User Filter
     * @param UserRepository $userRepository User Repository
     *
     * @return UserService
     */
    public function __construct(
        UserFilter $userFilter,
        UserRepository $userRepository
    ) {
        $this->userFilter = $userFilter;
        $this->userRepository = $userRepository;

        return $this;
    }


  public function create($data)
  {
    $filter = $this->userFilter->getInputFilter();

    $filter->setData($data);

    $isValid = $filter->isValid();
    //var_dump($isValid);

    if ($isValid === false) {
      return $filter->getMessages();
    }
    $register = $this->userRepository->insert($filter->getValues());
    return $register;
  }

  public function update($id, $data)
  {
    $filter = $this->userFilter->getInputFilter();
    $filter->remove('username');

    $filter->setData($data);

    $isValid = $filter->isValid();
    //var_dump($isValid);

    if ($isValid === false) {
      return $filter->getMessages();
    }
    $register = $this->userRepository->update($id, $filter->getValues());
    return $register;
  }

  public function delete($id)
  {
    return $this->userRepository->delete($id);
  }

  /**
   * List all user
   */
  public function listAll()
  {
      return $this->userRepository->selectAll();
  }

  public function getById($id)
  {
    return $this->userRepository->selectById($id);
  }

  public function findOneByEmail($email)
  {
    return $this->userRepository->findOneByEmail($email);
  }

  public function findRoleByUser($id) {
    return $this->userRepository->findRoleById($id);
  }
}
