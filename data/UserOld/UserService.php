<?php
namespace GearLogin\Service;

class UserService {

  protected $userRepository;

  public function __construct($userRepository, $registerFilter) {
    $this->userRepository = $userRepository;
    $this->registerFilter = $registerFilter;
  }

  public function listAll()
  {
    $register = $this->userRepository->findAll();
    return $register;
  }

  public function register($data) {
    $filter = $this->registerFilter->getInputFilter();

    $filter->setData($data);

    $isValid = $filter->isValid();
    //var_dump($isValid);

    if ($isValid === false) {
      return $filter->getMessages();
    }

    $register = $this->userRepository->register($filter->getValues());
    return $register;
  }

  public function get($email) {

  }
}
