<?php
namespace GearLogin\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;

class UserServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new UserService(
          $serviceLocator->get(\GearLogin\Repository\UserRepository::class),
          $serviceLocator->get(\GearLogin\Filter\RegisterFilter::class)
        );
        unset($serviceLocator);
        return $factory;
    }

    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        $factory = new UserService(
          $container->get(\GearLogin\Repository\UserRepository::class),
          $container->get(\GearLogin\Filter\RegisterFilter::class)
        );
        unset($container);
        return $factory;
    }
}
