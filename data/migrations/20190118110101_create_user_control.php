<?php


use Phinx\Migration\AbstractMigration;

class CreateUserControl extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $email = $this->table('user_request', ['id' => false, 'primary_key' => 'user_request_id']);
        $email->addColumn('user_request_id', 'integer', ['null' => false]);
        $email->addColumn('user_id', 'integer', ['null' => false]);
        $email->addColumn('user_request_type_id', 'integer', ['null' => false]);
        $email->addColumn('uid', 'string', ['null' => false]);
        $email->addColumn('created', 'datetime', ['null' => false]);
        $email->addColumn('expires', 'datetime', ['null' => false]);

        $email->save();
    }
}
