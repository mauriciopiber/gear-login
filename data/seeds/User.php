<?php
use Phinx\Seed\AbstractSeed;
use Zend\Crypt\Password\Bcrypt;

class User extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
      $bcrypt = new Bcrypt;
      $bcrypt->setCost(14);
      $data = [
          [
              'email'    => 'pbrtest1@gmail.com',
              'first_name' => 'Name 1',
              'last_name' => 'Name 1',
              'email_verified' => 0,
              'password' => $bcrypt->create('123456')
          ],
          [
              'email'    => 'pbrtest2@gmail.com',
              'first_name' => 'Name 2',
              'last_name' => 'Name 2',
              'email_verified' => 0,
              'password' => $bcrypt->create('123456')
          ],
          [
              'email'    => 'pbrtest3@gmail.com',
              'first_name' => 'Name 3',
              'last_name' => 'Name 3',
              'email_verified' => 0,
              'password' => $bcrypt->create('123456')
          ],
          [
              'email'    => 'pbrtest4@gmail.com',
              'first_name' => 'Name 4',
              'last_name' => 'Name 4',
              'email_verified' => 0,
              'password' => $bcrypt->create('123456')
          ],
          [
              'email'    => 'pbrtest5@gmail.com',
              'first_name' => 'Name 5',
              'last_name' => 'Name 5',
              'email_verified' => 0,
              'password' => $bcrypt->create('123456')
          ]
      ];

      $posts = $this->table('oauth_users');
      $posts->truncate();
      $posts->insert($data)
            ->save();
    }
}
