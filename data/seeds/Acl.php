<?php


use Phinx\Seed\AbstractSeed;

class Acl extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
      $data = [
          [
              'role'    => 'guest',
          ],
          [
              'role'    => 'user',
          ],
          [
              'role'    => 'admin',
              'parent_id' => 2
          ],

      ];

      $posts = $this->table('acl_role');
      $posts->truncate();
      $posts->insert($data)
            ->save();



      $data = [
          [
              'role_id' => 3,
              'user_id' => 1
          ],
          [
              'role_id' => 3,
              'user_id' => 2
          ],
          [
              'role_id' => 2,
              'user_id' => 3
          ],
          [
              'role_id' => 2,
              'user_id' => 4
          ],
          [
              'role_id' => 2,
              'user_id' => 5
          ],

      ];

      $posts = $this->table('acl_user_role');
      $posts->truncate();
      $posts->insert($data)
            ->save();
    }
}
