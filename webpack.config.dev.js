const merge = require('webpack-merge');
const common = require('./webpack.config.common.js');


const config = merge(common, {
  watch: true,
});

module.exports = config;
