FROM registry.piber.network/dev-php-apache-node:v1.0.3 as builder

WORKDIR /var/www/
RUN git clone git@bitbucket.org:mauriciopiber/gear-login.git \
  && cd gear-login \
  && git pull origin master
WORKDIR /var/www/gear-login

RUN composer install --no-dev -o
RUN npm install
RUN npm run build
RUN chmod 777 -R data/session
#RUN node_modules/.bin/webpack --config config/webpack.config.js --mode production --debug --progress --display-error-details --verbose

FROM registry.piber.network/php-apache:v1.0.3
COPY --from=builder /var/www/gear-login /var/www/module
