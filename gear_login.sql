-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: gear-login-mysql    Database: gear_login
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action`
--

DROP TABLE IF EXISTS `action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action` (
  `id_action` int(11) NOT NULL AUTO_INCREMENT,
  `id_controller` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_action`),
  KEY `id_controller` (`id_controller`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `action_ibfk_1` FOREIGN KEY (`id_controller`) REFERENCES `controller` (`id_controller`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `action_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `action_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action`
--

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;
/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controller`
--

DROP TABLE IF EXISTS `controller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controller` (
  `id_controller` int(11) NOT NULL AUTO_INCREMENT,
  `id_module` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `invokable` varchar(150) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_controller`),
  KEY `id_module` (`id_module`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `controller_ibfk_1` FOREIGN KEY (`id_module`) REFERENCES `module` (`id_module`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `controller_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `controller_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controller`
--

LOCK TABLES `controller` WRITE;
/*!40000 ALTER TABLE `controller` DISABLE KEYS */;
/*!40000 ALTER TABLE `controller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `remetente` varchar(255) NOT NULL,
  `destino` varchar(255) NOT NULL,
  `assunto` varchar(255) NOT NULL,
  `mensagem` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_email`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `email_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `email_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (20150223203704,'CreateGearAdmin','2018-08-14 13:41:27','2018-08-14 13:41:33',0),(20150304070210,'ReplaceRoleOneToMany','2018-08-14 13:41:33','2018-08-14 13:41:34',0);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module`
--

DROP TABLE IF EXISTS `module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module` (
  `id_module` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_module`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `module_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `module_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module`
--

LOCK TABLES `module` WRITE;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
/*!40000 ALTER TABLE `module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`access_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('008c6ada02ea37f4bdd2ea4bd38420fa636b006c','login','createpiber','2018-09-18 19:21:03','email profile openid'),('0a62193a02e99e9ea3fc2463247cf4ef80649c5e','login','createpiber','2018-09-18 21:39:10',NULL),('21f80ea6adcfc95c1d195304c7ca3305100d048b','login','createpiber','2018-09-18 21:41:55',NULL),('2ca149aa4ed2512c349d60c93bb82d4994a76643','login','createpiber','2018-09-18 23:27:16','openid profile email'),('3af86ff88678b8fe5d65187385a6f53bc2837024','testclient',NULL,'2033-04-25 14:29:20','create read'),('5081d11e8282e7c71463f74554d5f44bc1ddb89d','login','createpiber','2018-09-18 22:42:45',NULL),('5e04a77e4b95a7ad4a4f1aac27692581c2fb3df9','pibernetwork','createpiber','2018-10-13 19:31:57','openid profile email'),('6d4b5da61d0a7ddfc9090c4c66304fc8623031cd','login','createpiber','2018-09-18 22:15:52',NULL),('717ceaeb4a70d0e92c1e56a80f83924fd56d61fd','login','createpiber','2018-09-18 19:18:07','email profile openid'),('71859090cd54c4660a6befe671d1a19df8994733','login','createpiber','2018-09-18 22:56:32','openid profile email'),('7560a5e914d0e2a1d9334858e9b0269181dad9a7','login','createpiber','2018-09-18 19:17:04',NULL),('79e9b370051e0c477e0266e76a3384181e083f8f','pibernetwork','createpiber','2018-10-13 21:57:07','openid email profile'),('7aa0c5a25af1cf562f40efc36d906aa08eaa65ac','testclient',NULL,'2033-04-25 15:06:48','create read'),('85e7f8b2de543fb40c6e1349f37af20dfd784e00','pibernetwork','createpiber','2018-10-13 22:31:01','openid profile email'),('96d105e0be27e97aa882e4b650d1cdc2ab7d2c80','login','createpiber','2018-09-18 20:55:50',NULL),('9a151cb5fc173877bb08e25729144b2237d3f9f8','testclient',NULL,'2018-09-19 16:40:22','create read'),('ad8f8bc837e5cc7587849df0987fe458974fc9a8','login','createpiber','2018-09-18 19:17:47','email'),('adb721777a0acb2bb292591f9e92bd5ee880ab6f','login','createpiber','2018-09-18 21:12:43',NULL),('b170937e8e90e62c188e0846e9eaec3f99563f7f','testclient',NULL,'2018-09-19 16:50:23','create read'),('b2feeaccdf5fe41475c16d5db01da3f319ab3c32','login','createpiber','2018-09-18 22:22:04',NULL),('b7d3b658a1c2d87aec454fa8710bfa25c347aabc','testclient',NULL,'2033-05-02 23:18:29','create read'),('b89c08fd2ef33ca911563d8d3e8a149fefe5bb1c','login','createpiber','2033-05-02 23:26:50','openid profile email'),('bb1ee26e7766fd10182b1bc828314460e3131a27','login','createpiber','2018-09-18 21:12:00',NULL),('beb81ce51ce6b173246e2542fd8fa88776c99999','login','createpiber','2018-09-18 19:17:56','email profile'),('c49d070e279658f52156ea4ad9a5bc90cc624a3c','cursos-webflow','createpiber','2018-10-13 19:08:53','openid profile'),('cf79ebb00c746acebe5babdf3e8dfd8e1485f9a0','login','createpiber','2018-09-18 22:20:14',NULL),('d0da03e9a93a5408685252bf05e84403ec7c0d80','testclient',NULL,'2033-05-02 22:25:46','create read'),('d6b2bdead0355595fd480d4307567ebbe0f93c78','login','createpiber','2018-09-18 19:18:02','email profile openid'),('e69b39df27c96be238da32d2acea130c1d0d9d3a','pibernetwork','createpiber','2018-10-13 22:13:15','openid profile email'),('e9f24b8c7de5753ca3a8d672c8f1159b7ee103cb','testclient',NULL,'2033-04-25 15:06:45','create read'),('f32a46b628721f57d0de87953681d7b374671014','login','createpiber','2018-09-18 21:35:54',NULL),('f5bcec8f65a2480bec5bb0d88f7070e8459c53e2','login','createpiber','2018-09-18 21:13:58',NULL),('f6fc27f0bb3027dfab189bd5d4ac64d494451832','testclient',NULL,'2033-05-02 23:06:59','create read'),('f8ebe4f854c953588088ce9bec7e7bfe4c363ef4','pibernetwork','createpiber','2018-10-13 21:57:21','openid email profile'),('fa9132440959e1b7f60d0580513c8bda4dc3aa78','pibernetwork','createpiber','2018-10-13 21:57:26','openid email profile');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_authorization_codes`
--

DROP TABLE IF EXISTS `oauth_authorization_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  `id_token` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`authorization_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_authorization_codes`
--

LOCK TABLES `oauth_authorization_codes` WRITE;
/*!40000 ALTER TABLE `oauth_authorization_codes` DISABLE KEYS */;
INSERT INTO `oauth_authorization_codes` VALUES ('29a5ec8e80d6eeb5e0a48237cbd2c91c244cb807','login','createpiber','http://login.local/authorize','2018-09-18 23:20:02','openid profile email','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6OTA5MCIsInN1YiI6ImNyZWF0ZXBpYmVyIiwiYXVkIjoibG9naW4iLCJpYXQiOjE1MzczMTI3NzIsImV4cCI6MTUzNzMxNjM3MiwiYXV0aF90aW1lIjoxNTM3MzEyNzcyfQ.'),('581ef344327ad666f1525c851a4433e3bdab2c69','login','createpiber','http://login.local/authorize','2018-09-18 23:24:18','openid profile email','eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6OTA5MCIsInN1YiI6ImNyZWF0ZXBpYmVyIiwiYXVkIjoibG9naW4iLCJpYXQiOjE1MzczMTMwMjgsImV4cCI6MTUzNzMxNjYyOCwiYXV0aF90aW1lIjoxNTM3MzEzMDI4fQ.');
/*!40000 ALTER TABLE `oauth_authorization_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) NOT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(2000) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES ('1234','$2y$14$VvBq0z0dROWAUJ6svnlAK.bNUk1Ed5xtlfWDeOaRcglAFtrT2IEc2','1234123',NULL,NULL,NULL),('234','$2y$14$Qg.X83L/P83oDA.SIoy.e.0G2VXXW64BxWczecU4N7z3PfT3Yxf9S','21341234',NULL,NULL,NULL),('cursos-webflow',NULL,'http://login.local/authorize',NULL,'openid profile email create read',NULL),('login',NULL,'http://login.local/authorize',NULL,'openid profile email create read',NULL),('pibernetwork',NULL,'http://login.local/authorize',NULL,'openid profile email create read',NULL),('store01','$2y$14$eFG0hgIDw0Q1QKlJPN70YeqPCCvV20mC1rhJJuRDgDeaJDvWN6RTS','http://login.local/authorize',NULL,'create read',NULL),('testclient','$2y$14$f3qml4G2hG6sxM26VMq.geDYbsS089IBtVJ7DlD05BoViS9PFykE2','/oauth/receivecode',NULL,'create read',NULL);
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_jwt`
--

DROP TABLE IF EXISTS `oauth_jwt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_jwt`
--

LOCK TABLES `oauth_jwt` WRITE;
/*!40000 ALTER TABLE `oauth_jwt` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_jwt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_public_keys`
--

DROP TABLE IF EXISTS `oauth_public_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_public_keys` (
  `client_id` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) DEFAULT NULL,
  `private_key` varchar(2000) DEFAULT NULL,
  `encryption_algorithm` varchar(100) DEFAULT 'RS256'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_public_keys`
--

LOCK TABLES `oauth_public_keys` WRITE;
/*!40000 ALTER TABLE `oauth_public_keys` DISABLE KEYS */;
INSERT INTO `oauth_public_keys` VALUES (NULL,'-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv1byVn/CcLMPYwed/AV6\nUasfPhAE0KJ3GoBsZ5E5f9rxLA39tIhNxeBKrUCsQwklfDg/IwJ7xvdxvnhT6Edk\nXt+ESIdmbaB97Flw4GxGi/VfjWblFR6SKpd/JE/H5PKOHrdvhKyQY6rEr171RwKG\nK42/oeEbsz94UOpQ12wyxt0ML+OBXhuSRuMu93JhfpVT82ndD0ck/nt+LUG2wulR\n5CUwDkoPRTGLaweY9nRxadzkCsZ6uz1KFYgRo78/hR4MMkCbF4Lp5aTws0T7Rs5f\nspjdkZUD4OHBhwRG8LcPHJ9JsR6L+Nt2kboexh0+XyDk/bAr8a/6VKgzIJYSdUsc\nNwIDAQAB\n-----END PUBLIC KEY-----','-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC/VvJWf8Jwsw9j\nB538BXpRqx8+EATQoncagGxnkTl/2vEsDf20iE3F4EqtQKxDCSV8OD8jAnvG93G+\neFPoR2Re34RIh2ZtoH3sWXDgbEaL9V+NZuUVHpIql38kT8fk8o4et2+ErJBjqsSv\nXvVHAoYrjb+h4RuzP3hQ6lDXbDLG3Qwv44FeG5JG4y73cmF+lVPzad0PRyT+e34t\nQbbC6VHkJTAOSg9FMYtrB5j2dHFp3OQKxnq7PUoViBGjvz+FHgwyQJsXgunlpPCz\nRPtGzl+ymN2RlQPg4cGHBEbwtw8cn0mxHov423aRuh7GHT5fIOT9sCvxr/pUqDMg\nlhJ1Sxw3AgMBAAECggEAYpdBjZKNNRb+wa9GF3sXGQfMtGT6pipgppYvxXnOH/RH\nusSGysRFFsXIlNczK+OFnik5VyfLca1Evbkwuwo2TLBtcBXckHZXDDbV87226B0K\nHEk9lxFGjY28ZJB7Vpwo4OjyHHHygHiYmiiax/kj+0Mx0A63ADifHUpvV2EYFOEh\nkjqtNeAVL9dRNjRidN+LvGY6VqZsh4U3ekHI8vjc/qa3WvG2/c7XE3Qj4p8nmcjX\nEfG9A4G/baxJeCk/Ah/zW871Kh4PpC7HLLByrji8SGgkMbC6k4wW2gUjoJEuStdZ\nwaS6IDY7RvQnc6ZqWOZQy5KOQ6qSwghp5W1MKP4WYQKBgQD7uIECHeeo8iAu0LzA\nWD7har0zx05XfGJDDSz+iIOgo6YmsmbS6C1Es9UeDlm0kBq3Y/3GPZEoHdS26cEc\ntS4qIHYZTKJW0b4modjrLTTxOYPphupylNwbhA/i7G2lvNhrV8zAGu5pquG3lZYc\nt2x0ORMdqiwQChjd13/FAw6ERwKBgQDCl6mUexLvGcskYF99duR6ldHuWB78imbO\nduVG1afllECdgA5AGrccZ39TRoJh31uB1RMU7QfBkrHSbQK6joGnIyQwgHIyvNLs\ns6Q2QdvCv7uXq2HfQ70YmKXDm1SWQrTrhFpL2yIse4D8BQWgbar7WRkoGSjCT/5+\nIAcD02RQkQKBgQCniDrEj+a4+L0Wq3KUiacAHCxko8euTmiPu9swZOdGKOTSaHYq\n9L/8M7wpzt9BX4+IcxJZuGw9yOzj+FvAoY1iGEzBtdFt+xaVW66lh2bvDZqP+G1d\naVm9Ln3sR/MKFc/sUXlCeionRcYIi5Rm9GVITRw96B5/Zlj6fQzasJgezQKBgHN4\nHZfPJGAfdSzjxyYjK9cWGhmNzu1aj0DKyqiq1dNkkqcjOtGNSMUhISvgNjQvnxQ+\nxPDw6l5SVxSfvTPIpmTwdGQXhCxZYew1eSb9E5PPxFhuyUf74TS8N/kHrjzv3n22\n7wH+SZwNuW9S55DYItK9fzENJOORJlnfVWpOwUMRAoGBAKt7csDB83ilQWYt2RK0\ncampKug9WsMWP9B3LCN/IWiICeMcv3ABlpaxlVaB+PLXwaW6czYWn+Z93PhZUdnz\nkbOxkvnLg4UuqLnnhmlLcP2G07b+ILiLsgU7PZ4IxFmzFkIu06ftQ8PPHUquzbZF\nDt4GStVFN52gOBy8hFC3O0AQ\n-----END PRIVATE KEY-----','RS256');
/*!40000 ALTER TABLE `oauth_public_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `expires` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `scope` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`refresh_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('040dc848c0706a7494af6d89095be088a53963f6','login','createpiber','2018-10-02 19:55:50',NULL),('19738767820d3cd48d5e393b8ab5b4b3d30760c6','pibernetwork','createpiber','2018-10-27 20:57:21','openid email profile'),('1bb48d3142a84b92b9cfa00cb825d118a3023f97','login','createpiber','2018-10-02 18:21:03','email profile openid'),('26de199879c54f90414002ed78fd7974d3f83862','login','createpiber','2018-10-02 21:56:32','openid profile email'),('3039f00d01836938581d05dd6bedae428d30e1a7','login','createpiber','2018-10-02 20:12:00',NULL),('3f53c31a37b32265d605781ee8f2e42a07ce6c3d','login','createpiber','2018-10-02 20:13:58',NULL),('50a30263761afefa498080194dc32865b89d8a1a','cursos-webflow','createpiber','2018-10-27 18:08:54','openid profile'),('5701757b3d720fc2d0829a87415140a04313b646','pibernetwork','createpiber','2018-10-27 21:31:01','openid profile email'),('66fd261ff9821c25b570cffa444413d62f283421','login','createpiber','2018-10-02 18:18:02','email profile openid'),('68ee3be5bc8ebf2aa0c97702262311130a3ee500','login','createpiber','2018-10-02 20:41:56',NULL),('6f662d04742d00606051e322e339dc7fd11b5ca8','login','createpiber','2018-10-02 18:17:56','email profile'),('7fb4a3076e4833ab72fefb9b349507a96f707012','login','createpiber','2018-10-02 21:22:04',NULL),('82f848fc3843c5831bd954acb804f2ae4109e39a','login','createpiber','2018-10-02 21:15:52',NULL),('8969853c7e299ae4eb89a3e8cf08fb03c7644b06','login','createpiber','2018-10-02 21:42:45',NULL),('8bf171e85355a2fc5ce411c96feab7e85650f170','pibernetwork','createpiber','2018-10-27 21:13:15','openid profile email'),('985de9bfb2ec83d98af27505a7131813aba02d4b','pibernetwork','createpiber','2018-10-27 18:31:57','openid profile email'),('9b1456dac3bd241a2b40c75a4222a58a79677a05','login','createpiber','2018-10-02 20:12:43',NULL),('a10ba9a86ab121c11493a148fb45025675a7757b','login','createpiber','2018-10-02 20:35:54',NULL),('a302ca025dc711e27960f033d69171077c12289c','login','createpiber','2018-10-02 18:17:04',NULL),('b8c552efef4f909409e8e5ae00b37b1847b5dba2','login','createpiber','2018-10-02 20:39:10',NULL),('bdb04dd68c998840c5bbc5024b94f90e5655ae8a','login','createpiber','2018-10-02 18:17:47','email'),('d1b4e27783bd6b72a237962d0a9ab7979b4e5f2e','login','createpiber','2018-10-02 21:20:14',NULL),('d35176d04226aec1c43d45ebc10c8f404647e5b1','pibernetwork','createpiber','2018-10-27 20:57:26','openid email profile'),('da793b965a25b1174c2d2f96b6d6211ff7e5df6e','login','createpiber','2018-10-02 18:18:07','email profile openid'),('e39b863cf1607ec57f30bf60cbfcd25554a01f24','login','createpiber','2018-10-02 22:27:16','openid profile email'),('f6dbb062d440eb29f544bf3925fd2571fad4c8c7','pibernetwork','createpiber','2018-10-27 20:57:07','openid email profile');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_scopes`
--

DROP TABLE IF EXISTS `oauth_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_scopes` (
  `type` varchar(255) NOT NULL DEFAULT 'supported',
  `scope` varchar(2000) DEFAULT NULL,
  `client_id` varchar(80) DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_scopes`
--

LOCK TABLES `oauth_scopes` WRITE;
/*!40000 ALTER TABLE `oauth_scopes` DISABLE KEYS */;
INSERT INTO `oauth_scopes` VALUES ('supported','openid','login',NULL),('supported','first_name','login',NULL),('supported','last_name','login',NULL),('supported','create','login',NULL),('supported','read',NULL,NULL),('supported','read',NULL,NULL);
/*!40000 ALTER TABLE `oauth_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_users`
--

DROP TABLE IF EXISTS `oauth_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(2000) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_users`
--

LOCK TABLES `oauth_users` WRITE;
/*!40000 ALTER TABLE `oauth_users` DISABLE KEYS */;
INSERT INTO `oauth_users` VALUES ('createpiber','$2y$14$DfQHQ.4S23Y08Y3FVLLwqOlFetPeyfceYyfrJDibzThSeSAEyU.7W','myfirstname','mylastname','createpiber@gmail.com',1),('guest','$2y$14$sM5Nk7lTaUUJShQewXtwmOXgWLGqgKWGFyvcfwUfOGkn9jqToUxzy','myfirstname','mylastname','guest@gmail.com',1),('mauriciopiber','$2y$14$RNnb80dmPPIGpuoQyV5c4uLKKB6nMt7lH92JiETe7j8avyuIVpSYa','myfirstname','mylastname','mauriciopiber@gmail.com',1),('readpiber','$2y$14$nYeUdEYwuqiYYHjdk5reZeaD/W.REAvDEvyctR0yvLwJ.1kc54G8.','myfirstname','mylastname','readpiber@gmail.com',1);
/*!40000 ALTER TABLE `oauth_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_role`),
  UNIQUE KEY `name` (`name`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `id_parent` (`id_parent`),
  CONSTRAINT `role_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_ibfk_3` FOREIGN KEY (`id_parent`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rule`
--

DROP TABLE IF EXISTS `rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rule` (
  `id_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_action` int(11) NOT NULL,
  `id_controller` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rule`),
  KEY `id_action` (`id_action`),
  KEY `id_controller` (`id_controller`),
  KEY `id_role` (`id_role`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  CONSTRAINT `rule_ibfk_1` FOREIGN KEY (`id_action`) REFERENCES `action` (`id_action`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_2` FOREIGN KEY (`id_controller`) REFERENCES `controller` (`id_controller`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rule_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rule`
--

LOCK TABLES `rule` WRITE;
/*!40000 ALTER TABLE `rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(150) NOT NULL,
  `username` varchar(50) NOT NULL,
  `state` int(11) NOT NULL,
  `uid` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `email` (`email`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'mptest1@gmail.com','mypassword','1',3,'2',NULL,NULL,NULL,NULL,NULL),(3,'mptest2@gmail.com','mypassword','1',3,'2',NULL,NULL,NULL,NULL,NULL),(4,'319mptest2@gmail.com','mypassword','65test',3,'2',NULL,NULL,NULL,NULL,NULL),(5,'194mptest2@gmail.com','mypassword','360test',3,'2',NULL,NULL,NULL,NULL,NULL),(6,'373mptest2@gmail.com','mypassword','433test',3,'2',NULL,NULL,NULL,NULL,NULL),(7,'241mptest2@gmail.com','mypassword','328test',3,'2',NULL,NULL,NULL,NULL,NULL),(8,'327mptest2@gmail.com','mypassword','214test',3,'2',NULL,NULL,NULL,NULL,NULL),(9,'testlogin1@gmail.com','$2y$14$RVMuN8zwzCGhvH27.5Cq.Ok4sacuamK1VJrn.WN/b9Z9VtWCFIwhC','52test',3,'2',NULL,NULL,NULL,NULL,NULL),(11,'testlogin3@gmail.com','$2y$14$zHvdO2cHq0nSmdJ23qO8veggD5RquQdic5MTm.g2k0O6X2Doc9Rri','484test',3,'2',NULL,NULL,NULL,NULL,NULL),(16,'testlogin4@gmail.com','$2y$14$3JkNfohOdw1y0V3h9zx2/espgmsvfkxzBgmX24pB0Zt0rzWnC/sBa','59test',3,'2',NULL,NULL,NULL,NULL,NULL),(17,'testlogin5@gmail.com','$2y$14$eN1//w6L/W/ZkOJwDUrsh.zF0VMTC1JQcmmL9h/tV2bWLfCpUo5bC','456test',3,'2',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-08 22:12:22
