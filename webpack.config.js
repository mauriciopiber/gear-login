const merge = require('webpack-merge');
const common = require('./webpack.config.common.js');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

const config = merge(common, {
  watch: false,
  optimization: {
   minimizer: [
    new UglifyJSPlugin({
      parallel: true,
      uglifyOptions: {
        ecma: 6,
        compress: false // hangs without this
      },
      cache: path.join(__dirname, 'webpack-cache/uglify-cache'),
    })
   ],
  },
});

module.exports = config;
