const webpack = require('webpack');
const path = require('path');
//const merge = require('webpack-merge');
//const common = require('./webpack.common.js');

// const CleanWebpackPlugin = require('clean-webpack-plugin');
//const HtmlWebPackPlugin = require('html-webpack-plugin');
//const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PUBLIC_DIR = path.resolve(__dirname, '../public');
const DIST_DIR = path.resolve(__dirname, '../public/dist');
const SRC_DIR = path.resolve(__dirname, '../app');
const ADMIN_DIR = path.resolve(__dirname, '../node_modules/pbr-admin')

const config = {
  resolve: {
    extensions: ['.jsx', '.js'],
    modules: ['../app', '../node_modules', '../src'],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [SRC_DIR, ADMIN_DIR],
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
};

module.exports = config;
