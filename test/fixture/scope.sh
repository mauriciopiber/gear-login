set -e

BASEDIR=$(dirname "$0")

API="gear-login/pbr-food/group-controller"

fixture=(
  '{"code": "A", "description": "Mistura entre sí e com um do B"}'
  '{"code": "B", "description": "Não se misturam entre sí"}'
  '{"code": "C", "description": "Mistura entre sí e com um do B desde que não preparado em gordura."}'
  '{"code": "D", "description": "Não se misturam entre sí nem com outro grupo"}'
  '{"code": "E", "description": "Leite"}'
  '{"code": "F", "description": "Creme de Leite"}'
)

source $BASEDIR/rest-fixture.sh

restFixture
