#!/bin/bash
set -e

BASEDIR=$(dirname "$0")

$BASEDIR/../../vendor/bin/db-restart
$BASEDIR/../../vendor/bin/db-migrate

data=(
  "scope"
)


for i in "${data[@]}"
do
  echo ""
  echo ""
  echo "Start test $i"
  echo ""
  echo ""
  $BASEDIR/$i.sh
  echo ""
done
