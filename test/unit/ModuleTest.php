<?php
namespace GearTest\LoginTest;

use Gear\Login\Module;

class ModuleTest extends \PHPUnit_Framework_TestCase
{
    public function testLocation()
    {
        $this->assertStringEndsWith(
            '/src',
            Module::LOCATION
        );
    }
}
