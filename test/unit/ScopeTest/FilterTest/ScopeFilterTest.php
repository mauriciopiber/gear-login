<?php
namespace GearTest\LoginTest\ScopeTest\FilterTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Scope\Filter\ScopeFilter;
use Zend\Db\Adapter\Adapter;

/**
 * @group Filter
 */
class ScopeFilterTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dbAdapter = $this->prophesize(Adapter::class);
        $this->filter = new ScopeFilter($this->dbAdapter->reveal());
    }

    public function testClassExists()
    {
        $this->assertInstanceOf(
            'Gear\Login\Scope\Filter\ScopeFilter',
            $this->filter->getInputFilter()
        );
    }
}
