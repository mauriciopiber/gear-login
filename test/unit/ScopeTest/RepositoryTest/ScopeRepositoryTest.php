<?php
namespace GearTest\LoginTest\ScopeTest\RepositoryTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Scope\Repository\ScopeRepository;

/**
 * @group Repository
 */
class ScopeRepositoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->repository = new ScopeRepository(null);
    }

    public function testClassExists()
    {
        $this->assertInstanceOf('Gear\Login\Scope\Repository\ScopeRepository', $this->repository);
    }
}
