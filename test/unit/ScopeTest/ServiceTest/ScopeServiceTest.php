<?php
namespace GearTest\LoginTest\ScopeTest\ServiceTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Scope\Service\ScopeService;

/**
 * @group Service
 */
class ScopeServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->scopeFilter = $this->prophesize('Gear\Login\Scope\Filter\ScopeFilter');
        $this->scopeRepository = $this->prophesize('Gear\Login\Scope\Repository\ScopeRepository');

        $this->service = new ScopeService(
            $this->scopeFilter->reveal(),
            $this->scopeRepository->reveal()
        );
    }

    public function testClassExists()
    {
        $this->assertInstanceOf('Gear\Login\Scope\Service\ScopeService', $this->service);
    }
}
