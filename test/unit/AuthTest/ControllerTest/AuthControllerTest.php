<?php
namespace GearTest\LoginTest\AuthTest\ControllerTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Auth\Controller\AuthController;
use Gear\Login\User\Service\UserService;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;
use Zend\Http\PhpEnvironment\Response;
use Zend\View\Model\JsonModel;
use Zend\Stdlib\Parameters;
use Zend\Authentication\AuthenticationService;

class AuthControllerTest extends TestCase
{
    public function setUp() {

        $this->auth = $this->prophesize(AuthenticationService::class);
        $this->user = $this->prophesize(UserService::class);

        $this->controller = new AuthController(
            $this->auth->reveal(),
            $this->user->reveal()
        );

        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'AuthController'));
        $this->event      = new MvcEvent();
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
    }

    public function testGetIdentityWithoutLogin()
    {


      $this->request->setMethod('GET');
      //$this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(200, $response->getStatusCode());
      $this->assertInstanceOf(JsonModel::class, $result);
    }

    public function testGetIdentityWithLogin()
    {
        $this->auth->getIdentity()->willReturn(['email' => 'myemail@gmail.com'])->shouldBeCalled();
        $this->request->setMethod('GET');
        //$this->routeMatch->setParam('action', 'login');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertInstanceOf(JsonModel::class, $result);

        $result = $result->getVariables();
        $user = $result['payload'];

        $this->assertEquals($user['email'], 'myemail@gmail.com');
    }

    public function testChangePassword()
    {
        $this->auth->getIdentity()->willReturn(['email' => 'myemail@gmail.com'])->shouldBeCalled();

        $this->user
          ->changePassword(['email' => 'myemail@gmail.com'], 'myoldpass', 'newPass', 'newPass')
          ->willReturn(['error' => false, 'id' => 50])
          ->shouldBeCalled();


        $this->routeMatch->setParam('action', 'change-password');

        $this->request->setPost(
            new Parameters([
                'oldPassword' => 'myoldpass',
                'newPassword' => 'newPass',
                'confirmPassword' => 'newPass'
            ])
        );
        $this->request->setMethod('POST');
        //$this->routeMatch->setParam('')
        //$this->routeMatch->setParam('action', 'login');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertInstanceOf(JsonModel::class, $result);
    }
}
