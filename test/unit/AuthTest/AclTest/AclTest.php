<?php
namespace GearTest\LoginTest\AuthTest\AclTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Auth\Acl\AclAuth;
use Zend\Authentication\AuthenticationService;

class AuthAclTest extends TestCase {

  public function setUp() {
    $this->auth = $this->prophesize(AuthenticationService::class);

    $this->acl = new AclAuth($this->auth->reveal());

    $roles = require __DIR__.'/../../../../config/rule/roles.php';
    $resources = require __DIR__.'/../../../../config/rule/resources.php';
    $rules = require __DIR__.'/../../../../config/rule/rules.php';

    $this->acl->setRoles($roles);

    $this->acl->setResources($resources);

    $this->acl->setRules($rules);
  }

  public function testGuestAccessLogin() {
    $this->assertTrue($this->acl->isValid('guest', 'login'));
  }

  public function testGuestAccessRegister() {
    $this->assertTrue($this->acl->isValid('guest', 'register'));
  }

  public function testUserListDenyGuest() {
    $this->assertFalse($this->acl->isValid('guest', 'home'));
  }

  public function testUserListAllowMember() {
    $this->assertTrue($this->acl->isValid('member', 'home'));
  }

  public function testUserListAllowAdmin() {
    $this->assertTrue($this->acl->isValid('admin', 'home'));
  }

  //
  // public function testAdminRejectAccessLogin() {
  //   $this->assertFalse($this->acl->isValid('member', 'login'));
  //   $this->assertFalse($this->acl->isValid('admin', 'login'));
  // }

  public function testGuestDoNotAccessHome() {
    $this->assertFalse($this->acl->isValid('guest', 'home'));
  }

  public function testMemberAccessHome() {
    $this->assertTrue($this->acl->isValid('member', 'home'));
    $this->assertTrue($this->acl->isValid('admin', 'home'));
  }

  public function testOnlyAdminAccessUserList() {
    $this->assertFalse($this->acl->isValid('guest', 'api/user:LIST'));
    $this->assertFalse($this->acl->isValid('member', 'api/user:LIST'));
    $this->assertTrue($this->acl->isValid('admin', 'api/user:LIST'));
    //$this->assertTrue($this->acl->isValid('master', 'api/user:LIST'));
  }
}
