<?php
namespace GearTest\LoginTest\UserTest\ServiceTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\User\Service\UserService;
use Gear\Login\User\Filter\UserFilter;
use Gear\Login\User\Repository\UserRepository;
use Gear\Login\UserRole\Service\UserRoleService;
use Zend\Crypt\Password\Bcrypt;

/**
 * @group Service
 */
class UserServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->userFilter = $this->prophesize(UserFilter::class);
        $this->userRepository = $this->prophesize(UserRepository::class);
        $this->userRoleService = $this->prophesize(UserRoleService::class);

        $this->service = new UserService(
            $this->userFilter->reveal(),
            $this->userRepository->reveal(),
            $this->userRoleService->reveal()
        );
    }

    public function testFindOneByEmail()
    {
        $this->userRepository
          ->findOneByEmail('myemail@gmail.com')
          ->willReturn(['email' => 'myemail@gmail.com'])
          ->shouldBeCalled();

        $user = $this->service->findOneByEmail('myemail@gmail.com');
        $this->assertEquals($user['email'], 'myemail@gmail.com');
    }

    /**
     * @dataProvider getPass
     */
    public function testChangePasswordSuccesful(
        $identity,
        $oldPassword,
        $newPassword,
        $confirmPassword,
        $passwordHash
    ) {
        $this->userFilter->getChangePasswordFilter()->willReturn($this->userFilter)->shouldBeCalled();

        $values = [
            'password' => $newPassword,
            'confirmPassword' => $confirmPassword
        ];

        $this->userFilter->setData($values)->shouldBeCalled();

        $this->userFilter->isValid()->willReturn(true)->shouldBeCalled();
        $this->userFilter->getMessages()->shouldNotBeCalled();

        $bcrypt = $this->prophesize(Bcrypt::class);
        $bcrypt->verify($oldPassword, $passwordHash)->willReturn(true)->shouldBeCalled();
        $this->service->setBcrypt($bcrypt->reveal());

        $this->userRepository
          ->findOneByEmail('myemail@gmail.com')
          ->willReturn(['email' => 'myemail@gmail.com', 'password' => $passwordHash])
          ->shouldBeCalled();
        $this->userRepository->update($identity['username'], ['password' => $newPassword])->willReturn(5)->shouldBeCalled();

        $this->userFilter->getValues()->willReturn($values)->shouldBeCalled();


        $password = $this->service->changePassword($identity, $oldPassword, $newPassword, $confirmPassword);

        $this->assertEquals($password['error'], false);
        $this->assertEquals($password['id'], 5);
    }

    /**
     * @dataProvider getPass
     */
    public function testChangePasswordErrorWhenOldPasswordCheckFail(
        $identity,
        $oldPassword,
        $newPassword,
        $confirmPassword,
        $passwordHash
    ) {
        $bcrypt = $this->prophesize(Bcrypt::class);
        $bcrypt->verify($oldPassword, $passwordHash)->willReturn(false)->shouldBeCalled();
        $this->service->setBcrypt($bcrypt->reveal());


        $this->userRepository
          ->findOneByEmail('myemail@gmail.com')
          ->willReturn(['email' => 'myemail@gmail.com', 'password' => $passwordHash])
          ->shouldBeCalled();


        $password = $this->service->changePassword($identity, $oldPassword, $newPassword, $confirmPassword);

        $this->assertEquals($password['error'], true);
        $this->assertEquals($password['messages'], ['Wrong password']);

    }

    /**
     * @dataProvider getPass
     */
    public function testChangePasswordErrorWhenNewPasswordCheckFail(
        $identity,
        $oldPassword,
        $newPassword,
        $confirmPassword,
        $passwordHash
    ) {

        $this->userFilter->getChangePasswordFilter()->willReturn($this->userFilter)->shouldBeCalled();

        $this->userFilter->setData([
            'password' => $newPassword,
            'confirmPassword' => $confirmPassword
        ])->shouldBeCalled();

        $this->userFilter->isValid()->willReturn(false)->shouldBeCalled();

        $messages = [
            'wrong'
        ];

        $this->userFilter->getMessages()->willReturn($messages)->shouldBeCalled();


        $bcrypt = $this->prophesize(Bcrypt::class);
        $bcrypt->verify($oldPassword, $passwordHash)->willReturn(true)->shouldBeCalled();
        $this->service->setBcrypt($bcrypt->reveal());


        $this->userRepository
          ->findOneByEmail('myemail@gmail.com')
          ->willReturn(['email' => 'myemail@gmail.com', 'password' => $passwordHash])
          ->shouldBeCalled();


        $password = $this->service->changePassword($identity, $oldPassword, $newPassword, $confirmPassword);

        $this->assertEquals($password['error'], true);
        $this->assertEquals($password['messages'], $messages);
    }

    public function testClassExists()
    {
        $this->assertInstanceOf('Gear\Login\User\Service\UserService', $this->service);
    }


    public function getPass()
    {
        return [
            [
                [
                    'username' => 5,
                    'email' => 'myemail@gmail.com',
                ],
                'aeaeaexxx',
                '654321',
                '654321',
                '$2y$14$uCwuwq1CABXEsacekXWFtOL7J/dvXpYRxf6yusUtxVwlqPJtS932q'
            ]
        ];
    }

}
