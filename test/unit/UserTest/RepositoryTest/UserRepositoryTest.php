<?php
namespace GearTest\LoginTest\UserTest\RepositoryTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\User\Repository\UserRepository;
use Zend\Db\Adapter\Adapter;

/**
 * @group Repository
 */
class UserRepositoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->adapter = $this->prophesize(Adapter::class);

        $this->repository = new UserRepository($this->adapter->reveal());
    }

    public function testClassExists()
    {
        $this->assertInstanceOf('Gear\Login\User\Repository\UserRepository', $this->repository);
    }
}
