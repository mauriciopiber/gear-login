<?php
namespace GearTest\LoginTest\UserTest\FilterTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\User\Filter\UserFilter;
use Zend\Db\Adapter\Adapter;

/**
 * @group Filter
 */
class UserFilterTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dbAdapter = $this->prophesize(Adapter::class);
        $this->filter = new UserFilter($this->dbAdapter->reveal());
    }

    public function testClassExists()
    {
        $this->assertInstanceOf(
            'Gear\Login\User\Filter\UserFilter',
            $this->filter->getInputFilter()
        );
    }
}
