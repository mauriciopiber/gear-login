<?php
namespace GearTest\LoginTest\ControllerTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Admin\Login\Controller\LoginController;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;
use GearBaseTest\Assertions\AclAssertTrait;
use Gear\Login\Admin\Login\Service\LoginService;
use Gear\Login\Admin\Login\Filter\LoginFilter;
use Gear\Login\Admin\Login\Form\LoginForm;
use Zend\Mvc\Plugin\Prg\PostRedirectGet;
use Zend\Http\PhpEnvironment\Response;

/**
 * @group LoginController
 * @group Controller
 */
class LoginControllerTest extends TestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    public function setUp()
    {
        parent::setUp();

        $this->form = $this->prophesize(LoginForm::class);
        $this->filter = $this->prophesize(LoginFilter::class);
        $this->service = $this->prophesize(LoginService::class);

        $this->controller = new LoginController(
          $this->service->reveal(),
          $this->form->reveal(),
          $this->filter->reveal()
        );

        $this->prg = $this->prophesize(PostRedirectGet::class);

        $this->controller->getPluginManager()->setService('prg', $this->prg->reveal());

        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'LoginController'));
        $this->event      = new MvcEvent();
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
    }

    /**
     * @group x1
     */
    public function testEnterLoginRedirectToHomeIfLoginHasBeenDoneBefore()
    {
        $this->service->getIdentity()->willReturn(true)->shouldBeCalled();
        $this->routeMatch->setParam('action', 'login');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        //var_dump($response);
        $this->assertEquals(302, $response->getStatusCode());

        $redirectUri = $response->getHeaders()->get('Location')->getUri();
        $this->assertEquals($redirectUri, '/');
    }

    public function testEnterHomeRedirectToLoginIfLoginHasntDoneBefore()
    {
      $this->service->getIdentity()->willReturn(false)->shouldBeCalled();
      $this->routeMatch->setParam('action', 'home');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      //var_dump($response);
      $this->assertEquals(302, $response->getStatusCode());

      $redirectUri = $response->getHeaders()->get('Location')->getUri();
      $this->assertEquals($redirectUri, '/login');
    }

    public function testPostToLoginWillReturnGetResponse()
    {
      //$prg = new \Zend\Mvc\Plugin\Prg\PostRedirectGet();
      $this->prg->setController($this->controller)->shouldBeCalled();

      $this->response = $this->prophesize(Response::class);
      //$this->response->getStatusCode()->willReturn(303);
      $this->prg->__invoke(
        '/login',
        true
      )->willReturn($this->response->reveal());


      $this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(200, $response->getStatusCode());
      $this->assertInstanceOf(Response::class, $result);
    }

    /**
    * @group login-1
    */
    public function testEnterLoginPageFirstTime()
    {
      //$prg = new \Zend\Mvc\Plugin\Prg\PostRedirectGet();
      $this->prg->setController($this->controller)->shouldBeCalled();

      $this->response = $this->prophesize(Response::class);
      //$this->response->getStatusCode()->willReturn(303);
      $this->prg->__invoke(
        '/login',
        true
      )->willReturn(false)->shouldBeCalled();

      $this->form->setData([
          'response_type' => null,
          'client_id' => null,
          'redirect_uri' => null,
          'state' => null,
          'scope' => null
      ])->shouldBeCalled();

      $this->form->isValid()->willReturn(true)->shouldBeCalled();

      $this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(200, $response->getStatusCode());
      //$this->assertInstanceOf(Response::class, $result);
    }

    /**
     * @group x2
     */
    public function testIfFormIsInvalidReturnValidation()
    {
      $this->filter->getInputFilter()->willReturn($this->filter)->shouldBeCalled();
      $this->form->setInputFilter($this->filter)->shouldBeCalled();
      $this->form->isValid()->willReturn(false)->shouldBeCalled();
      $this->form->setData(null)->shouldBeCalled();
      $this->form->getData()->willReturn(null)->shouldBeCalled();

      $this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(200, $response->getStatusCode());
      //var_dump($response);
    }

    public function prepareForm($formData, $isValid = true)
    {
      $this->filter->getInputFilter()->willReturn($this->filter)->shouldBeCalled();
      $this->form->setInputFilter($this->filter)->shouldBeCalled();
      $this->form->isValid()->willReturn($isValid)->shouldBeCalled();
      $this->form->setData($formData)->shouldBeCalled();
      $this->form->getData()->willReturn($formData)->shouldBeCalled();
    }

    public function preparePrg($result)
    {
      $this->prg->setController($this->controller)->shouldBeCalled();
      $this->prg->__invoke(
        '/login',
        true
      )->willReturn($result)->shouldBeCalled();
    }

    /**
     * @group x3
     */
    public function testIfLoginIsInvalidReturnValidation()
    {
      $formData = [
        'email' => 'myemail@gmail.com',
        'password' => '..123..'
      ];

      $this->prepareForm($formData, true);
      $this->preparePrg($formData);

      $this->service->getIdentity()->willReturn(false)->shouldBeCalled();

      $loginError = 'Login Error';

      $this->service->login($formData)->willReturn([
        'error' => true,
        'message' => $loginError,
      ])->shouldBeCalled();

      $this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(200, $response->getStatusCode());
      $this->assertEquals($result->getVariable('error'), $loginError);
    }

    /**
     * @group x4
     */
    public function testValidLoginForAuthorization()
    {
      $formData = [
        'email' => 'myemail@gmail.com',
        'password' => '..123..',
        'client_id' => 'my_client',
        'response_type' => 'token',
        'redirect_uri' => 'login.local//authorize',
        'state' => 123,
        'scope' => 'create read'
      ];

      $this->prepareForm($formData, true);
      $this->preparePrg($formData);

      $this->service->getIdentity()->willReturn(false)->shouldBeCalled();

      $loginError = 'Login Error';

      $this->service->login($formData)->willReturn([
        'error' => false,
      ])->shouldBeCalled();

      $this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(302, $response->getStatusCode());
      $redirectUri = $response->getHeaders()->get('Location')->getUri();
      $expected = '/oauth/authorize?client_id=my_client&response_type=token&redirect_uri=login.local'
                . '%2F%2Fauthorize&state=123&scope=create+read';
      $this->assertEquals($expected, $redirectUri);
      //$this->assertEquals($result['error'], $loginError);
    }

    /**
     * @group x5
     */
    public function testValidLoginForHome()
    {
      $formData = [
        'email' => 'myemail@gmail.com',
        'password' => '..123..'
      ];

      $this->prepareForm($formData, true);
      $this->preparePrg($formData);

      $this->service->getIdentity()->willReturn(false)->shouldBeCalled();

      //$loginError = 'Login Error';

      $this->service->login($formData)->willReturn([
        'error' => false,
      ])->shouldBeCalled();

      $this->routeMatch->setParam('action', 'login');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(302, $response->getStatusCode());
      $redirectUri = $response->getHeaders()->get('Location')->getUri();
      $this->assertEquals($redirectUri, '/');
      //$this->assertEquals($result['error'], $loginError);
    }

    /*
    public function testSubmitInvalidLoginData()
    {
      $this->prg->__invoke(
        '/gear-login/api/login/login',
        true
      )->willReturn(false);

      $this->routeMatch->setParam('action', 'login');
      $this->request->setMethod('POST');
      $result = $this->controller->dispatch($this->request);
      $response = $this->controller->getResponse();
      $this->assertEquals(200, $response->getStatusCode());
      var_dump($result);
      $this->assertInstanceOf(Response::class, $result);

    }
    */
/*
    public function test404()
    {
        $this->routeMatch->setParam('action', 'action-that-doesnt-exist');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }
*/
}
