<?php
namespace GearTest\AdminTest\LoginTest\ControllerTest;

use PHPUnit\Framework\TestCase;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceManager;
use Gear\Login\Admin\Login\Controller\LoginControllerFactory;
use Gear\Login\Admin\Login\Controller\LoginController;

/**
 * @group Gear\Login
 * @group LoginController
 * @group Controller
 */
class LoginControllerFactoryTest extends TestCase
{
    public function testLoginControllerFactory()
    {
        $this->controllerManager = $this->prophesize(ControllerManager::class);

        $this->serviceLocator = $this->prophesize(ServiceManager::class);

        //$this->controllerManager->getServiceLocator()->willReturn($this->serviceLocator->reveal());

        $factory = new LoginControllerFactory();

        $instance = $factory->__invoke($this->serviceLocator->reveal(), null);

        $this->assertInstanceOf(LoginController::class, $instance);
    }
}
