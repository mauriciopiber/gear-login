<?php
namespace GearTest\LoginTest\AdminTest\RegisterTest\ServiceTest;

use Gear\Login\Admin\Register\Service\RegisterService;
use PHPUnit\Framework\TestCase;
use Zend\Authentication\AuthenticationService;
use Gear\Login\User\Service\UserService;
use GearEmail\Service\EmailService;
use Gear\Login\Role\Service\RoleService;
use Gear\Login\UserRole\Service\UserRoleService;

class RegisterServiceTest extends TestCase {

  public function setUp()
  {
    $this->email = $this->prophesize(EmailService::class);
    $this->userService = $this->prophesize(UserService::class);
    $this->roleService = $this->prophesize(RoleService::class);
    $this->userRoleService = $this->prophesize(UserRoleService::class);

    $this->register = new RegisterService(
      $this->userService->reveal(),
      $this->email->reveal(),
      $this->userRoleService->reveal(),
      $this->roleService->reveal()
    );
  }

  public function testRegisterUser()
  {
    $data = [
      'email' => 'mauriciopiber@gmail.com',
      'password' => 'testeteste',
      'confirmPassword' => 'testeteste'
    ];


    $this->userService->create($data)->willReturn(['id' => 25])->shouldBeCalled();
    $this->roleService->findOneByRole(RegisterService::DEFAULT_ROLE)->willReturn(['role' => 'admin', 'role_id' => 10])->shouldBeCalled();
    $this->userRoleService->create([
        'user_id' => 25,
        'role_id' => 10,
    ])->shouldBeCalled();

    $created = $this->register->register($data);
    //$this->email->sendEmail()->shouldBeCalled();
    // $this->userRepository->insert($data)->shouldBeCalled()->willReturn(true);
    // $created = $this->register->register($data);
    $this->assertTrue($created);
  }

  // public function testCreateActiveEmail()
  // {
  //   $response = $this->register->createActivationEmail();
  //   $this->assertTrue($response);
  // }





  //public function test
}
