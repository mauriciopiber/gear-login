<?php

namespace GearTest\AdminTest\HomeTest\ControllerTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Admin\Home\Controller\HomeController;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;
use GearBaseTest\Assertions\AclAssertTrait;
use Gear\Login\Admin\Login\Service\LoginService;
//use Gear\Login\Login\Filter\LoginFilter;
//use Gear\Login\Login\Form\LoginForm;
use Zend\Mvc\Plugin\Prg\PostRedirectGet;
use Zend\Http\PhpEnvironment\Response;

/**
 * @group LoginController
 * @group Controller
 */
class HomeControllerTest extends TestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    public function setUp()
    {
        parent::setUp();

        //$this->form = $this->prophesize(LoginForm::class);
        //$this->filter = $this->prophesize(LoginFilter::class);
        $this->service = $this->prophesize(LoginService::class);

        $this->controller = new HomeController(
          $this->service->reveal()//,
          //$this->form->reveal(),
          //$this->filter->reveal()
        );

        $this->prg = $this->prophesize(PostRedirectGet::class);

        $this->controller->getPluginManager()->setService('prg', $this->prg->reveal());

        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'LoginController'));
        $this->event      = new MvcEvent();
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
    }

    /**
     * @group x1
     */
    public function testEnterHomeBeforeLogin()
    {
        $this->service->getIdentity()->willReturn(false)->shouldBeCalled();
        $this->routeMatch->setParam('action', 'home');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        //var_dump($response);
        $this->assertEquals(302, $response->getStatusCode());

        $redirectUri = $response->getHeaders()->get('Location')->getUri();
        $this->assertEquals($redirectUri, '/login');
    }

    public function mockIdentity($role = 'admin') {
      return [
        'username' => 1,
        'email' => 'myemail',
        'roles' => [
          $role
        ]
      ];
    }

    /**
     * @group x1
     */
    public function testEnterHomeAfterLoginAsAdmin()
    {
        $this->service->getIdentity()->willReturn($this->mockIdentity())->shouldBeCalled();
        $this->routeMatch->setParam('action', 'home');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        //var_dump($response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($result->getVariable('layout'), 'admin');

        // $redirectUri = $response->getHeaders()->get('Location')->getUri();
        // $this->assertEquals($redirectUri, '/');
    }

    /**
     * @group x1
     */
    public function testEnterHomeAfterLoginAsMember()
    {
        $this->service->getIdentity()->willReturn($this->mockIdentity('member'))->shouldBeCalled();
        $this->routeMatch->setParam('action', 'home');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        //var_dump($response);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals($result->getVariable('layout'), 'client');

        // $redirectUri = $response->getHeaders()->get('Location')->getUri();
        // $this->assertEquals($redirectUri, '/');
    }
}
