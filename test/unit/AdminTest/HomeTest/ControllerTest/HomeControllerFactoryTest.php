<?php
namespace GearTest\AdminTest\HomeTest\ControllerTest;

use PHPUnit\Framework\TestCase;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceManager;
use Gear\Login\Admin\Home\Controller\HomeControllerFactory;
use Gear\Login\Admin\Home\Controller\HomeController;

/**
 * @group Gear\Home
 * @group HomeController
 * @group Controller
 */
class HomeControllerFactoryTest extends TestCase
{
    public function testHomeControllerFactory()
    {
        $this->controllerManager = $this->prophesize(ControllerManager::class);

        $this->serviceLocator = $this->prophesize(ServiceManager::class);

        //$this->controllerManager->getServiceLocator()->willReturn($this->serviceLocator->reveal());

        $factory = new HomeControllerFactory();

        $instance = $factory->__invoke($this->serviceLocator->reveal(), null);

        $this->assertInstanceOf(HomeController::class, $instance);
    }
}
