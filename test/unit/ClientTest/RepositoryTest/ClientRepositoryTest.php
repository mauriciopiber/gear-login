<?php
namespace GearTest\LoginTest\ClientTest\RepositoryTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Client\Repository\ClientRepository;
use Zend\Db\Adapter\Adapter;

/**
 * @group Repository
 */
class ClientRepositoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->adapter = $this->prophesize(Adapter::class);

        $this->repository = new ClientRepository($this->adapter->reveal());
    }

    public function testClassExists()
    {
        $this->assertInstanceOf('Gear\Login\Client\Repository\ClientRepository', $this->repository);
    }
}
