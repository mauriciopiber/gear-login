<?php
namespace GearTest\LoginTest\ClientTest\ServiceTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Client\Service\ClientService;

/**
 * @group Service
 */
class ClientServiceTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->clientFilter = $this->prophesize('Gear\Login\Client\Filter\ClientFilter');
        $this->clientRepository = $this->prophesize('Gear\Login\Client\Repository\ClientRepository');

        $this->service = new ClientService(
            $this->clientFilter->reveal(),
            $this->clientRepository->reveal()
        );
    }

    public function testClassExists()
    {
        $this->assertInstanceOf('Gear\Login\Client\Service\ClientService', $this->service);
    }
}
