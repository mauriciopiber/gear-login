<?php
namespace GearTest\LoginTest\ClientTest\FilterTest;

use PHPUnit\Framework\TestCase;
use Gear\Login\Client\Filter\ClientFilter;
use Zend\Db\Adapter\Adapter;

/**
 * @group Filter
 */
class ClientFilterTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->dbAdapter = $this->prophesize(Adapter::class);
        $this->filter = new ClientFilter($this->dbAdapter->reveal());
    }

    public function testClassExists()
    {
        $this->assertInstanceOf(
            'Gear\Login\Client\Filter\ClientFilter',
            $this->filter->getInputFilter()
        );
    }
}
