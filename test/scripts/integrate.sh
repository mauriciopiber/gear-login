#!/bin/bash
set -e

BASEDIR=$(dirname "$0")

data=(
  "user"
  "client"
  "scope"
  "role"
  "rule"
  "resource"
)


for i in "${data[@]}"
do
  echo ""
  echo ""
  echo "Start test $i"
  echo ""
  echo ""
  $BASEDIR/$i.sh
  echo ""
done
