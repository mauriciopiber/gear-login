function testStage {
  text=${1}
  echo "----------------------------------------------------------"
  echo "$text"
  echo "----------------------------------------------------------"
}

re='^[0-9]+$'
#echo "$token"
BASEDIR=$(dirname "$0")

source "$BASEDIR/../../vendor/bin/rest-actions";

function restTest {
  keys=()
  testStage "Create Test"
  for json in "${insert[@]}"
  do
    response=$(runPost "$json" "$API")
    echo "$response"
    id=$(echo $response | jq -r '.id')

    if ! [[ $id =~ $re ]] ; then
       echo "error: Not a id" >&2;
       echo $response
       exit 1
    fi
    testStage "Created id $id"

    keys+=($id)
  done


  testStage "Get All Test"
  echo ""
  runGet $API
  echo ""

  testStage "Get By Id"
  for id in "${keys[@]}"
  do
    echo ""
    runGet $API/$id
    echo ""
  done

  testStage "Update"
  updateResponse=$(runPut "$update" "$API/${keys[-1]}")
  echo "$updateResponse"
  echo ""

  testStage "Get After Update"
  updateId=$(echo $updateResponse | jq -r '.id')
  runGet $API/$updateId
  echo ""

  testStage "Delete"
  # delete group
  runDelete $API/${keys[-2]}
  echo ""

  testStage "Get After Delete"
  runGet $API/${keys[-2]}
  echo ""


  testStage "Test validation"
  runPut "{}" $API/
  runPut "{}" "$API/99999999999"

  for i in "${validate[@]}"
  do
    echo ""
    runPost "$i" "$API"
    echo ""
    runPut "$i" "$API/${keys-1}"
    echo ""
  done

}
