set -e


BASEDIR=$(dirname "$0")

API="gear-login/gear-login/api/user"

insert=(
  '{
    "email": "myemailb@gmail.com",
    "password": "123456",
    "first_name": "Name 1",
    "last_name": "Last Name 1"
  }'
  '{
    "email": "myemailc@gmail.com",
    "password": "123456",
    "first_name": "Name 2",
    "last_name": "Last Name 2"
  }'
  '{
    "email": "myemaild@gmail.com",
    "password": "123456",
    "first_name": "Name 3",
    "last_name": "Last Name 3"
  }'
)

update='{
  "email": "myemail@gmail.com",
  "password": "123456",
  "first_name": "Name 2 Update",
  "last_name": "Last Name 2 Update"
}'



validate=(
  '{}'
  '{
    "email": "myemail@gmail.com",
    "password": "123456",
    "first_name": "Name 2 Update",
    "last_name": "Last Name 2 Update"
  }'
)



source $BASEDIR/rest-test.sh

restTest
