const webpack = require('webpack');
const path = require('path');
//const merge = require('webpack-merge');
//const common = require('./webpack.common.js');

// const CleanWebpackPlugin = require('clean-webpack-plugin');
//const HtmlWebPackPlugin = require('html-webpack-plugin');
//const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PUBLIC_DIR = path.resolve(__dirname, 'public');
const DIST_DIR = path.resolve(__dirname, 'public/dist');
const SRC_DIR = path.resolve(__dirname, 'app');
const ADMIN_DIR = path.resolve(__dirname, 'node_modules/pbr-admin')

const config = {
  //watch: true,
  entry: {
    //home: [path.resolve(SRC_DIR, 'containers/Home/Home.js')],
    //client: [path.resolve(SRC_DIR, 'containers/Client/Client.js')],
    //backend
    admin: [path.resolve(SRC_DIR, 'admin.js')],
    client: [path.resolve(SRC_DIR, 'admin-client.js')],
    //frontend
    register: [path.resolve(SRC_DIR, 'register.js')],
    login: [path.resolve(SRC_DIR, 'login.js')],
    'recovery-password': [path.resolve(SRC_DIR, 'recovery-password.js')]
    // admin: path.resolve(SRC_DIR, 'admin.js')],
    // adminMaster: path.resolve(SRC_DIR, 'admin-master.js')],
    // recoveryPasswod: path.resolve(SRC_DIR, 'recovery-password.js')],
    // register: path.resolve(SRC_DIR, 'register.js')],
  },
  resolve: {
    extensions: ['.jsx', '.js'],
    modules: ['app', 'node_modules', 'src'],
  },
  output: {
    path: path.resolve(DIST_DIR),
    filename: '[name].js',
    publicPath: '/dist/',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [SRC_DIR, ADMIN_DIR],
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.scss$/,
        include: SRC_DIR,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
    ],
  },
};

module.exports = config;
