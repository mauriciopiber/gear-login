import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider } from 'react-intl';
import { Router } from 'react-router-dom';
import Register from 'containers/Register/Register';

import 'antd/dist/antd.css';

const formError = (error) ? error : undefined;
const formValues = (values) ? values : undefined;

const render = component => {
  ReactDOM.render(
    // <Provider store={store}>
      <IntlProvider locale="en">
        {/* <Router history={history}> */}
          {component}
        {/* </Router> */}
      </IntlProvider>
    // </Provider>
  , document.getElementById('app')
  );
}

render(<Register errors={formError} values={formValues}/>);

if (module.hot) {
  module.hot.accept();
  module.hot.accept('containers/Register/Register.jsx', () => {
    const NextRegister = require('containers/Register/Register.jsx').default;
    render(<NextRegister  errors={formError} values={formValues} />);
  });
}
