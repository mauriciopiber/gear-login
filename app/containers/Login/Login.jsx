import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Layout, Row, Col, Card } from 'antd';
import propTypes from 'prop-types';
import PageCenter from 'components/App/PageCenter';
import LoginForm from './LoginForm.jsx';


export class Login extends React.Component {
  render() {
    console.log('updating');

    const mq = {
      xs: {
        span: 24
      },
      sm: {
        span: 12
      },
      lg: {
        span: 8
      },
      xl: {
        span: 6
      }
    }

    return (
      <PageCenter>
        <Col {...mq}>
          <Card title="Login Pbr">
            <LoginForm {...this.props}/>
          </Card>
        </Col>
      </PageCenter>
    );
  }
}

Login.defaultProps = ({
  error: undefined,
  values: undefined
});

export default Login;
