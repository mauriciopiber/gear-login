import React from 'react';
import { storiesOf } from '@storybook/react';
import { Button } from '@storybook/react/demo';
import Login from './Login';
import LoginForm from './LoginForm'

storiesOf('Login', module)
  .add('Login Page', () => <Login/>)
  .add('Login Form', () => <LoginForm/>);
