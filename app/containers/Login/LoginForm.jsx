import React from 'react';
import propTypes from 'prop-types';
import { Form, Row, Col, Button, Input, Icon } from 'antd';
import FormHoc from 'components/FormExternal/FormExternalHoc';
const FormItem = Form.Item;

const LoginForm = (props) => {
  const { getFieldDecorator } = props.form;
  return (
    <Form id="login" onSubmit={props.handleSubmit} className="login-form" action="/login" method="POST">
        {getFieldDecorator('client_id')(
          <Input name="client_id" type="hidden"/>
        )}
        {getFieldDecorator('response_type')(
          <Input name="response_type" type="hidden"/>
        )}
        {getFieldDecorator('redirect_uri')(
          <Input name="redirect_uri" type="hidden"/>
        )}
        {getFieldDecorator('scope')(
          <Input name="scope" type="hidden"/>
        )}
        {getFieldDecorator('state')(
          <Input name="state" type="hidden"/>
        )}
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your E-mail!' }],
          })(
            <Input name="email" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="E-mail" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input name="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
          )}
        </FormItem>
        <FormItem>
          <Row>
            <Col span="12">
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
            </Col>
            <Col span="12">
              <a className="login-form-forgot" href="/recovery-password">Forgot password</a>
            </Col>
            <Col span="24">
              If not have an account <a href="/register">register now!</a>
            </Col>
          </Row>
          {/* {getFieldDecorator('remember', {
            valuePropName: 'checked',
            initialValue: true,
          })(
            <Checkbox>Remember me</Checkbox>
          )} */}
        </FormItem>
      </Form>
  )
}

const WrappedLoginForm = Form.create()(FormHoc(LoginForm));

export default WrappedLoginForm;
