import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/Client/Client.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';

export class ClientList extends React.Component {
  render() {
    const columns = [
      {
        title: 'Client',
        dataIndex: 'client_id',
        key: 'client_id',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'User Id',
        dataIndex: 'user_id',
        key: 'user_id',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Scope',
        dataIndex: 'scope',
        key: 'scope',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 180,
        render: AppTableAction({...this.props, key: 'client_id'})
      },
    ];
    return (
      <React.Fragment>
        <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        />
        <Table
          rowKey={orm => orm.client_id}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    clientFilter: state.filters && state.filters.client && state.filters.client.data && state.filters.client.data.results || [],
    data: state.client && state.client.list && state.client.list.data && state.client.list.data.results || [],
    page: state.client && state.client.list && state.client.list.data && state.client.list.data.page || undefined,
    total: state.client && state.client.list && state.client.list.data && parseInt(state.client.list.data.total) || undefined,
    isLoading: state.client && state.client.list && state.client.list.pending || false,
    hasSelected: state.client && state.client.list.selected && state.client.list.selected.length > 0,
    tableSelected: state.client && state.client.list.selected || []
  }),
  dispatch => ({
    loadClientFilter: () => dispatch(clientFilter.request()),
    resetClientFilter: () => dispatch(clientFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    startDelete: payload =>
      dispatch(viewActions.delete.start(payload)),
    startEdit: payload =>
      dispatch(viewActions.edit.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
    startCreate: () => dispatch(viewActions.create.start()),
    startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),
  }),
)(TableHoc(ClientList));
