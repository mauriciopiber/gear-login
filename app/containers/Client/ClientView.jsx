import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/Client/Client.actions';

const ClientView = ({values, ui, cancelView}) => {
  const data = [];

  return(
    <View
      label="Client"
      id={values.client_id}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.client
      && state.client.get
      && state.client.get.data
      && state.client.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(ClientView);
