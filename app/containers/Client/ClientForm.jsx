import React from 'react';
import { Form, Input, Button, Icon, Modal, Row, Col, Select } from 'antd';
import { connect } from 'react-redux';
import formActions from 'pbr-admin/src/store/Form/Form.actions';
import FormHoc from 'pbr-admin/src/components/Form/FormHoc';
import FormModal from 'pbr-admin/src/components/Form/FormModal';
import {
  apiActions,
  viewActions,
} from 'store/Client/Client.actions.js';

export class ClientForm extends React.Component {

  toggleAction = (values) => values.client_id != undefined;

  normatize = (values) => ({
    ...values,
    id: values.client_id
  });

  handleSubmit = (e) => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(this.toggleAction(values));
        if (this.toggleAction(values)) {
          this.props.update(this.normatize(values));
          return;
        }
        this.props.create(values);
      }
     });
  }
  componentWillReceiveProps = (props) => {
    if (props.values === undefined) {
      return;
    }
    if (props.values && props.values !== this.props.values) {
      this.props.form.setFieldsValue(props.values);
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    return(
      <FormModal
        visible={this.props.ui.form.display}
        cancel={this.props.cancelCreate}
        reset={this.props.startReset}
        id={this.props.values.client_id}
        label="Client"
        loading={this.props.loading}
        submit={this.handleSubmit}
      >
          {getFieldDecorator('client_id')(
            <Input type="hidden"/>
          )}
      </FormModal>
    )
  }
}

export default connect(
  state => ({
    ui: state.ui && state.ui.ui,
    values: state.client
      && state.client.get
      && state.client.get.data
      && state.client.get.data.result
      || {},
    errors: state.client
      && state.client.add
      && state.client.add.error
      || state.client
      && state.client.edit
      && state.client.edit.error
      ||{},
    loading: state.client.add.pending || state.client.edit.pending,
    formUi: state.form,
  }),
  dispatch => ({
    cancelCreate: () => dispatch(viewActions.create.cancel()),
    startReset: () => dispatch(formActions.resetFormStart()),
    completeReset: () => dispatch(formActions.resetFormComplete()),
    create: (values) => dispatch(apiActions.add.request(values)),
    resetAdd: () => dispatch(apiActions.add.reset()),
    update: (values) => dispatch(apiActions.edit.request(values))
  })
)(FormHoc(ClientForm));
