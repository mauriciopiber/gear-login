import React from 'react';
import { connect } from 'react-redux';
import ClientList from './ClientList';
import ClientView from './ClientView';
import ClientForm from './ClientForm';
import ClientDel from './ClientDel';

class Client extends React.Component {
  render() {
    return (
      <div>
        <ClientList />
        <ClientView />
        <ClientForm />
        <ClientDel />
      </div>
    )
  }
}

export default connect()(Client)
