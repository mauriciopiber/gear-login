import React from 'react';
import { connect } from 'react-redux';
import DeleteModal from 'pbr-admin/src/components/Delete/DeleteModal/DeleteModal';
import DeleteAllModal from 'pbr-admin/src/components/Delete/DeleteAllModal/DeleteAllModal';
import {
  viewActions,
  apiActions
} from 'store/Client/Client.actions';

class ClientDelete extends React.Component {

  render() {
    return (
      <React.Fragment>
        <DeleteModal
          visible={this.props.ui.delete.display}
          cancelAction={this.props.cancelDelete}
          loading={this.props.del.pending}
          confirmAction={() => this.props.deleteClient(this.props.del.selected)}
          selected={this.props.del.selected}
          label="Client"
        />
        <DeleteAllModal
          visible={this.props.ui.deleteAll.display}
          cancelAction={this.props.cancelRemoveAll}
          confirmAction={() => this.props.confirmRemoveAll(this.props.tableSelected)}
          selected={this.props.tableSelected}
          label="Client"
        />
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    del: state.client && state.client.del || {},
    ui: state.ui.ui,
    tableSelected: state.client.list.selected || []
  }),
  dispatch => ({
    confirmRemoveAll: (payload) => dispatch(viewActions.deleteAll.confirm(payload)),
    cancelRemoveAll: () => dispatch(viewActions.deleteAll.cancel()),
    cancelDelete: (id) => dispatch(viewActions.delete.cancel()),
    deleteClient: (client) => dispatch(apiActions.del.request({id: client})),
  })
)(ClientDelete)
