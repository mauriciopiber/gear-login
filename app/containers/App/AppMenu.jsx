import React from 'react';
import propTypes from 'prop-types';
import { Menu } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

const AppMenu = (props) => {

  let currentLocation = props.location.pathname;
  let index;
  for (let i = 0; i < props.routes.length; i++) {
    if (props.routes[i].to === currentLocation) {
      index = ((i+1).toString());
    }
  }

  return (
    <Menu theme="dark" defaultSelectedKeys={[index]} mode="inline">
      {props.routes.map((value, index) => {
        if (value.show === false) {
          return;
        }
        if (props.isLogged === false && value.private) {
          return;
        }
        return (
          <Menu.Item key={index+1}>
            <Link to={value.to}>{value.icon}<span>{value.label}</span></Link>
          </Menu.Item>
        )
      })}
    </Menu>
  )
}

export default withRouter(
  connect(
    state => ({
      isLogged: state.auth && state.auth.user && state.auth.user.email ? true : false
    })
  )(AppMenu)
);
