import React from 'react';
import propTypes from 'prop-types';
import { Layout } from 'antd';

const AppFooter = (props) => {
  return (
    <Layout.Footer style={{ textAlign: 'center' }}>
      Ant Design ©2018 Created by Ant UED
    </Layout.Footer>
  )
}


export default AppFooter;
