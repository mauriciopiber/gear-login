import React from 'react';
import { connect } from 'react-redux';
import { Layout } from 'antd';

class AppSider extends React.Component {
  state = {
    collapsed: false,
  };

  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }

  render() {
    return (
      <Layout.Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
      >
        <div className="logo" style={{height: '64px'}}/>
        {this.props.children}
      </Layout.Sider>
    )
  }
}

export default connect()(AppSider)
