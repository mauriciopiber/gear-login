import React from 'react';
import propTypes from 'prop-types';
import { Button } from 'antd';

const AppHeader = (props) => {
  return (
      <Button href="/logout" style={{float: 'right', margin: '15px 15px'}}>Logout</Button>
  )
}

export default AppHeader;
