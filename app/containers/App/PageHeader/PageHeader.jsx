import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Row, Col, Button, Icon } from 'antd';

class PageHeader extends React.Component {
  render() {
    return (
      <Row>
        <Col span="12">
          <Button
             type="primary"
             disabled={!this.props.hasSelected}
             onClick={() => this.props.startRemoveAll(this.props.tableSelected)}
           >
             <Icon type="delete" theme="outlined" />
           </Button>
           {' '}
          <Button className="btn-create" onClick={this.props.startCreate}>
            <Icon type="plus" theme="outlined" />
          </Button>
        </Col>
      </Row>
    )
  }
}

PageHeader.propTypes = ({
  tableSelected: propTypes.array,
  startCreate: propTypes.func,
  hasSelected: propTypes.bool
})

export default PageHeader;
