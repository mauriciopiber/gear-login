import React from 'react';
import propTypes from 'prop-types';
import { Layout } from 'antd';
import AppMenu from 'containers/App/AppMenu';
import { Route , Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ component: Component, auth, ...rest }) => (
  <Route {...rest} render={(props) => (
    auth === true
      ? <Component {...props} />
      : <Redirect to='/' />
  )} />
)

const AppContent = (props) => {
  return (
    <Layout.Content style={{ margin: '0 5px' }}>
      <div style={{ padding: 15, background: '#fff', minHeight: 360 }}>
        <Switch>
          {props.routes.map((value, index) => {
            if (value.private) {
              return <PrivateRoute auth={props.isLogged} key={index+1} path={value.to} component={value.render} />
            }
            return <Route exact  key={index+1} path={value.to} component={value.render} />
          })}
        </Switch>
      </div>
    </Layout.Content>
  )
}

export default withRouter(connect(
  state => ({
    isLogged: state.auth && state.auth.user && state.auth.user.email ? true : false
  })
)(AppContent));
