import React from 'react';
import { connect } from 'react-redux';
import AccessTokenList from './AccessTokenList';
import AccessTokenView from './AccessTokenView';
import AccessTokenForm from './AccessTokenForm';
import AccessTokenDel from './AccessTokenDel';

class AccessToken extends React.Component {
  render() {
    return (
      <div>
        <AccessTokenList />
        <AccessTokenView />
        {/* <AccessTokenForm />
        <AccessTokenDel /> */}
      </div>
    )
  }
}

export default connect()(AccessToken)
