import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/AccessToken/AccessToken.actions';

const AccessTokenView = ({values, ui, cancelView}) => {
  const data = [
    {
      'column': 'client_id',
      'label': 'Client'
    },
    {
      'column': 'email',
      'label': 'User'
    },
    {
      column: 'redirect_uri',
      label: 'Redirect URI'
    },
    {
      'column': 'expires',
      'label': 'Expires'
    },
    {
      'column': 'scope',
      'label': 'Scope'
    },
    {
      'column': 'access_token',
      'label': 'Access Token'
    }
  ];

  return(
    <View
      label="Access Token"
      id={values.access_token}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.accessToken
      && state.accessToken.get
      && state.accessToken.get.data
      && state.accessToken.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(AccessTokenView);
