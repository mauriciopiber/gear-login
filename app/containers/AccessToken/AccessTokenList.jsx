import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/AccessToken/AccessToken.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';

export class AccessTokenList extends React.Component {
  render() {
    const columns = [
      {
        title: 'Id',
        dataIndex: 'access_token',
        key: 'access_token',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Client',
        dataIndex: 'client_id',
        key: 'client_id',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'User',
        dataIndex: 'email',
        key: 'email',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Expires',
        dataIndex: 'expires',
        key: 'expires',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Scope',
        dataIndex: 'scope',
        key: 'scope',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 60,
        render: AppTableAction({...this.props, key: 'access_token_id'})
      },
    ];
    return (
      <React.Fragment>
        <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        />
        <Table
          rowKey={orm => orm.access_token_id}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    accessTokenFilter: state.filters && state.filters.accessToken && state.filters.accessToken.data && state.filters.accessToken.data.results || [],
    data: state.accessToken && state.accessToken.list && state.accessToken.list.data && state.accessToken.list.data.results || [],
    page: state.accessToken && state.accessToken.list && state.accessToken.list.data && state.accessToken.list.data.page || undefined,
    total: state.accessToken && state.accessToken.list && state.accessToken.list.data && parseInt(state.accessToken.list.data.total) || undefined,
    isLoading: state.accessToken && state.accessToken.list && state.accessToken.list.pending || false,
    hasSelected: state.accessToken && state.accessToken.list.selected && state.accessToken.list.selected.length > 0,
    tableSelected: state.accessToken && state.accessToken.list.selected || []
  }),
  dispatch => ({
    loadAccessTokenFilter: () => dispatch(accessTokenFilter.request()),
    resetAccessTokenFilter: () => dispatch(accessTokenFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    /*startDelete: payload =>
      dispatch(viewActions.delete.start(payload)),
    startEdit: payload =>
      dispatch(viewActions.edit.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
    startCreate: () => dispatch(viewActions.create.start()),
    startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),*/
  }),
)(TableHoc(AccessTokenList));
