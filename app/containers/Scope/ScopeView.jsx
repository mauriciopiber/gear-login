import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/Scope/Scope.actions';

const ScopeView = ({values, ui, cancelView}) => {
  const data = [];

  return(
    <View
      label="Scope"
      id={values.scope_id}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.scope
      && state.scope.get
      && state.scope.get.data
      && state.scope.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(ScopeView);
