import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/Scope/Scope.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';

export class ScopeList extends React.Component {
  render() {
    const columns = [
      {
        title: 'Scope',
        dataIndex: 'scope',
        key: 'scope',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 180,
        render: AppTableAction({...this.props, key: 'scope'})
      },
    ];
    return (
      <React.Fragment>
        <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        />
        <Table
          rowKey={orm => orm.scope_id}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    scopeFilter: state.filters && state.filters.scope && state.filters.scope.data && state.filters.scope.data.results || [],
    data: state.scope && state.scope.list && state.scope.list.data && state.scope.list.data.results || [],
    page: state.scope && state.scope.list && state.scope.list.data && state.scope.list.data.page || undefined,
    total: state.scope && state.scope.list && state.scope.list.data && parseInt(state.scope.list.data.total) || undefined,
    isLoading: state.scope && state.scope.list && state.scope.list.pending || false,
    hasSelected: state.scope && state.scope.list.selected && state.scope.list.selected.length > 0,
    tableSelected: state.scope && state.scope.list.selected || []
  }),
  dispatch => ({
    loadScopeFilter: () => dispatch(scopeFilter.request()),
    resetScopeFilter: () => dispatch(scopeFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    startDelete: payload =>
      dispatch(viewActions.delete.start(payload)),
    startEdit: payload =>
      dispatch(viewActions.edit.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
    startCreate: () => dispatch(viewActions.create.start()),
    startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),
  }),
)(TableHoc(ScopeList));
