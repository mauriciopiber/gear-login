import React from 'react';
import { connect } from 'react-redux';
import DeleteModal from 'pbr-admin/src/components/Delete/DeleteModal/DeleteModal';
import DeleteAllModal from 'pbr-admin/src/components/Delete/DeleteAllModal/DeleteAllModal';
import {
  viewActions,
  apiActions
} from 'store/Scope/Scope.actions';

class ScopeDelete extends React.Component {

  render() {
    return (
      <React.Fragment>
        <DeleteModal
          visible={this.props.ui.delete.display}
          cancelAction={this.props.cancelDelete}
          loading={this.props.del.pending}
          confirmAction={() => this.props.deleteScope(this.props.del.selected)}
          selected={this.props.del.selected}
        />
        <DeleteAllModal
          visible={this.props.ui.deleteAll.display}
          cancelAction={this.props.cancelRemoveAll}
          confirmAction={() => this.props.confirmRemoveAll(this.props.tableSelected)}
          selected={this.props.tableSelected}
        />
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    del: state.scope && state.scope.del || {},
    ui: state.ui.ui,
    tableSelected: state.scope.list.selected || []
  }),
  dispatch => ({
    confirmRemoveAll: (payload) => dispatch(viewActions.deleteAll.confirm(payload)),
    cancelRemoveAll: () => dispatch(viewActions.deleteAll.cancel()),
    cancelDelete: (id) => dispatch(viewActions.delete.cancel()),
    deleteScope: (scope) => dispatch(apiActions.del.request({id: scope})),
  })
)(ScopeDelete)
