import React from 'react';
import { connect } from 'react-redux';
import ScopeList from './ScopeList';
import ScopeView from './ScopeView';
import ScopeForm from './ScopeForm';
import ScopeDel from './ScopeDel';

class Scope extends React.Component {
  render() {
    return (
      <div>
        <ScopeList />
        <ScopeView />
        <ScopeForm />
        <ScopeDel />
      </div>
    )
  }
}

export default connect()(Scope)
