import React from 'react';
import { connect } from 'react-redux';
import AuthorizationCodeList from './AuthorizationCodeList';
import AuthorizationCodeView from './AuthorizationCodeView';
// import AuthorizationCodeForm from './AuthorizationCodeForm';
// import AuthorizationCodeDel from './AuthorizationCodeDel';

class AuthorizationCode extends React.Component {
  render() {
    return (
      <div>
        <AuthorizationCodeList />
        <AuthorizationCodeView />
        {/* <AuthorizationCodeForm />
        <AuthorizationCodeDel /> */}
      </div>
    )
  }
}

export default connect()(AuthorizationCode)
