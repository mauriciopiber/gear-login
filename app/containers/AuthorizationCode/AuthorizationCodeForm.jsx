import React from 'react';
import { Form, Input, Button, Icon, Modal, Row, Col, Select } from 'antd';
import { connect } from 'react-redux';
import formActions from 'pbr-admin/src/store/Form/Form.actions';
import FormHoc from 'pbr-admin/src/components/Form/FormHoc';
import FormModal from 'pbr-admin/src/components/Form/FormModal';
import {
  apiActions,
  viewActions,
} from 'store/AuthorizationCode/AuthorizationCode.actions.js';

export class AuthorizationCodeForm extends React.Component {

  toggleAction = (values) => values.authorization_code_id != undefined;

  normatize = (values) => ({
    ...values,
    id: values.authorization_code_id
  });

  handleSubmit = (e) => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(this.toggleAction(values));
        if (this.toggleAction(values)) {
          this.props.update(this.normatize(values));
          return;
        }
        this.props.create(values);
      }
     });
  }
  componentWillReceiveProps = (props) => {
    if (props.values === undefined) {
      return;
    }
    if (props.values && props.values !== this.props.values) {
      this.props.form.setFieldsValue(props.values);
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    return(
      <FormModal
        visible={this.props.ui.form.display}
        cancel={this.props.cancelCreate}
        reset={this.props.startReset}
        id={this.props.values.authorization_code_id}
        label="AuthorizationCode"
        loading={this.props.loading}
        submit={this.handleSubmit}
      >
          {getFieldDecorator('authorization_code_id')(
            <Input type="hidden"/>
          )}
      </FormModal>
    )
  }
}

export default connect(
  state => ({
    ui: state.ui && state.ui.ui,
    values: state.authorizationCode
      && state.authorizationCode.get
      && state.authorizationCode.get.data
      && state.authorizationCode.get.data.result
      || {},
    errors: state.authorizationCode
      && state.authorizationCode.add
      && state.authorizationCode.add.error
      || state.authorizationCode
      && state.authorizationCode.edit
      && state.authorizationCode.edit.error
      ||{},
    loading: state.authorizationCode.add.pending || state.authorizationCode.edit.pending,
    formUi: state.form,
  }),
  dispatch => ({
    cancelCreate: () => dispatch(viewActions.create.cancel()),
    startReset: () => dispatch(formActions.resetFormStart()),
    completeReset: () => dispatch(formActions.resetFormComplete()),
    create: (values) => dispatch(apiActions.add.request(values)),
    resetAdd: () => dispatch(apiActions.add.reset()),
    update: (values) => dispatch(apiActions.edit.request(values))
  })
)(FormHoc(AuthorizationCodeForm));
