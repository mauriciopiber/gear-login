import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/AuthorizationCode/AuthorizationCode.actions';

const AuthorizationCodeView = ({values, ui, cancelView}) => {
  const data = [
    {
      'column': 'authorization_code',
      'label': 'Authorization Code'
    },
    {
      'column': 'client_id',
      'label': 'Client'
    },
    {
      'column': 'email',
      'label': 'User'
    },
    {
      column: 'redirect_uri',
      label: 'Redirect URI'
    },
    {
      'column': 'expires',
      'label': 'Expires'
    },
    {
      'column': 'scope',
      'label': 'Scope'
    },
  ];

  return(
    <View
      label="Authorization Code"
      id={values.authorization_code}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.authorizationCode
      && state.authorizationCode.get
      && state.authorizationCode.get.data
      && state.authorizationCode.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(AuthorizationCodeView);
