import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/AuthorizationCode/AuthorizationCode.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';

export class AuthorizationCodeList extends React.Component {
  render() {
    const columns = [
      {
        title: 'Code',
        dataIndex: 'authorization_code',
        key: 'authorization_code',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Redirect URI',
        dataIndex: 'redirect_uri',
        key: 'redirect_uri',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Client',
        dataIndex: 'client_id',
        key: 'client_id',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'User',
        dataIndex: 'email',
        key: 'email',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Expires',
        dataIndex: 'expires',
        key: 'expires',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Scope',
        dataIndex: 'scope',
        key: 'scope',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 60,
        render: AppTableAction({...this.props, key: 'authorization_code'})
      },
    ];
    return (
      <React.Fragment>
        {/* <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        /> */}
        <Table
          rowKey={orm => orm.authorization_code}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    authorizationCodeFilter: state.filters && state.filters.authorizationCode && state.filters.authorizationCode.data && state.filters.authorizationCode.data.results || [],
    data: state.authorizationCode && state.authorizationCode.list && state.authorizationCode.list.data && state.authorizationCode.list.data.results || [],
    page: state.authorizationCode && state.authorizationCode.list && state.authorizationCode.list.data && state.authorizationCode.list.data.page || undefined,
    total: state.authorizationCode && state.authorizationCode.list && state.authorizationCode.list.data && parseInt(state.authorizationCode.list.data.total) || undefined,
    isLoading: state.authorizationCode && state.authorizationCode.list && state.authorizationCode.list.pending || false,
    hasSelected: state.authorizationCode && state.authorizationCode.list.selected && state.authorizationCode.list.selected.length > 0,
    tableSelected: state.authorizationCode && state.authorizationCode.list.selected || []
  }),
  dispatch => ({
    loadAuthorizationCodeFilter: () => dispatch(authorizationCodeFilter.request()),
    resetAuthorizationCodeFilter: () => dispatch(authorizationCodeFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
       dispatch(viewActions.view.start(payload)),
    // startDelete: payload =>
    //   dispatch(viewActions.delete.start(payload)),
    // startEdit: payload =>
    //   dispatch(viewActions.edit.start(payload)),
    // select: payload => dispatch(apiActions.list.select({ payload })),
    // startCreate: () => dispatch(viewActions.create.start()),
    // startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),
  }),
)(TableHoc(AuthorizationCodeList));
