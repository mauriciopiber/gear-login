import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { authActions } from 'store/Auth/Auth.actions';
import { Row, Col, List, Button } from 'antd';

class ProfileView extends React.Component {
  componentDidMount() {
    this.props.loadProfile();
  }

  render() {

    const columns = !this.props.auth ? [] : [
      {
        title: 'E-mail',
        value: this.props.auth.email,
      },
      {
        title: 'Role',
        value: this.props.auth.roles[0],
      }
    ]

    return (
      <React.Fragment>
      <Row>
        <Col span="10">
          <List
           bordered
           dataSource={columns}
           renderItem={item => (
             <List.Item>
               <List.Item.Meta
                 title={<a>{item.title}</a>}
                 description={item.value}
               />
             </List.Item>
           )}
         />
        </Col>
      </Row>
      <Row>
        <Link to="/profile/edit"><Button>Edit</Button></Link>
      </Row>
      <Row>
        <Link to="/profile/change-password"><Button>Change Password</Button></Link>
      </Row>
      </React.Fragment>


    )
  }
}

export default connect(
  state => ({
    auth: state.auth && state.auth.get && state.auth.get.data && state.auth.get.data.payload || null
  }),
  dispatch => ({
    loadProfile: () => dispatch(authActions.get.request())
  })
)(ProfileView)
