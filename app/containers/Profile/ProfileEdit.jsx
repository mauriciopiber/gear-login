import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Row, Col, List, Button } from 'antd';
import ProfileForm from './ProfileForm';

class ProfileEdit extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Row>
          <Col span="6">
            <ProfileForm/>
          </Col>
        </Row>
        <Row style={{marginTop: '5px'}}>
          <Link to="/profile"><Button>Back</Button></Link>
        </Row>

      </React.Fragment>

    )
  }
}

export default connect()(ProfileEdit)
