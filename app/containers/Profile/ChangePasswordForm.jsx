import React from 'react';
import propTypes from 'prop-types';
import { Form, Row, Col, Button, Input, Icon } from 'antd';
const FormItem = Form.Item;

const ChangePasswordForm = (props) => {
  const { getFieldDecorator } = props.form;
  return (
    <Form id="login" onSubmit={props.handleSubmit} className="login-form" action="/register" method="POST">
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input name="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator('confirmPassword', {
            rules: [{ required: true, message: 'Please confirm your Password!' }],
          })(
            <Input name="confirmPassword" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Confirm Password" />
          )}
        </FormItem>
        <FormItem>
          <Row>
            <Col span="12">
              <Button type="primary" htmlType="submit" className="register-form-button">
                Change Password
              </Button>
            </Col>

          </Row>
        </FormItem>
      </Form>
  )
}

const WrappedChangePasswordForm = Form.create()(ChangePasswordForm);

export default WrappedChangePasswordForm;
