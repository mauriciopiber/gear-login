import React from 'react';
import { connect } from 'react-redux';
import { Form, Button, Input, Select } from 'antd';
import { roleFilter } from 'store/Filters/Filters.actions';
import { authActions } from 'store/Auth/Auth.actions';

class ProfileForm extends React.Component {
  componentDidMount() {
    this.props.loadRole();

    if (this.props.values) {
      let { values } = this.props;
      values.role_id = values.roles[0];
      this.props.form.setFieldsValue(values);
    } else {
      this.props.loadProfile();
    }
  }

  //toggleAction = (values) => values.client_id != undefined;

  // normatize = (values) => ({
  //   ...values,
  //   id: values.client_id
  // });

  handleSubmit = (e) => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(values);
        //values.id = values.username;
        this.props.updateProfile({
          id: values.username,
          ...values
        });
      }
     });
  }
  componentWillReceiveProps = (props) => {
    if (props.values === undefined) {
      return;
    }
    if (props.values && props.values !== this.props.values) {

      let values = props.values;
      values.role_id = props.values.roles[0];
      this.props.form.setFieldsValue(values);
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        {getFieldDecorator('username')(
          <Input type="hidden"/>
        )}
        <Form.Item label="E-mail">
        {getFieldDecorator('email', {
          rules: [{ required: true, message: 'Please input e-mail!' }],
        })(
          <Input placeholder="E-mail" disabled={this.props.loading} />
        )}
        </Form.Item>
        <Form.Item label="Role">
        {getFieldDecorator('role_id')(
          <Select>
            {this.props.roles.map(role => {
              return <Select.Option key={role.role_id} value={role.role_id}>{role.role}</Select.Option>
            })}
          </Select>
        )}
        </Form.Item>
        <Button loading={this.props.loading} htmlType="submit">Save</Button>{' '}
        {/* <Button onClick={this.props.cancel}>Cancel</Button>{' '} */}
        <Button onClick={this.props.loadProfile}>Reset</Button>
      </Form>
    )
  }
}

export default connect(
  state => ({
    roles: state.filters && state.filters.role && state.filters.role.data && state.filters.role.data.results || [],
    values: state.auth && state.auth.get && state.auth.get.data && state.auth.get.data.payload || undefined,
    loading: state.auth && state.auth.update && state.auth.update.pending || false
  }),
  dispatch => ({
    loadRole: () => dispatch(roleFilter.request()),
    loadProfile: () => dispatch(authActions.get.request()),
    updateProfile: (payload) => dispatch(authActions.update.request(payload)),
  })
)(Form.create()(ProfileForm));
