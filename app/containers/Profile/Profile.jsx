import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Route , Switch, Redirect, withRouter } from 'react-router-dom';
import ProfileEdit from 'containers/Profile/ProfileEdit';
import ProfileView from 'containers/Profile/ProfileView';
import ProfilePassword from 'containers/Profile/ProfilePassword';

class Profile extends React.Component {

  render() {
    return (
      <div>
        <Switch>
          <Route exact key={1} path="/profile" component={ProfileView} />
          <Route exact key={2} path="/profile/edit" component={ProfileEdit} />
          <Route exact key={3} path="/profile/change-password" component={ProfilePassword} />
        </Switch>
      </div>
    )
  }
}

export default connect()(Profile)
