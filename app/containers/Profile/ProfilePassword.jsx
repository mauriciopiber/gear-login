import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, List, Button } from 'antd';
import { Link } from 'react-router-dom';
import ChangePasswordForm from './ChangePasswordForm';

class ProfilePassword extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Row>
          <Col span="6">
            <ChangePasswordForm/>
          </Col>
        </Row>
        <Row style={{marginTop: '5px'}}>
          <Link to="/profile"><Button>Back</Button></Link>
        </Row>

      </React.Fragment>
    )
  }
}

export default connect()(ProfilePassword)
