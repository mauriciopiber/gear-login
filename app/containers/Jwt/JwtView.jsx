import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/Jwt/Jwt.actions';

const JwtView = ({values, ui, cancelView}) => {
  const data = [];

  return(
    <View
      label="Jwt"
      id={values.jwt_id}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.jwt
      && state.jwt.get
      && state.jwt.get.data
      && state.jwt.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(JwtView);
