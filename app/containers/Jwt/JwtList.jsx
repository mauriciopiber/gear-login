import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/Jwt/Jwt.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';

export class JwtList extends React.Component {
  render() {
    const columns = [
      {
        title: 'Id',
        dataIndex: 'jwt_id',
        key: 'jwt_id',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 180,
        render: AppTableAction({...this.props, key: 'jwt_id'})
      },
    ];
    return (
      <React.Fragment>
        <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        />
        <Table
          rowKey={orm => orm.jwt_id}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    jwtFilter: state.filters && state.filters.jwt && state.filters.jwt.data && state.filters.jwt.data.results || [],
    data: state.jwt && state.jwt.list && state.jwt.list.data && state.jwt.list.data.results || [],
    page: state.jwt && state.jwt.list && state.jwt.list.data && state.jwt.list.data.page || undefined,
    total: state.jwt && state.jwt.list && state.jwt.list.data && parseInt(state.jwt.list.data.total) || undefined,
    isLoading: state.jwt && state.jwt.list && state.jwt.list.pending || false,
    hasSelected: state.jwt && state.jwt.list.selected && state.jwt.list.selected.length > 0,
    tableSelected: state.jwt && state.jwt.list.selected || []
  }),
  dispatch => ({
    loadJwtFilter: () => dispatch(jwtFilter.request()),
    resetJwtFilter: () => dispatch(jwtFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    startDelete: payload =>
      dispatch(viewActions.delete.start(payload)),
    startEdit: payload =>
      dispatch(viewActions.edit.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
    startCreate: () => dispatch(viewActions.create.start()),
    startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),
  }),
)(TableHoc(JwtList));
