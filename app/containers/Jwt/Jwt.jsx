import React from 'react';
import { connect } from 'react-redux';
import JwtList from './JwtList';
import JwtView from './JwtView';
import JwtForm from './JwtForm';
import JwtDel from './JwtDel';

class Jwt extends React.Component {
  render() {
    return (
      <div>
        <JwtList />
        <JwtView />
        <JwtForm />
        <JwtDel />
      </div>
    )
  }
}

export default connect()(Jwt)
