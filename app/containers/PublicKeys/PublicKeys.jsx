import React from 'react';
import { connect } from 'react-redux';
import PublicKeysList from './PublicKeysList';
// import PublicKeysView from './PublicKeysView';
// import PublicKeysForm from './PublicKeysForm';
// import PublicKeysDel from './PublicKeysDel';

class PublicKeys extends React.Component {
  render() {
    return (
      <div>
        <PublicKeysList />
        {/* <PublicKeysView />
        <PublicKeysForm />
        <PublicKeysDel /> */}
      </div>
    )
  }
}

export default connect()(PublicKeys)
