import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/PublicKeys/PublicKeys.actions';

const PublicKeysView = ({values, ui, cancelView}) => {
  const data = [
    {
      column: 'public_key',
      label: 'Public Key'
    },
    {
      column: 'private_key',
      label: 'Private Key'
    },
    {
      column: 'encryption_algorithm',
      label: 'Encryption'
    },
    {
      column: 'Client ID',
      label: 'Client'
    }
  ];

  return(
    <View
      label="PublicKeys"
      id={values.public_keys}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.publicKeys
      && state.publicKeys.get
      && state.publicKeys.get.data
      && state.publicKeys.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(PublicKeysView);
