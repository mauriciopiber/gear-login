import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/PublicKeys/PublicKeys.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';

export class PublicKeysList extends React.Component {
  render() {
    const columns = [
      {
        title: 'Public Key',
        dataIndex: 'public_key',
        key: 'public_keys',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Private Key',
        dataIndex: 'private_key',
        key: 'private_keys',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Client',
        dataIndex: 'client_id',
        key: 'client_id',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Encryption',
        dataIndex: 'encryption_algorithm',
        key: 'encryption_algorithm',
        sorter: true,
        filterDropdown: TextFilter(),
      }/*,
      {
        title: 'Action',
        key: 'action',
        width: 180,
        render: AppTableAction({...this.props, key: 'public_keys'})
      },*/
    ];
    return (
      <React.Fragment>
        {/* <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        /> */}
        <Table
          rowKey={orm => orm.public_keys}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    data: state.publicKeys && state.publicKeys.list && state.publicKeys.list.data && state.publicKeys.list.data.results || [],
    page: state.publicKeys && state.publicKeys.list && state.publicKeys.list.data && state.publicKeys.list.data.page || undefined,
    total: state.publicKeys && state.publicKeys.list && state.publicKeys.list.data && parseInt(state.publicKeys.list.data.total) || undefined,
    isLoading: state.publicKeys && state.publicKeys.list && state.publicKeys.list.pending || false,
    hasSelected: state.publicKeys && state.publicKeys.list.selected && state.publicKeys.list.selected.length > 0,
    tableSelected: state.publicKeys && state.publicKeys.list.selected || []
  }),
  dispatch => ({
    loadPublicKeysFilter: () => dispatch(publicKeysFilter.request()),
    resetPublicKeysFilter: () => dispatch(publicKeysFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    /*
    startDelete: payload =>
      dispatch(viewActions.delete.start(payload)),
    startEdit: payload =>
      dispatch(viewActions.edit.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
    startCreate: () => dispatch(viewActions.create.start()),
    startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),
    */
  }),
)(TableHoc(PublicKeysList));
