import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Layout, Row, Col, Card } from 'antd';
import propTypes from 'prop-types';
import PageCenter from 'components/App/PageCenter';
import RegisterForm from './RegisterForm.jsx';


export class Register extends React.Component {
  render() {
    console.log('updating');

    const mq = {
      xs: {
        span: 24
      },
      sm: {
        span: 12
      },
      lg: {
        span: 8
      },
      xl: {
        span: 6
      }
    }

    return (
      <PageCenter>
        <Col {...mq}>
          <Card title="Register Pbr">
            <RegisterForm {...this.props}/>
          </Card>
        </Col>
      </PageCenter>
    );
  }
}

Register.defaultProps = ({
  error: undefined,
  values: undefined
});

export default Register;
