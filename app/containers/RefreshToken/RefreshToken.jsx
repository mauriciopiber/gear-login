import React from 'react';
import { connect } from 'react-redux';
import RefreshTokenList from './RefreshTokenList';
import RefreshTokenView from './RefreshTokenView';

class RefreshToken extends React.Component {
  render() {
    return (
      <div>
        <RefreshTokenList />
        <RefreshTokenView />
      </div>
    )
  }
}

export default connect()(RefreshToken)
