import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/RefreshToken/RefreshToken.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import { clientFilter, userFilter } from 'store/Filters/Filters.actions';
import { getUserFilter, getClientFilter } from 'store/Filters/Filters.selectors';
import {
  getRefreshTokenData,
  getRefreshTokenPage,
  getRefreshTokenTotal,
  getRefreshTokenSelected,
  getRefreshTokenHasSelected,
  getRefreshTokenLoading
} from 'store/RefreshToken/RefreshToken.selectors'

export class RefreshTokenList extends React.Component {
  componentDidMount() {
    this.props.loadClientFilter();
    this.props.loadUserFilter();
  }

  componentWillUnmount() {
    this.props.resetClientFilter();
    this.props.resetUserFilter();
  }

  render() {
    const columns = [
      {
        title: 'Client',
        dataIndex: 'client_id',
        key: 'client_id',
        sorter: true,
        filters: this.props.clientFilter.map(qt => ({text: qt.client_id, value: qt.client_id}))
      },
      {
        title: 'User',
        dataIndex: 'email',
        key: 'email',
        sorter: true,
        filters: this.props.userFilter.map(qt => ({text: qt.email, value: qt.username}))
      },
      {
        title: 'Expires',
        dataIndex: 'expires',
        key: 'expires',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Scope',
        dataIndex: 'scope',
        key: 'scope',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'R. Token',
        dataIndex: 'refresh_token',
        key: 'refresh_token',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 60,
        render: AppTableAction({...this.props, key: 'refresh_token'})
      },
    ];
    return (
      <React.Fragment>
        <Table
          rowKey={orm => orm.refresh_token_id}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    userFilter: getUserFilter(state),
    clientFilter: getClientFilter(state),
    data: getRefreshTokenData(state),
    page: getRefreshTokenPage(state),
    total: getRefreshTokenTotal(state),
    isLoading: getRefreshTokenLoading(state),
    hasSelected: getRefreshTokenHasSelected(state),
    tableSelected: getRefreshTokenSelected(state)
  }),
  dispatch => ({
    loadUserFilter: () => dispatch(userFilter.request()),
    resetUserFilter: () => dispatch(userFilter.reset()),
    loadClientFilter: () => dispatch(clientFilter.request()),
    resetClientFilter: () => dispatch(clientFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
  }),
)(TableHoc(RefreshTokenList));
