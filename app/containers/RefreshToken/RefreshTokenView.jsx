import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/RefreshToken/RefreshToken.actions';

const RefreshTokenView = ({values, ui, cancelView}) => {
  console.log(values);
  const data = [
    {
      'column': 'email',
      'label': 'User'
    },
    {
      'column': 'expires',
      'label': 'Expires'
    },
    {
      'column': 'client_id',
      'label': 'Client'
    },
    {
      'column': 'scope',
      'label': 'Scope'
    },

  ];

  return(
    <View
      label="RefreshToken"
      id={values.refresh_token}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.refreshToken
      && state.refreshToken.get
      && state.refreshToken.get.data
      && state.refreshToken.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(RefreshTokenView);
