import React from 'react';
import { Form, Input, Button, Icon, Modal, Row, Col, Select } from 'antd';
import { connect } from 'react-redux';
import formActions from 'pbr-admin/src/store/Form/Form.actions';
import FormHoc from 'pbr-admin/src/components/Form/FormHoc';
import FormModal from 'pbr-admin/src/components/Form/FormModal';
import {
  apiActions,
  viewActions,
} from 'store/Role/Role.actions.js';

export class RoleForm extends React.Component {

  toggleAction = (values) => values.role_id != undefined;

  normatize = (values) => ({
    ...values,
    id: values.role_id
  });

  handleSubmit = (e) => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(this.toggleAction(values));
        if (this.toggleAction(values)) {
          this.props.update(this.normatize(values));
          return;
        }
        this.props.create(values);
      }
     });
  }
  componentWillReceiveProps = (props) => {
    if (props.values === undefined) {
      return;
    }
    if (props.values && props.values !== this.props.values) {
      this.props.form.setFieldsValue(props.values);
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    return(
      <FormModal
        visible={this.props.ui.form.display}
        cancel={this.props.cancelCreate}
        reset={this.props.startReset}
        id={this.props.values.role_id}
        label="Role"
        loading={this.props.loading}
        submit={this.handleSubmit}
      >
          {getFieldDecorator('role_id')(
            <Input type="hidden"/>
          )}
          <Form.Item label="Role">
          {getFieldDecorator('role', {
            rules: [{ required: true, message: 'Please input role!' }],
          })(
            <Input placeholder="Role" disabled={this.props.loading} />
          )}
          </Form.Item>
          <Form.Item label="Parent">
          {getFieldDecorator('parent_id')(
            <Select>
              {this.props.roles.map(role => {
                return <Select.Option key={role.role_id} value={role.role_id}>{role.role}</Select.Option>
              })}
            </Select>
          )}
          </Form.Item>

      </FormModal>
    )
  }
}

export default connect(
  state => ({
    roles: state.role && state.role.list && state.role.list.data && state.role.list.data.results || [],
    ui: state.ui && state.ui.ui,
    values: state.role
      && state.role.get
      && state.role.get.data
      && state.role.get.data.result
      || {},
    errors: state.role
      && state.role.add
      && state.role.add.error
      || state.role
      && state.role.edit
      && state.role.edit.error
      ||{},
    loading: state.role.add.pending || state.role.edit.pending,
    formUi: state.form,
  }),
  dispatch => ({
    cancelCreate: () => dispatch(viewActions.create.cancel()),
    startReset: () => dispatch(formActions.resetFormStart()),
    completeReset: () => dispatch(formActions.resetFormComplete()),
    create: (values) => dispatch(apiActions.add.request(values)),
    resetAdd: () => dispatch(apiActions.add.reset()),
    update: (values) => dispatch(apiActions.edit.request(values))
  })
)(FormHoc(RoleForm));
