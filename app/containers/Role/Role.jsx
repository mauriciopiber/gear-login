import React from 'react';
import { connect } from 'react-redux';
import RoleList from './RoleList';
import RoleView from './RoleView';
import RoleForm from './RoleForm';
import RoleDel from './RoleDel';

class Role extends React.Component {
  render() {
    return (
      <div>
        <RoleList />
        <RoleView />
        <RoleForm />
        <RoleDel />
      </div>
    )
  }
}

export default connect()(Role)
