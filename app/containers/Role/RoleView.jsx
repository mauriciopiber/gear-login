import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/Role/Role.actions';

const RoleView = ({values, ui, cancelView}) => {
  const data = [{
      label: 'Role',
      column: 'role'
  },{
    label: 'Parent',
    column: 'parent_role'
  }];

  return(
    <View
      label="Role"
      id={values.role_id}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.role
      && state.role.get
      && state.role.get.data
      && state.role.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(RoleView);
