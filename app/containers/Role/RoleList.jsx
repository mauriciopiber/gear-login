import React from 'react';
import { connect } from 'react-redux';
import { Table, Row, Col } from 'antd';
import {
  apiActions,
  viewActions
} from 'store/Role/Role.actions';
import propTypes from 'prop-types';
import TableHoc from 'pbr-admin/src/components/Table/TableHoc';
import AppTableAction from 'pbr-admin/src/components/Table/TableAction';
import TextFilter from 'pbr-admin/src/components/Table/Filter/TextFilter';
import PageHeader from 'pbr-admin/src/components/PageHeader/PageHeader';
import { roleFilter } from 'store/Filters/Filters.actions';

export class RoleList extends React.Component {
  componentDidMount() {
    this.props.loadRoleFilter();
  }

  componentWillUnmount() {
    this.props.resetRoleFilter();
  }

  render() {

    const roleFilters = this.props.roleFilter.map(qt => ({text: qt.role, value: qt.role_id}));
    const columns = [
      {
        title: 'Id',
        dataIndex: 'role_id',
        key: 'role_id',
        width: 50,
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Role',
        dataIndex: 'role',
        key: 'role',
        sorter: true,
        filterDropdown: TextFilter(),
      },
      {
        title: 'Parent',
        dataIndex: 'parent_role',
        key: 'role_id',
        sorter: true,
        filters: roleFilters,
        //filterDropdown: TextFilter(),
      },
      {
        title: 'Action',
        key: 'action',
        width: 180,
        render: AppTableAction({...this.props, key: 'role_id'})
      },
    ];
    return (
      <React.Fragment>
        <PageHeader
          startCreate={this.props.startCreate}
          startRemoveAll={this.props.startRemoveAll}
          hasSelected={this.props.hasSelected}
          tableSelected={this.props.tableSelected}
        />
        <Table
          rowKey={orm => orm.role_id}
          dataSource={this.props.data ? this.props.data : []}
          columns={columns}
          loading={this.props.isLoading}
          rowSelection={this.props.rowSelection}
          loading={this.props.isLoading}
          onChange={this.props.handleTableChange}
          pagination={this.props.pagination}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    roleFilter: state.filters && state.filters.role && state.filters.role.data && state.filters.role.data.results || [],
    data: state.role && state.role.list && state.role.list.data && state.role.list.data.results || [],
    page: state.role && state.role.list && state.role.list.data && state.role.list.data.page || undefined,
    total: state.role && state.role.list && state.role.list.data && parseInt(state.role.list.data.total) || undefined,
    isLoading: state.role && state.role.list && state.role.list.pending || false,
    hasSelected: state.role && state.role.list.selected && state.role.list.selected.length > 0,
    tableSelected: state.role && state.role.list.selected || []
  }),
  dispatch => ({
    loadRoleFilter: () => dispatch(roleFilter.request()),
    resetRoleFilter: () => dispatch(roleFilter.reset()),
    load: (params) => dispatch(apiActions.list.request(params)),
    reset: () => dispatch(apiActions.list.reset()),
    startView: payload =>
      dispatch(viewActions.view.start(payload)),
    startDelete: payload =>
      dispatch(viewActions.delete.start(payload)),
    startEdit: payload =>
      dispatch(viewActions.edit.start(payload)),
    select: payload => dispatch(apiActions.list.select({ payload })),
    startCreate: () => dispatch(viewActions.create.start()),
    startRemoveAll: (payload) => dispatch(viewActions.deleteAll.start(payload)),
  }),
)(TableHoc(RoleList));
