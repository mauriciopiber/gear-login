import React from 'react';
import { connect } from 'react-redux';
import UserList from './UserList';
import UserView from './UserView';
import UserForm from './UserForm';
import UserDel from './UserDel';

class User extends React.Component {
  render() {
    return (
      <div>
        <UserList />
        <UserView />
        <UserForm />
        <UserDel />
      </div>
    )
  }
}

export default connect()(User)
