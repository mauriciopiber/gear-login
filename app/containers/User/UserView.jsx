import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import View from 'components/View/View';
import { viewActions } from 'store/User/User.actions';

const UserView = ({values, ui, cancelView}) => {
  const data = [{
      label: 'E-mail',
      column: 'email'
  },{
    label: 'Role',
    column: 'role'
  }];

  return(
    <View
      label="User"
      id={values.username}
      data={data}
      visible={ui.view.display}
      cancel={cancelView}
      values={values}
    />
  )
}

export default connect(
  state => ({
    values: state.user
      && state.user.get
      && state.user.get.data
      && state.user.get.data.result
      || {},
    ui: state.ui.ui,
  }),
  dispatch => ({
    cancelView: () => dispatch(viewActions.view.cancel()),
  })
)(UserView);
