import React from 'react';
import { Form, Input, Button, Icon, Modal, Row, Col, Select } from 'antd';
import { connect } from 'react-redux';
import formActions from 'pbr-admin/src/store/Form/Form.actions';
import FormHoc from 'pbr-admin/src/components/Form/FormHoc';
import FormModal from 'pbr-admin/src/components/Form/FormModal';
import {
  apiActions,
  viewActions,
} from 'store/User/User.actions.js';

export class UserForm extends React.Component {

  toggleAction = (values) => values.username;

  normatize = (values) => ({
    ...values,
    id: values.username
  });

  handleSubmit = (e) => {

    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (this.toggleAction(values)) {
          this.props.update(this.normatize(values));
          return;
        }
        this.props.create(values);
      }
     });
  }
  componentWillReceiveProps = (props) => {
    if (props.values === undefined) {
      return;
    }
    if (props.values && props.values !== this.props.values) {
      this.props.form.setFieldsValue(props.values);
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    const Password = !this.props.values.username ? (
        <Row>
          <Col span="12">
            <Form.Item label="Password">
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Please input password!' }],
            })(
              <Input placeholder="Password" disabled={this.props.loading} />
            )}
            </Form.Item>
          </Col>
          <Col span="12">
            <Form.Item label="Confirm Password">
            {getFieldDecorator('confirmPassword', {
              rules: [{ required: true, message: 'Please input confirm password!' }],
            })(
              <Input placeholder="Confirm Password" disabled={this.props.loading} />
            )}
            </Form.Item>

          </Col>
        </Row>
      ) : <div/>;

    return(
      <FormModal
        visible={this.props.ui.form.display}
        cancel={this.props.cancelCreate}
        reset={this.props.startReset}
        id={this.props.values.username}
        label="User"
        loading={this.props.loading}
        submit={this.handleSubmit}
      >
          {getFieldDecorator('username')(
            <Input type="hidden"/>
          )}
          <Form.Item label="E-mail">
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input e-mail!' }],
          })(
            <Input placeholder="E-mail" disabled={this.props.loading} />
          )}
          </Form.Item>
          <Form.Item label="Role">
          {getFieldDecorator('role_id')(
            <Select>
              {this.props.roles.map(role => {
                return <Select.Option key={role.role_id} value={role.role_id}>{role.role}</Select.Option>
              })}
            </Select>
          )}
          </Form.Item>
          {Password}

      </FormModal>
    )
  }
}

export default connect(
  state => ({
    roles: state.filters && state.filters.role && state.filters.role.data && state.filters.role.data.results || [],
    ui: state.ui && state.ui.ui,
    values: state.user
      && state.user.get
      && state.user.get.data
      && state.user.get.data.result
      || {},
    errors: state.user
      && state.user.add
      && state.user.add.error
      || state.user
      && state.user.edit
      && state.user.edit.error
      ||{},
    loading: state.user.add.pending || state.user.edit.pending,
    formUi: state.form,
  }),
  dispatch => ({
    cancelCreate: () => dispatch(viewActions.create.cancel()),
    startReset: () => dispatch(formActions.resetFormStart()),
    completeReset: () => dispatch(formActions.resetFormComplete()),
    create: (values) => dispatch(apiActions.add.request(values)),
    resetAdd: () => dispatch(apiActions.add.reset()),
    update: (values) => dispatch(apiActions.edit.request(values))
  })
)(FormHoc(UserForm));
