import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Layout, Menu, Breadcrumb, Icon, Button } from 'antd';
import { connect } from 'react-redux';
//import AuthAction from 'middleware/Auth/Auth.actions';
import AppAdmin from 'pbr-admin/lib/components/App/AppAdmin';
import AppHeader from 'containers/App/AppHeader';
import routes from 'routes/admin.js';

class Admin extends React.Component {
  componentDidMount() {
    //this.props.init();
  }

  render() {
    return (
      <AppAdmin
        routes={routes}
        header={AppHeader}
      />
    );
  }
}

export default withRouter(connect(
  state => ({
    //auth: state.auth.user,
    //isLogged: state.auth.user && state.auth.user.email ? true : false
  }),
  dispatch => ({
    //init: () => dispatch(AuthAction.onInit())
  })
)(Admin));
