import sagaCreator from 'pbr-action-creator/src/apiCreator/apiSagaCreator';
import history from 'store/history';
import { select, take, put, call, all } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import ProfileApi from 'api/Profile';




export function* manageProfile() {
  while (true) {

    const { payload }  = yield take('START_MANAGE_PROFILE');
    // const { code, state } = payload;
    // yield put(viewAuthAction.authorizeStart());
    // yield call(delay, 1000);
    // yield put(viewAuthAction.authorizeAuth());
    // yield call(delay, 1000);
    //
    // yield put(apiAuthAction.request({
    //   code,
    //   state
    // }));
    //
    // const authResponse = yield take([apiAuthAction.ERROR, apiAuthAction.SUCCESS]);
    //
    // if (authResponse.type === apiAuthAction.ERROR) {
    //   yield put(viewAuthAction.authorizeError());
    //   return;
    // }
    //
    // yield put(viewAuthAction.authorizeUserInfo());
    // yield call(delay, 1000);
    //
    // const token = authResponse.payload.access_token;
    //
    // yield put(userInfoAction.request({
    //   token
    // }));
    //
    // const userResponse = yield take([userInfoAction.ERROR, userInfoAction.SUCCESS]);
    //
    // if (userResponse.type === userInfoAction.ERROR) {
    //   yield put(viewAuthAction.authorizeError());
    //   return;
    // }
    //
    // const tokenSave = {
    //   access_token: authResponse.payload.access_token,
    //   refresh_token: authResponse.payload.refresh_token,
    //   timestamp: Date.now(),
    //   expires_in: authResponse.payload.expires_in
    // }
    //
    // yield all([
    //   put(AuthAction.saveToken(tokenSave)),
    //   put(tokenManager.persistToken(tokenSave)),
    //   put(AuthAction.saveUserInfo(userResponse.payload)),
    //   put(userManager.persistUserInfo(userResponse.payload))
    // ]);
    //
    //
    //
    // //get user info
    // yield put(viewAuthAction.authorizeRedirect());
    // yield call(delay, 1000);
    // yield call(history.push, '/');
  }
}
//
// function* retryTokenSaga() {
//
//   const { payload } = yield take('RETRY_TOKEN');
//   const { access_token, refresh_token } = payload;
//   //console.log('token', access_token, refresh_token);
//
//
//   const refreshAction = refreshTokenApi.request({
//     access_token,
//     refresh_token
//   });
//   yield put(refreshAction);
//
//   const refreshResp = yield take([refreshTokenApi.ERROR, refreshTokenApi.SUCCESS]);
//
//   if (refreshResp.type === refreshTokenApi.ERROR) {
//     yield put({type: 'LOGOUT_START'});
//     return
//   }
//
//   const tokenSave = {
//     access_token: refreshResp.payload.access_token,
//     refresh_token: refreshResp.payload.refresh_token,
//     timestamp: Date.now(),
//     expires_in: refreshResp.payload.expires_in
//   }
//
//   yield all([
//     put(AuthAction.saveToken(tokenSave)),
//     put(tokenManager.persistToken(tokenSave)),
//   ]);
//
// }
//
// function* initValidateTokenSaga() {
//
//   while(true) {
//     const { payload } = yield take(tokenManager.VALIDATE_TOKEN);
//     const { access_token, expires_in, timestamp } = payload;
//
//     const validTimestamp = new Date() < new Date(timestamp + (expires_in*1000));
//
//     if (validTimestamp) {
//       yield put(validateTokenApi.request(access_token));
//
//       const validateResp = yield take([validateTokenApi.ERROR, validateTokenApi.SUCCESS]);
//
//       if (validateResp.type === validateTokenApi.ERROR) {
//         yield put({type: 'RETRY_TOKEN', payload});
//       }
//       break;
//     }
//
//     yield put({type: 'RETRY_TOKEN', payload});
//     break;
//   }
// }

export default function* watchAuthSaga() {
  yield all([
    manageProfile(),
  ])
}
