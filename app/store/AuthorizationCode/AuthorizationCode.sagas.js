import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './AuthorizationCode.actions.js';
import AuthorizationCode from 'api/AuthorizationCode';

const authorizationCodeSaga = createSagaActions('AUTHORIZATION_CODE', apiActions, viewActions, AuthorizationCode);

export default authorizationCodeSaga;
