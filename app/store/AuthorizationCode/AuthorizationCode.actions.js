import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('AUTHORIZATION_CODE');

export const viewActions = createViewActions('AUTHORIZATION_CODE');
