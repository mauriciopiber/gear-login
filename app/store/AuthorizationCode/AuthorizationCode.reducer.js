import {
  createApiReducer
} from 'pbr-admin/src/store/AdminCreator/adminReducerCreator';

import {
  apiActions
} from './AuthorizationCode.actions.js';


const reducer = createApiReducer(apiActions);

export default reducer;
