import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import Sagas from './sagas';
import Reducer from './reducer';
//import Auth from 'middleware/Auth/Auth';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  Reducer,
  composeEnhancer(applyMiddleware(
    sagaMiddleware,
    //Auth
  )),
);

var sagasTask = sagaMiddleware.run(Sagas);
//window.saga = sagaMiddleware;

if (module.hot) {
  module.hot.accept();
  module.hot.accept('./sagas', () => {
    const nextRootSaga = require('./sagas').default;
    sagasTask.cancel();
    sagasTask.done.then(() => {
      sagasTask = sagaMiddleware.run(nextRootSaga);
    });
  });
}



if (module.hot) {
  module.hot.accept();
  //module.hot.accept('../middleware/Auth/Auth.reducer');
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./reducer', () => {
    const nextRootReducer = require('./reducer').default;
    store.replaceReducer(nextRootReducer);
  });
}

export default store;
