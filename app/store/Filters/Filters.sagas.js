import {
  all,
  takeEvery,
  put,
  select,
  call
} from 'redux-saga/effects';
//import { ui } from 'store/Ui/Ui.actions';
//import ResetAction from 'store/Form/Form.actions';
//import { delay } from 'redux-saga';
import apiSagaCreator from 'pbr-action-creator/src/apiCreator/apiSagaCreator';
import {
  userFilter,
  roleFilter,
  clientFilter,
} from './Filters.actions';
import UserApi from 'api/User';
import RoleApi from 'api/Role';
import ClientApi from 'api/Client';


const extractAdd = (action) => {
  return action.payload;
};

const validateApi = (result) => (result.error && result.messages) || (result.title && result.detail);

const userList = () => {
  return UserApi.apiList({
    fields: ['username', 'email'],
    results: -1,
    orderField: 'email'
  });
}

const roleList = () => {
  return RoleApi.apiList({
    fields: ['role_id', 'role'],
    results: -1,
    orderField: 'role'
  })
}

const clientList = () => {
  return ClientApi.apiList({
    fields: ['client_id'],
    results: -1,
    orderField: 'client_id'
  })
}

const userFilterSaga = apiSagaCreator(userFilter, userList, extractAdd, validateApi);
const roleFilterSaga = apiSagaCreator(roleFilter, roleList, extractAdd, validateApi);
const clientFilterSaga = apiSagaCreator(clientFilter, clientList, extractAdd, validateApi);

const filterSagas = function* () {
  yield all([
    userFilterSaga,
    roleFilterSaga,
    clientFilterSaga,
  ]);
}

export default filterSagas;
