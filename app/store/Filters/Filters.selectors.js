//import { createSelector } from 'reselect';


const getFilter = (filterKey) => (state) => {
  return state.filters
    && state.filters[filterKey]
    && state.filters[filterKey].data
    && state.filters[filterKey].data.results
    || [];
}


export const getUserFilter = getFilter('user');

export const getRoleFilter = getFilter('role');

export const getClientFilter = getFilter('client');
