import apiActionCreator from 'pbr-action-creator/src/apiCreator/apiActionCreator';


export const userFilter = apiActionCreator('USER_FILTER');

export const roleFilter = apiActionCreator('ROLE_FILTER');

export const clientFilter = apiActionCreator('CLIENT_FILTER');
