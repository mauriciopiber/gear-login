import apiReducerCreator from 'pbr-action-creator/src/apiCreator/apiReducerCreator';
import { combineReducers } from 'redux';

import {
  userFilter,
  roleFilter,
  clientFilter,
} from './Filters.actions';


const filterReducer = combineReducers({
  user: apiReducerCreator(userFilter),
  role: apiReducerCreator(roleFilter),
  client: apiReducerCreator(clientFilter)
});

export default filterReducer;
