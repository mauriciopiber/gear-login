import apiActionCreator from 'pbr-action-creator/src/apiCreator/apiActionCreator';


export const authActions = {
  get:  apiActionCreator(`GET_AUTH`),
  update: apiActionCreator('UPDATE_AUTH')
};
