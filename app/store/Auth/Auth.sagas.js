import apiSagaCreator from 'pbr-action-creator/src/apiCreator/apiSagaCreator';
import AuthApi from 'api/Auth';
import UserApi from 'api/User';
import { authActions } from 'store/Auth/Auth.actions';
import {
  all,
} from 'redux-saga/effects';

const extractAdd = (action) => {
  return action.payload;
};

const validateApi = (result) => (result.error && result.messages) || (result.title && result.detail);


const authSagas = function* () {
  yield all([
    apiSagaCreator(authActions.get, AuthApi.apiList, extractAdd, validateApi),
    apiSagaCreator(authActions.update, AuthApi.apiEdit, extractAdd, validateApi)
  ])
}

export default authSagas;
