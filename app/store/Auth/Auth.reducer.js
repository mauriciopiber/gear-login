import apiReducerCreator from 'pbr-action-creator/src/apiCreator/apiReducerCreator';
import { combineReducers } from 'redux';
import { authActions } from './Auth.actions';

export default combineReducers({
  get: apiReducerCreator(authActions.get),
  update: apiReducerCreator(authActions.update)
});
