import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './AccessToken.actions.js';
import AccessToken from 'api/AccessToken';

const accessTokenSaga = createSagaActions('ACCESS_TOKEN', apiActions, viewActions, AccessToken);

export default accessTokenSaga;
