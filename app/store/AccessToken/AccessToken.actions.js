import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('ACCESS_TOKEN');

export const viewActions = createViewActions('ACCESS_TOKEN');
