import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './RefreshToken.actions.js';
import RefreshToken from 'api/RefreshToken';

const refreshTokenSaga = createSagaActions('REFRESH_TOKEN', apiActions, viewActions, RefreshToken);

export default refreshTokenSaga;
