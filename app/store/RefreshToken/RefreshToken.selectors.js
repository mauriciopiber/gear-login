import { createSelector } from 'reselect'



const getList = (listKey) => (state) => state[listKey] && state[listKey].list || undefined;
// const getAdd = (listKey) => (state) => state[listKey] && state[listKey].add || undefined;
// const getEdit = (listKey) => (state) => state[listKey] && state[listKey].edit || undefined;
// const getDel = (listKey) => (state) => state[listKey] && state[listKey].del || undefined;
// const getGet = (listKey) => (state) => state[listKey] && state[listKey].get || undefined;
//


const getRefreshTokenList = getList('refreshToken');




export const getRefreshTokenData = createSelector(
  [getRefreshTokenList],
  list => list && list.data && list.data.results || []
)

export const getRefreshTokenPage = createSelector(
  [getRefreshTokenList],
  list => list && list.data && list.data.page || undefined
)

export const getRefreshTokenTotal = createSelector(
  [getRefreshTokenList],
  list => list && list.data && parseInt(list.data.total) || undefined
)


export const getRefreshTokenLoading = (state) => state.refreshToken && state.refreshToken.list && state.refreshToken.list.pending || false;
export const getRefreshTokenHasSelected = (state) => state.refreshToken && state.refreshToken.list.selected && state.refreshToken.list.selected.length > 0;
export const getRefreshTokenSelected = (state) => state.refreshToken && state.refreshToken.list.selected || [];
