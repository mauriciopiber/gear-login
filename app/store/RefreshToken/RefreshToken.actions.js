import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('REFRESH_TOKEN');

export const viewActions = createViewActions('REFRESH_TOKEN');
