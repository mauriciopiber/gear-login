import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './Jwt.actions.js';
import Jwt from 'api/Jwt';

const jwtSaga = createSagaActions('JWT', apiActions, viewActions, Jwt);

export default jwtSaga;
