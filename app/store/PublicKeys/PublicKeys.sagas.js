import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './PublicKeys.actions.js';
import PublicKeys from 'api/PublicKeys';

const publicKeysSaga = createSagaActions('PUBLIC_KEYS', apiActions, viewActions, PublicKeys);

export default publicKeysSaga;
