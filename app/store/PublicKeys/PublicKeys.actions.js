import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('PUBLIC_KEYS');

export const viewActions = createViewActions('PUBLIC_KEYS');
