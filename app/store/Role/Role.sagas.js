import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './Role.actions.js';
import Role from 'api/Role';

const userSaga = createSagaActions('ROLE', apiActions, viewActions, Role);

export default userSaga;
