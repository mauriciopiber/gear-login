import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('ROLE');

export const viewActions = createViewActions('ROLE');
