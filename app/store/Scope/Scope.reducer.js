import {
  createApiReducer
} from 'pbr-admin/src/store/AdminCreator/adminReducerCreator';

import {
  apiActions
} from './Scope.actions.js';


const reducer = createApiReducer(apiActions);

export default reducer;
