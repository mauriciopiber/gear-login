import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './Scope.actions.js';
import Scope from 'api/Scope';

const scopeSaga = createSagaActions('SCOPE', apiActions, viewActions, Scope);

export default scopeSaga;
