import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('SCOPE');

export const viewActions = createViewActions('SCOPE');
