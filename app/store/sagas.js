import { all } from 'redux-saga/effects';

//oauth 2
import AccessTokenSaga from 'store/AccessToken/AccessToken.sagas';
import AuthorizationCodeSaga from 'store/AuthorizationCode/AuthorizationCode.sagas';
import ClientSaga from 'store/Client/Client.sagas';
import JwtSaga from 'store/Jwt/Jwt.sagas';
import PublicKeysSaga from 'store/PublicKeys/PublicKeys.sagas';
import RefreshTokenSaga from 'store/RefreshToken/RefreshToken.sagas';
import ScopeSaga from 'store/Scope/Scope.sagas';

//oauth user
import UserSaga from 'store/User/User.sagas';

//acl
import RoleSaga from 'store/Role/Role.sagas';


//suuport
import FiltersSaga from 'store/Filters/Filters.sagas'
// import FoodSaga from 'containers/Food/Food.sagas';
// import QuantityTypeSaga from 'containers/QuantityType/QuantityType.sagas';
// import AuthSaga, { authSaga, authApiSaga } from 'containers/Authorize/Authorize.sagas';
// import LogoutSaga from 'containers/Logout/Logout.sagas';
import AuthReducer from 'store/Auth/Auth.sagas';


function* indexSaga() {
  try {
    yield all([
      AccessTokenSaga,
      AuthorizationCodeSaga,
      ClientSaga,
      JwtSaga,
      PublicKeysSaga,
      RefreshTokenSaga,
      ScopeSaga,
      UserSaga,
      RoleSaga,
      AccessTokenSaga,
      AuthReducer(),
      FiltersSaga(),
      // GroupSaga(),
      // FoodSaga(),
      // AuthSaga(),
      // LogoutSaga()
      //authSaga(),
      //authApiSaga
    ]);
  } catch(e) {
    console.error(e);
  }

}

export default indexSaga;
