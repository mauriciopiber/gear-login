
export const getRefreshTokenData = (state) => state.refreshToken && state.refreshToken.list && state.refreshToken.list.data && state.refreshToken.list.data.results || [];
export const getRefreshTokenPage = (state) => state.refreshToken && state.refreshToken.list && state.refreshToken.list.data && state.refreshToken.list.data.page || undefined;
export const getRefreshTokenTotal = (state) => state.refreshToken && state.refreshToken.list && state.refreshToken.list.data && parseInt(state.refreshToken.list.data.total) || undefined;
export const getRefreshTokenLoading = (state) => state.refreshToken && state.refreshToken.list && state.refreshToken.list.pending || false;
export const getRefreshTokenHasSelected = (state) => state.refreshToken && state.refreshToken.list.selected && state.refreshToken.list.selected.length > 0;
export const getRefreshTokenSelected = (state) => state.refreshToken && state.refreshToken.list.selected || [];
