import { combineReducers } from 'redux';
import UiReducer from 'pbr-admin/src/store/Ui/Ui.reducer';
import FormReducer from 'pbr-admin/src/store/Form/Form.reducer';
import FiltersReducer from 'store/Filters/Filters.reducer';


import AccessTokenReducer from 'store/AccessToken/AccessToken.reducer';
import AuthorizationCodeReducer from 'store/AuthorizationCode/AuthorizationCode.reducer';
import ClientReducer from 'store/Client/Client.reducer';
import JwtReducer from 'store/Jwt/Jwt.reducer';
import PublicKeysReducer from 'store/PublicKeys/PublicKeys.reducer';
import RefreshTokenReducer from 'store/RefreshToken/RefreshToken.reducer';
import ScopeReducer from 'store/Scope/Scope.reducer';
import UserReducer from 'store/User/User.reducer';
import RoleReducer from 'store/Role/Role.reducer';
import AuthReducer from 'store/Auth/Auth.reducer';
// import QuantityTypeReducer from 'containers/QuantityType/QuantityType.reducer';
// import FoodReducer from 'containers/Food/Food.reducer';
// import FormReducer from 'containers/Form/Form.reducer';
// import AuthReducer from 'containers/Authorize/Authorize.reducer';
// import LogoutReducer from 'containers/Logout/Logout.reducer';

const reducer = combineReducers({
   ui: UiReducer,
   form: FormReducer,
   filters: FiltersReducer,
   user: UserReducer,
   role: RoleReducer,
   accessToken: AccessTokenReducer,
   authorizationCode: AuthorizationCodeReducer,
   jwt: JwtReducer,
   client: ClientReducer,
   publicKeys: PublicKeysReducer,
   refreshToken: RefreshTokenReducer,
   scope: ScopeReducer,
   auth: AuthReducer,
});

export default reducer;
