import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('USER');

export const viewActions = createViewActions('USER');
