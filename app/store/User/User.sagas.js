import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './User.actions.js';
import User from 'api/User';

const userSaga = createSagaActions('USER', apiActions, viewActions, User);

export default userSaga;
