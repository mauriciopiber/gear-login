import {
  createViewActions,
  createApiActions
} from 'pbr-admin/src/store/AdminCreator/adminActionCreator';


export const apiActions = createApiActions('CLIENT');

export const viewActions = createViewActions('CLIENT');
