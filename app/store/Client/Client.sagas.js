import {
  createSagaActions
} from 'pbr-admin/src/store/AdminCreator/adminSagaCreator';
import {
  apiActions,
  viewActions,
} from './Client.actions.js';
import Client from 'api/Client';

const clientSaga = createSagaActions('CLIENT', apiActions, viewActions, Client);

export default clientSaga;
