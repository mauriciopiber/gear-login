import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { Router } from 'react-router-dom';
import Admin from 'containers/Admin/Admin';
//import 'normalize.css';
import 'antd/dist/antd.css';
import history from './store/history'
import store from 'store/store';

/*



import './index.scss';
*/

//window.store = store;

//window.fetchMock = require('fetch-mock');
//window.sinon = require('sinon');


const render = component => {
  ReactDOM.render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <Router history={history}>
          {component}
        </Router>
      </IntlProvider>
    </Provider>
  , document.getElementById('app')
  );
}

render(<Admin  />);

if (module.hot) {
  module.hot.accept();
  module.hot.accept('containers/Admin/Admin.jsx', () => {
    const NextAdmin = require('containers/Admin/Admin.jsx').default;
    render(<NextAdmin  />);
  });
}

window.store = store;
