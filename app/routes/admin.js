import React from 'react';
import Loadable from 'react-loadable';
import { Icon } from 'antd';

const Loading = (props) => {

  if (props.error) {
    console.error(props.error);
  }

  return (
    <div>Loading...</div>
  );
}

const Home = Loadable({
  loader: () => import('containers/Home/Home'),
  loading: Loading,
});


const User = Loadable({
  loader: () => import('containers/User/User'),
  loading: Loading,
});

const Role = Loadable({
  loader: () => import('containers/Role/Role'),
  loading: Loading,
});

const Scope = Loadable({
  loader: () => import('containers/Scope/Scope'),
  loading: Loading,
});

const Client = Loadable({
  loader: () => import('containers/Client/Client'),
  loading: Loading,
});

const AccessToken = Loadable({
  loader: () => import('containers/AccessToken/AccessToken'),
  loading: Loading,
});

// const Jwt = Loadable({
//   loader: () => import('containers/Jwt/Jwt'),
//   loading: Loading,
// });

const AuthorizationCode = Loadable({
  loader: () => import('containers/AuthorizationCode/AuthorizationCode'),
  loading: Loading,
});

const RefreshToken = Loadable({
  loader: () => import('containers/RefreshToken/RefreshToken'),
  loading: Loading,
});

const PublicKeys = Loadable({
  loader: () => import('containers/PublicKeys/PublicKeys'),
  loading: Loading,
});

const Profile = Loadable({
  loader: () => import('containers/Profile/Profile'),
  loading: Loading,
});


//import Home from 'containers/Home/Home';


const routes = [
  {
    to: '/',
    label: 'Home',
    exact: true,
    render: Home,
    icon: <Icon type="cloud" theme="twoTone" />,
    private: false,
  },
  {
    submenu: true,
    name: 'OAuth2',
    routes: [
      {
        to: '/public-keys',
        label: 'Public Keys',
        render: PublicKeys,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false
      },
      {
        to: '/refresh-token',
        label: 'Refresh Token',
        render: RefreshToken,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false
      },
      {
        to: '/access-token',
        label: 'Access Token',
        render: AccessToken,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false,
      },
      {
        to: '/authorization-code',
        label: 'Authorization Code',
        render: AuthorizationCode,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false,
      },
    ],
  },
  {
    submenu: true,
    name: 'Users',
    routes: [
      {
        to: '/user',
        label: 'User',
        render: User,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false,
      },
      {
        to: '/role',
        label: 'Role',
        render: Role,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false,
      },
      {
        to: '/scope',
        label: 'Scope',
        render: Scope,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false,
      },
      {
        to: '/client',
        label: 'Client',
        render: Client,
        icon: <Icon type="cloud" theme="twoTone" />,
        private: false,
      },
    ],
  },
  // {
  //   to: '/jwt',
  //   label: 'Jwt',
  //   render: Jwt,
  //   icon: <Icon type="cloud" theme="twoTone" />,
  //   private: false,
  // },

  {
    to: '/profile',
    label: 'Profile',
    render: Profile,
    icon: <Icon type="cloud" theme="twoTone" />,
    private: false,
  },
];

export default routes;
