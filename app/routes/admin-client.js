import React from 'react';
import Loadable from 'react-loadable';
import { Icon } from 'antd';

const Loading = () => <div>Loading...</div>;

const Home = Loadable({
  loader: () => import('containers/Home/Home'),
  loading: Loading,
});

const Profile = Loadable({
  loader: () => import('containers/Profile/Profile'),
  loading: Loading,
});

//import Home from 'containers/Home/Home';


const routes = [
  {
    to: '/',
    label: 'Home',
    render: Home,
    icon: <Icon type="cloud" theme="twoTone" />,
    private: false,
  },
  {
    to: '/profile',
    label: 'Profile',
    render: Profile,
    icon: <Icon type="cloud" theme="twoTone" />,
    private: false,
  },
];

export default routes;
