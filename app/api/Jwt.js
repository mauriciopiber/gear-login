import { baseApi } from './config';
import AbstractApi from 'pbr-admin/src/api/abstractApi';

class GroupApi extends AbstractApi {
  constructor() {
    super();
    //console.log(process.env.NODE_ENV);
    this.endPoint = `${baseApi}/jwt`
  }
}

export default new GroupApi();
