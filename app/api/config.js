
export const baseApi = process.env.NODE_ENV === 'production'
  ? 'http://login.piber.network/gear-login/api'
  : 'http://localhost:9090/gear-login/api';
