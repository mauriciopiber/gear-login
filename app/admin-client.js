import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { Router } from 'react-router-dom';
import AdminClient from 'containers/Admin/AdminClient';
//import 'normalize.css';
import 'antd/dist/antd.css';
import history from './store/history'
import store from 'store/store';

/*



import './index.scss';
*/

//window.store = store;

//window.fetchMock = require('fetch-mock');
//window.sinon = require('sinon');

const render = component => {
  ReactDOM.render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <Router history={history}>
          {component}
        </Router>
      </IntlProvider>
    </Provider>
  , document.getElementById('app')
  );
}

render(<AdminClient />);

if (module.hot) {
  module.hot.accept();
  module.hot.accept('containers/Admin/AdminClient.jsx', () => {
    const NextAdminClient = require('containers/Admin/AdminClient.jsx').default;
    render(<NextAdminClient />);
  });
}
