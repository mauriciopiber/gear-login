import React from 'react';
import ReactDOM from 'react-dom';
//import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { Router } from 'react-router-dom';
import Login from 'containers/Login/Login';
//import 'normalize.css';
import 'antd/dist/antd.css';
//import history from './store/history'

/*
import store from 'store/store';

console.log('login foi');
import './index.scss';
*/

//window.store = store;

//window.fetchMock = require('fetch-mock');
//window.sinon = require('sinon');

const formError = (error) ? error : undefined;
const formValues = (values) ? values : undefined;

const render = component => {
  ReactDOM.render(
    // <Provider store={store}>
      <IntlProvider locale="en">
        {/* <Router history={history}> */}
          {component}
        {/* </Router> */}
      </IntlProvider>
    // </Provider>
  , document.getElementById('app')
  );
}

render(<Login errors={formError} values={formValues}/>);

if (module.hot) {
  module.hot.accept();
  module.hot.accept('containers/Login/Login.jsx', () => {
    const NextLogin = require('containers/Login/Login.jsx').default;
    render(<NextLogin  errors={formError} values={formValues} />);
  });
}
