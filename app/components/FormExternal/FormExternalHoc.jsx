import React from 'react';
import { Form } from 'antd';

const FormExternalHoc = (WrappedComponent) => {
  class HOC extends React.Component {

    state = {
      valid: false
    };

    componentDidMount() {
      //console.log(this.props);
        if (this.props.values) {
          console.log(this.props);
          this.props.form.setFieldsValue(this.props.values);
        }

        if (this.props.errors) {
          const validation = {};
          const values = this.props.form.getFieldsValue();

          Object.keys(values).map(key => {
            validation[key] = {
              value: values[key],
            }
            if (this.props.errors[key]) {
              validation[key].errors = Object.keys(this.props.errors[key]).map(message => {
                return new Error(this.props.errors[key][message]);
              })
            }

          });
          this.props.form.setFields(validation);
        }
    }

    handleSubmit = (e) => {
      if (this.state.valid) {
        this.setState({valid: false});
        return;
      }
      e.preventDefault();

      this.props.form.validateFields((err, values) => {
        if (!err) {
          this.setState({valid: true});
          document.getElementById(e.currentTarget.id).submit();
        }
      });
    }
    render() {

      return <WrappedComponent {...this.props} handleSubmit={this.handleSubmit}/>;
    }
  }

  return Form.create()(HOC);
};

export default FormExternalHoc;
