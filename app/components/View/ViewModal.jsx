import React from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Modal, Form, Button } from 'antd';

class ViewModal extends React.Component {
  render() {
    const { children, form } = this.props;

    return (
        <Modal
          visible={this.props.visible}
          onCancel={this.props.cancel}
          footer={false}
          width={'50%'}
        >
          {children}
        </Modal>

    )
  }
}

ViewModal.propTypes = {
  label: propTypes.string,
  id: propTypes.number,
  visible: propTypes.bool.isRequired,
  cancel: propTypes.func.isRequired,
  submit: propTypes.func
};

export default ViewModal;
