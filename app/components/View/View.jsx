import React from 'react';
import propTypes from 'prop-types';
import { Card, Table } from 'antd';
import ViewModal from 'components/View/ViewModal';

const View = ({label, id, data, visible, cancel, values}) => {
  const columns = [{
    title: 'Column',
    dataIndex: 'column',
    key: 'column'
  },{
    title: 'Value',
    dataIndex: 'value',
    key: 'value'
  }];
  const output = [];

  for (let i = 0; i < data.length; i++) {
    output.push({
      key: i,
      column: data[i].label,
      value: values[data[i].column]
    });
  }


  return(
    <ViewModal
      visible={visible}
      cancel={cancel}
    >
      <Card title={label+' #'+id} style={{marginTop: '25px'}}>
        <Table
          dataSource={output}
          columns={columns}
          pagination={false}
        />
      </Card>
    </ViewModal>
  )
}

View.propTypes = ({
  label: propTypes.string.isRequired,
  id: propTypes.number.isRequired,
  visible: propTypes.bool.isRequired,
  cancel: propTypes.func.isRequired,
  data: propTypes.array.isRequired,
});

View.defaultProps = ({
  data: [],
});

export default View;
