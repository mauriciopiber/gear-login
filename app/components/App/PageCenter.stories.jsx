import React from 'react';
import { storiesOf } from '@storybook/react';
import PageCenter from './PageCenter';

storiesOf('Page Center', module)
  .add('Page Center', () => <PageCenter><div>haha</div></PageCenter>);
