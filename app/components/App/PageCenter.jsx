import React from 'react';
import { Layout, Row } from 'antd';
//import propTypes from 'prop-types';

const PageCenter = (props) => {
  return (
    <Layout>
      <Layout.Content style={{height: '100vh'}}>
        <Row type="flex" type="flex" justify="space-around" align="middle" style={{'height': '100vh'}}>
          {props.children}
        </Row>
      </Layout.Content>
    </Layout>
  )
}



export default PageCenter;
