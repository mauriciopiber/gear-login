# Register Client from scratch

## pibernetwork
## no pass
#!/bin/bash

# curl -X POST \
#   http://localhost:9090/gear-login/api/client \
#   -H "Content-Type: multipart/form-data" \
#   -d @- <<'EOF'
#   {
#     "client_id": "pibernetwork",
#     "redirect_uri": "http://login.local/authorize",
#     "scope": [
#       "openid",
#       "profile",
#       "email",
#       "create",
#       "read"
#     ]
#   }
# EOF
#
# curl -X POST \
#   http://localhost:9090/gear-login/api/client \
#   -H "Content-Type: multipart/form-data" \
#   -d @- <<'EOF'
#   {
#     "client_id": "cursos-webflow",
#     "redirect_uri": "http://login.local/authorize",
#     "scope": [
#       "openid",
#       "profile",
#       "email",
#       "create",
#       "read"
#     ]
#   }
# EOF

#!/bin/bash
# curl -X POST \
#   http://localhost:9090/gear-login/api/user \
#   -H "Content-Type: multipart/form-data" \
#   -d @- <<'EOF'
#   {
#     "username": "mauriciopiber",
#     "password": "mypass",
#     "first_name": "myfirstname",
#     "last_name": "mylastname",
#     "email": "mauriciopiber@gmail.com"
#   }
# EOF

#!/bin/bash
curl -X POST \
  http://localhost:9090/gear-login/api/user \
  -H "Content-Type: multipart/form-data" \
  -d @- <<'EOF'
  {
    "username": "readpiber",
    "password": "mypass",
    "first_name": "myfirstname",
    "last_name": "mylastname",
    "email": "readpiber@gmail.com"
  }
EOF

#!/bin/bash
curl -X POST \
  http://localhost:9090/gear-login/api/user \
  -H "Content-Type: multipart/form-data" \
  -d @- <<'EOF'
  {
    "username": "createpiber",
    "password": "mypass",
    "first_name": "myfirstname",
    "last_name": "mylastname",
    "email": "createpiber@gmail.com"
  }
EOF

#!/bin/bash
curl -X POST \
  http://localhost:9090/gear-login/api/user \
  -H "Content-Type: multipart/form-data" \
  -d @- <<'EOF'
  {
    "username": "guest",
    "password": "mypass",
    "first_name": "myfirstname",
    "last_name": "mylastname",
    "email": "guest@gmail.com"
  }
EOF

# Register Users for clients

## mauriciopiber@gmail.com
## ..

## readpiber@gmail.com

## createpiber@gmail.com

## guest
