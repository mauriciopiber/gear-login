#!/bin/bash

cp config/autoload/local.php.dist config/autoload/local.php

echo "Deploy Testing - Composer"

ls -l vendor/autoload.php &> /dev/null

if [ "$?" != "0" ]; then
    composer update
fi;

echo "Deploy Testing - VirtualHost"

ls -l /etc/apache2/sites-enabled/gear-login.gear.dev.conf > /dev/null
if [ "$?" != "0" ]; then
    sudo vendor/bin/virtualhost  $(pwd) gear-login.gear.dev TESTING
fi

echo "Deploy Testing - Database"
sudo vendor/bin/install-db-module gear-login.mysql.sql GearLogin

chmod 777 -R build
chmod 777 -R data/session
chmod 777 -R data/cache
chmod 777 -R data/DoctrineModule
chmod 777 -R data/DoctrineORMModule

echo "flush_all" | nc -q 2 localhost 11211
