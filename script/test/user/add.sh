#!/bin/bash
curl -X POST \
  http://localhost:9090/gear-login/api/user \
  -H "Content-Type: application/json" \
  -d @- <<'EOF'
  {
    "email": "testet33325este@gmail.com",
    "password": "mypassword",
    "confirmPassword": "mypassword",
    "role_id": 1
  }
EOF
