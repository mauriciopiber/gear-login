#!/bin/bash

curl -X POST \
  http://localhost:9090/gear-login/api/client \
  -H "Content-Type: multipart/form-data" \
  -d @- <<'EOF'
  {
    "client_id": "login",
    "client_secret": "loginsecrent",
    "redirect_uri": "http://localhost:8003/register"
  }
EOF
