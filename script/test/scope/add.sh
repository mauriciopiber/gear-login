#!/bin/bash

curl -X POST \
  http://localhost:9090/gear-login/api/scope \
  -H "Content-Type: multipart/form-data" \
  -d @- <<'EOF'
  {
    "scope": "read",
    "type": "supported",
    "is_default": null
  }
EOF
