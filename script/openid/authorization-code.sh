#!/bin/bash

###
## First Step: Enter the URL to get the CODE
###

## In Browser
# http://localhost:9090/oauth/authorize?response_type=code&client_id=testclient&redirect_uri=/oauth/receivecode&state=xyz&scope=openid


#http://localhost:9090/oauth/authorize?response_type=code%20id_token&client_id=login&redirect_uri=http://login.local/authorize&state=xyz&scope=openid,first_name,last_name

## Get the CODE
# Exemple: e712dad9b4a2e014a5dbdb0c498191314d92fcdf


## With the Code, get an Oauth2 Token

# curl -u testclient:testpass -X POST http://localhost:9090/oauth \
#   -d grant_type=authorization_code \
#   -d code=e712dad9b4a2e014a5dbdb0c498191314d92fcdf \
#   -d redirect_uri=/oauth/receivecode

 curl -X POST http://localhost:9090/oauth \
   -d grant_type=authorization_code \
   -d client_id=login \
   -d code=7eb3067d47e5a57e5e9787dc262328f8ef0583de \
   -d redirect_uri=http://login.local/authorize

####
### {
###   "access_token":"43d810ecb321291c13328e818ea4fec752ffba35",
###   "expires_in":3600,
###   "token_type":"Bearer",
###   "scope":null,
###   "refresh_token":"7428380f0a7a3011b2b21ac03a9b2c839a0a8668"
### }
####
