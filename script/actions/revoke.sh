#!/bin/bash

url="localhost:9090"
TOKEN=${1}

curl -X POST -d token=$TOKEN -d token_type_hint=access_token http://$url/oauth/revoke
