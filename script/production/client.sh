curl -X POST \
  http://localhost:9090/gear-login/api/client \
  -H "Content-Type: multipart/form-data" \
  -d @- <<'EOF'
  {
    "client_id": "store01",
    "client_secret": "mystore",
    "redirect_uri": "http://login.local/authorize",
    "scope": [
      "create",
      "read"
    ]
  }
EOF
