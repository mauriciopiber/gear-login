#!/bin/bash

echo "Deploy Develoment - Composer"

ls -l vendor/autoload.php &> /dev/null

if [ "$?" != "0" ]; then
    composer update
fi;

echo "Deploy Develoment - VirtualHost"

ls -l /etc/apache2/sites-enabled/gear-login.gear.dev.conf > /dev/null

if [ "$?" != "0" ]; then
    vendor/bin/virtualhost  $(pwd) gear-login.gear.dev DEVELOPMENT
fi

database=$(php -r '$global = require_once("config/autoload/global.php"); echo $global["doctrine"]["connection"]["orm_default"]["params"]["dbname"];')
username=$(php -r '$local = require_once("config/autoload/local.php"); echo $local["doctrine"]["connection"]["orm_default"]["params"]["user"];')
password=$(php -r '$local = require_once("config/autoload/local.php"); echo $local["doctrine"]["connection"]["orm_default"]["params"]["password"];')

echo "Deploy Develoment - Migrations/DB"
vendor/bin/database $database $username $password