#!/bin/bash

vendor/bin/unload-module BjyAuthorize
php public/index.php gear database fix
php public/index.php gear project fixture
php public/index.php gear project setUpAcl --memcached
php public/index.php gear module load BjyAuthorize --after=ZfcUserDoctrineORM

composer dump-autoload
