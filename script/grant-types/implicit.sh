#!/bin/bash

###
## First Step: Enter the URL to get the TOKEN
###

## In Browser
# http://localhost:9090/oauth/authorize?response_type=token&client_id=testclient&redirect_uri=/oauth/receivecode&state=xyz

## Get the TOKEN
# Exemple: 8461d1b0609d1f70c3e76419f9fbbbff9dfd9c4a

####
### access_token=8461d1b0609d1f70c3e76419f9fbbbff9dfd9c4a
### expires_in=3600
### token_type=Bearer
### state=xyz
#### 
