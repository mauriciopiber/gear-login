#!/bin/bash

#http --auth testclient:testpass -f POST http://$url/oauth grant_type=refresh_token refresh_token=4d55277225e3c46204e8ea255f0abe86cc47f829

ACCESS=${1}
TOKEN=${2}

#curl -X POST -u testclient:testpass -d grant_type=refresh_token -d refresh_token=$TOKEN http://localhost:9090/oauth
curl -X POST -d access_token=$ACCESS -d grant_type=refresh_token -d refresh_token=$TOKEN http://localhost:9090/oauth
