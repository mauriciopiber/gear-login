#!/bin/bash

url="localhost:9090"

echo ""
echo ""
curl http://$url/oauth
echo ""
echo ""
curl -X POST http://$url/oauth
echo ""
echo ""
curl -X POST http://$url/oauth -d grant_type=client_credentials
echo ""
echo ""
curl -X POST -u 'testclient:testpass' http://$url/oauth -d grant_type=client_credentials
echo ""
echo ""

####
### {
###   "access_token":"03df97f40070bd0b625080da5bf39e920fd5453b",
###   "expires_in":3600,
###   "token_type":"Bearer",
###   "scope":null
### }
####
