#!/bin/bash

url="localhost:9090"

echo ""
echo ""
#curl http://$url/oauth
echo ""
echo ""
#curl -X POST http://$url/oauth
echo ""
echo ""
#curl -X POST http://$url/oauth -d grant_type=password
echo ""
echo ""
#curl -X POST -u 'testclient:testpass' http://$url/oauth -d grant_type=password
echo ""
echo ""
#curl -X POST http://$url/oauth -d grant_type=password -d username=piber -d password=testpass
echo ""
echo ""
curl -X POST --user testclient:testpass http://$url/oauth -d grant_type=password # --verbose
echo ""
echo ""
curl -X POST --user testclient:testpass http://$url/oauth -d grant_type=password -d username=piber -d password=testpass #--verbose


####
### {
###   "access_token":"dbfdc9660480ca98b7577389867a96b753914ea9",
###   "expires_in":3600,
###   "token_type":"Bearer",
###   "scope":null,
###   "refresh_token":"14c4308987ec2f5ef5da85add3fc4ef02721270e"
### }
#### 
