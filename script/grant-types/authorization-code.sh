#!/bin/bash

###
## First Step: Enter the URL to get the CODE
###

## In Browser
# http://localhost:9090/oauth/authorize?response_type=code&client_id=testclient&redirect_uri=/oauth/receivecode&state=xyz&scope=openid
# http://localhost:9090/oauth/authorize?response_type=code&client_id=login&redirect_uri=http://login.local/authorize&state=xyz

## Get the CODE
# Exemple: e712dad9b4a2e014a5dbdb0c498191314d92fcdf


## With the Code, get an Oauth2 Token

# curl -u testclient:testpass -X POST http://localhost:9090/oauth \
#   -d grant_type=authorization_code \
#   -d code=e712dad9b4a2e014a5dbdb0c498191314d92fcdf \
#   -d redirect_uri=/oauth/receivecode

 # curl -u login: -X POST http://localhost:9090/oauth \
 #   -d grant_type=authorization_code \
 #   -d code=b10a41edebe20325cfe09c7dc0a3be4f85d565a2 \
 #   -d redirect_uri=http://login.local/authorize


   curl -u pibernetwork: -X POST http://localhost:9090/oauth \
     -d grant_type=authorization_code \
     -d code=b9d91fca12c06bb94f56c74fd60e63d72004cee2 \
     -d redirect_uri=http://login.local/authorize



####
### {
###   "access_token":"43d810ecb321291c13328e818ea4fec752ffba35",
###   "expires_in":3600,
###   "token_type":"Bearer",
###   "scope":null,
###   "refresh_token":"7428380f0a7a3011b2b21ac03a9b2c839a0a8668"
### }
####
