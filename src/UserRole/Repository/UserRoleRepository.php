<?php
namespace Gear\Login\UserRole\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRoleRepository extends AbstractRestRepository
{
  const TABLE = ['ur' => 'acl_user_role'];
  const ID = 'user_id';

  const COLUMNS = ['user_id', 'role_id'];

  const JOIN = [
      [
        'table' => ['u' => 'oauth_users'],
        'link' => 'u.username = ur.user_id',
        'columns' => ['email']
      ],
      [
        'table' => ['r' => 'acl_role'],
        'link' => 'r.role_id = ur.role_id',
        'columns' => ['role']
      ],
  ];

  const FILTER = [
    'user_id' => 'user_id',
    'role_id' => 'role_id',
  ];
}
