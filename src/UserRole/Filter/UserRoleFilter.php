<?php
namespace Gear\Login\UserRole\Filter;

use Zend\InputFilter\InputFilter;
use Gear\Rest\Filter\RestFilterInterface;
use Zend\Validator\Db\RecordExists;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package Gear\Login/User/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRoleFilter extends InputFilter implements RestFilterInterface
{
    public function __construct($dbAdapter) {
        $this->dbAdapter = $dbAdapter;
        return $this;
    }
    /**
     * Create the Filter to Form Elements
     *
     * @return UserFilter
     *
     */
    public function getInputFilter()
    {
        $userValidator = new RecordExists(
            [
                'table'   => 'oauth_users',
                'field'   => 'username',
                'adapter' => $this->dbAdapter,
            ]
        );

        $this->add([
            'name'       => 'user_id',
            'required'   => true,
            'validators' => [
                $userValidator
            ]
        ]);

        $roleValidator = new RecordExists(
            [
                'table'   => 'acl_role',
                'field'   => 'role_id',
                'adapter' => $this->dbAdapter,
            ]
        );

        $this->add([
            'name'       => 'role_id',
            'required'   => true,
            'validators' => [
                $roleValidator
            ]
        ]);
        return $this;
    }
}
