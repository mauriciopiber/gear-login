<?php
namespace Gear\Login\AuthorizationCode\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class AuthorizationCodeRepository extends AbstractRestRepository
{
    const TABLE = ['r' => 'oauth_authorization_codes'];
    const ID = 'authorization_code';

    const COLUMNS = ['authorization_code', 'user_id', 'client_id', 'expires', 'scope', 'redirect_uri', 'id_token'];

    const JOIN = [
        [
            'table' => ['u' => 'oauth_users'],
            'link' => 'r.user_id = u.username',
            'columns' => ['email'],
            'type' => 'left'
        ]
    ];

    const FILTER = [
        'user_id' => 'user_id',
        'client_id' => 'client_id'
    ];

}
