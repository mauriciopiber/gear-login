<?php
namespace Gear\Login\PublicKeys\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class PublicKeysRepository extends AbstractRestRepository
{
    const TABLE = ['c' => 'oauth_public_keys'];
    const ID = 'public_key';

    const COLUMNS = ['client_id', 'public_key', 'private_key', 'encryption_algorithm'];

    const JOIN = [];

    const FILTER = [
        'client_id' => 'client_id',
        'public_key' => 'public_key'
    ];

}
