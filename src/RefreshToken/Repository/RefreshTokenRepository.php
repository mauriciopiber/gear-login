<?php
namespace Gear\Login\RefreshToken\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class RefreshTokenRepository extends AbstractRestRepository
{
    const TABLE = ['c' => 'oauth_refresh_tokens'];
    const ID = 'refresh_token';

    const COLUMNS = ['client_id', 'refresh_token', 'user_id', 'expires', 'scope'];

    const JOIN = [
        [
            'table' => ['u' => 'oauth_users'],
            'link' => 'u.username = c.user_id',
            'columns' => ['email'],
            'type' => 'left'
        ]
    ];

    const FILTER = [
        'expires' =>'expires like ?',
        'client_id' => 'client_id',
        'user_id' => 'user_id',
        'email' => 'user_id'
    ];

}
