<?php
namespace Gear\Login\Client\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class ClientRepository extends AbstractRestRepository
{
    const TABLE = ['c' => 'oauth_clients'];
    const ID = 'client_id';

    const COLUMNS = ['client_id', 'client_secret', 'redirect_uri', 'grant_types', 'scope', 'user_id'];

    const JOIN = [];

    const FILTER = [
        'user_id' => 'user_id',
        'client_id' => 'client_id'
    ];

}
