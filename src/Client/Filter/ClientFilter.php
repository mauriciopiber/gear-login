<?php
namespace Gear\Login\Client\Filter;

use Zend\InputFilter\InputFilter;
use Gear\Rest\Filter\RestFilterInterface;
use Zend\Validator\Db\NoRecordExists;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package Gear\Login/Client/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class ClientFilter extends InputFilter implements RestFilterInterface
{
  public function __construct($dbAdapter) {
      $this->dbAdapter = $dbAdapter;
      return $this;
  }
    /**
     * Create the Filter to Form Elements
     *
     * @return ClientFilter
     *
     */
    public function getInputFilter()
    {

        $this->add(array(
            'name'       => 'client_id',
            'required'   => true,
        ));

        $this->add(array(
            'name'       => 'client_secret',
            'required'   => false,
        ));

        $this->add(array(
            'name'       => 'redirect_uri',
            'required'   => true,
        ));

        $this->add(array(
            'name'       => 'grant_types',
            'required'   => false,
        ));

        $this->add(array(
            'name'       => 'scope',
            'required'   => true,
        ));
        return $this;
    }
}
