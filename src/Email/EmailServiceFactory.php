<?php
namespace Gear\Login\Email;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Gear\Login\Email\EmailService;

class EmailServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $container)
    {
        $factory = new EmailService();
        return $factory;
    }


    /**
     * Create RegisterController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RegisterController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new EmailService();
      return $factory;
    }
}
