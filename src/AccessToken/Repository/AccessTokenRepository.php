<?php
namespace Gear\Login\AccessToken\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class AccessTokenRepository extends AbstractRestRepository
{
    const TABLE = ['r' => 'oauth_access_tokens'];
    const ID = 'access_token';

    const COLUMNS = ['access_token', 'user_id', 'client_id', 'expires', 'scope'];

    const JOIN = [];

    const FILTER = [
        'user_id' => 'user_id',
        'client_id' => 'client_id'
    ];

}
