<?php
namespace Gear\Login\AccessToken\Filter;

use Zend\InputFilter\InputFilter;
use Gear\Rest\Filter\RestFilterInterface;
use Zend\Validator\Db\NoRecordExists;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package Gear\Login/AccessToken/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class AccessTokenFilter extends InputFilter implements RestFilterInterface
{
  public function __construct($dbAdapter) {
      $this->dbAdapter = $dbAdapter;
      return $this;
  }
    /**
     * Create the Filter to Form Elements
     *
     * @return AccessTokenFilter
     *
     */
    public function getInputFilter()
    {

      return $this;
    }
}
