<?php
namespace Gear\Login\Scope\Controller;

use Gear\Rest\Controller\AbstractRestController;

/**
 * PHP Version 5
 *
 * @category Controller
 * @package Pbr\Food/MealFood/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class ScopeController extends AbstractRestController
{

}
