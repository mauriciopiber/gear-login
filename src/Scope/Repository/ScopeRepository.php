<?php
namespace Gear\Login\Scope\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class ScopeRepository extends AbstractRestRepository
{
    const TABLE = ['r' => 'oauth_scopes'];
    const ID = 'scope';

    const COLUMNS = ['scope', 'client_id', 'is_default', 'type'];

    const JOIN = [];

    const FILTER = [
        'scope' => 'scope',
    ];
}
