<?php
namespace Gear\Login\Admin\Home\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Gear\Login\Admin\Home\Controller\HomeController;
use Gear\Login\Service\AuthService;
use Gear\Login\Admin\Login\Service\LoginService;
use Gear\Login\Admin\Login\Form\LoginForm;
use Gear\Login\Admin\Login\Filter\LoginFilter;
use Interop\Container\ContainerInterface;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package Gear\Login/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class HomeControllerFactory implements FactoryInterface
{
    /**
     * Create HomeController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return HomeController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        //$serviceLocator = $container->getServiceLocator();

        $factory = new HomeController(
          $container->get(LoginService::class)
        );
        return $factory;
    }
}
