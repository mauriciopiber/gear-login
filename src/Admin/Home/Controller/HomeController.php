<?php
namespace Gear\Login\Admin\Home\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Serializer\Adapter\Json as JsonAdapter;
use Gear\Login\Admin\Home\Form\HomeForm;
use Zend\View\Model\ViewModel;

class HomeController extends AbstractRestfulController
{
    protected $jsonAdapter;

    protected $customerService;

    public function __construct($loginService)
    {
      $this->loginService = $loginService;
    }

    public function homeAction()
    {
      //se está logado, retorna para página principal
      if (!$this->loginService->getIdentity()) {
        return $this->redirect()->toUrl(
          '/login'
        );
      }


      $oauthParams = [
        'response_type' =>  $this->params()->fromQuery('response_type', null),
        'client_id' => $this->params()->fromQuery('client_id', null),
        'redirect_uri' => $this->params()->fromQuery('redirect_uri', null),
        'state' => $this->params()->fromQuery('state', null),
        'scope' => $this->params()->fromQuery('scope', null)
      ];

      if ($oauthParams['response_type'] && $oauthParams['client_id']) {
        $url = http_build_query($oauthParams);

        $response = $this->redirect()->toUrl(
          '/oauth/authorize?'.$url,
          [
            'authorized' => 'yes'
          ]
        );
        return $response;
      }

      $identity = $this->loginService->getIdentity();

      $role = isset($identity['roles'][0]) ? $identity['roles'][0] : 'user';

      switch ($role) {
        case 'master':
        case 'admin':
          $layout = 'admin';
          break;
        case 'user':
        case 'member':
        default:
          $layout = 'client';
      }

      $viewModel = new ViewModel();
      //$viewModel->setTemplate('gear/login/home');
      $viewModel->setVariables([
        'email' => $identity['email'],
        'layout' => $layout,
      ]);
      //$viewModel->setTerminal(true);
      return $viewModel;
    }
}
