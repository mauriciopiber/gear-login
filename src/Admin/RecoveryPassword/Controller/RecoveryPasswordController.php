<?php
namespace Gear\Login\Admin\RecoveryPassword\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Serializer\Adapter\Json as JsonAdapter;
use OAuth2\Request as OAuth2Request;
use OAuth2\Response as OAuth2Response;
use OAuth2\Server as OAuth2Server;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Http\PhpEnvironment\Response;
use Gear\Login\Admin\RecoveryPassword\Service\RecoveryPasswordService;
use Gear\Login\Admin\RecoveryPassword\Form\RecoveryPasswordForm;
use Gear\Login\Admin\RecoveryPassword\Filter\RecoveryPasswordFilter;

class RecoveryPasswordController extends AbstractActionController
{
    public function __construct(
      AuthenticationService $authService//,
      // RecoveryPasswordService $service,
      // RecoveryPasswordForm $form,
      // RecoveryPasswordFilter $filter
    ) {
        // $this->RecoveryPasswordService = $service;
        // $this->RecoveryPasswordForm = $form;
        // $this->RecoveryPasswordFilter = $filter;
      $this->authService = $authService;
    }

    public function recoveryPasswordAction()
    {
      $this->layout('layout');


      if ($this->authService->getIdentity()) {
        return $this->redirect()->toRoute(
          '/home'
        );
      }
    }
}
