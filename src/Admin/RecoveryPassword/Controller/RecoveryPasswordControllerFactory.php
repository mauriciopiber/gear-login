<?php
namespace Gear\Login\Admin\RecoveryPassword\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Gear\Login\Admin\RecoveryPassword\Controller\RecoveryPasswordController;
use Gear\Login\Auth\AuthService;
use Zend\Authentication\AuthenticationService;
use Gear\Login\Admin\RecoveryPassword\Service\RecoveryPasswordService;
use Gear\Login\Admin\RecoveryPassword\Form\RecoveryPasswordForm;
use Gear\Login\Admin\RecoveryPassword\Filter\RecoveryPasswordFilter;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package Gear\Login/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class RecoveryPasswordControllerFactory implements FactoryInterface
{
    /**
     * Create RecoveryPasswordController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RecoveryPasswordController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        $factory = new RecoveryPasswordController(
          $container->get(AuthenticationService::class)//,
          // $container->get(RecoveryPasswordService::class),
          // $container->get(RecoveryPasswordForm::class),
          // $container->get(RecoveryPasswordFilter::class)
        );

        return $factory;
    }
}
