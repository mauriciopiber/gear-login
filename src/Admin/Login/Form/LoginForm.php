<?php
namespace Gear\Login\Admin\Login\Form;

 use Zend\Form\Form;

class LoginForm extends Form
{
   public function __construct($name = null)
   {
      // we want to ignore the name passed
      parent::__construct('login');

      $this->add([
        'name' => 'response_type',
        'type' => 'hidden',
      ]);

      $this->add([
        'name' => 'client_id',
        'type' => 'hidden',
      ]);

      $this->add([
        'name' => 'redirect_uri',
        'type' => 'hidden',
      ]);

      $this->add([
        'name' => 'state',
        'type' => 'hidden',
      ]);

      $this->add([
        'name' => 'scope',
        'type' => 'hidden',
      ]);

      $this->add([
        'name' => 'email',
        'type' => 'Text',
        'options' => [
          'label' => 'E-mail',
        ],
      ]);
      $this->add([
        'name' => 'password',
        'type' => 'Password',
        'options' => [
          'label' => 'Password',
        ],
      ]);
      $this->add([
        'name' => 'submit',
        'type' => 'Submit',
        'attributes' => [
          'value' => 'Login',
          'id' => 'submitbutton',
        ],
      ]);
   }
}
