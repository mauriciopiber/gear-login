<?php
namespace Gear\Login\Admin\Login\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

class LoginFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new LoginForm();
        return $factory;
    }


    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new LoginForm();
      return $factory;
    }
}
