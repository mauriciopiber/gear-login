<?php
namespace Gear\Login\Admin\Login\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Gear\Login\Admin\Login\Controller\LoginController;
use Gear\Login\Service\AuthService;
use Gear\Login\Admin\Login\Service\LoginService;
use Gear\Login\Admin\Login\Form\LoginForm;
use Gear\Login\Admin\Login\Filter\LoginFilter;
use Interop\Container\ContainerInterface;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package Gear\Login/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class LoginControllerFactory implements FactoryInterface
{
    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        //$serviceLocator = $container->getServiceLocator();

        $factory = new LoginController(
          $container->get(LoginService::class),
          $container->get(LoginForm::class),
          $container->get(LoginFilter::class)
        );
        //
        return $factory;
    }
}
