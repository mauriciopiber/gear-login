<?php
namespace Gear\Login\Admin\Login\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Serializer\Adapter\Json as JsonAdapter;
use Gear\Login\Admin\Login\Form\LoginForm;

class LoginController extends AbstractRestfulController
{
    protected $jsonAdapter;

    protected $customerService;

    public function __construct($loginService, $loginForm, $loginFilter)
    {
      $this->loginService = $loginService;
      $this->loginForm = $loginForm;
      $this->loginFilter = $loginFilter;
      $this->jsonAdapter = new JsonAdapter();
    }

    public function loginAction()
    {
      $this->layout('layout');
      //se está logado, retorna para página principal
      if ($this->loginService->getIdentity()) {

        $oauthParams = [
          'response_type' =>  $this->params()->fromQuery('response_type', null),
          'client_id' => $this->params()->fromQuery('client_id', null),
          'redirect_uri' => $this->params()->fromQuery('redirect_uri', null),
          'state' => $this->params()->fromQuery('state', null),
          'scope' => $this->params()->fromQuery('scope', null)
        ];

        if ($oauthParams['response_type'] && $oauthParams['client_id']) {
          $url = http_build_query($oauthParams);

          $response = $this->redirect()->toUrl(
            '/oauth/authorize?'.$url,
            [
              'authorized' => 'yes'
            ]
          );
          return $response;
        }

        return $this->redirect()->toUrl(
          '/'
        );
      }
      //var_dump($this->loginService->getIdentity());
      $prg = $this->prg('/login', true);

      if ($prg instanceof \Zend\Http\PhpEnvironment\Response) {
        return $prg;
      }

      $form = $this->loginForm;

      if ($prg === false) {
        //$form->get('submit')->setValue('Add');
        $oauthParams = [
          'response_type' =>  $this->params()->fromQuery('response_type', null),
          'client_id' => $this->params()->fromQuery('client_id', null),
          'redirect_uri' => $this->params()->fromQuery('redirect_uri', null),
          'state' => $this->params()->fromQuery('state', null),
          'scope' => $this->params()->fromQuery('scope', null)
        ];

        $form->setData($oauthParams);
        $form->isValid();
        //var_dump($form->getData());die();

        //$form->setUseInputFilterDefaults(false);
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setVariables(array('form' => $form));
        //$viewModel->setTerminal(true);
        return $viewModel;
        //var_dump($oauthParams);
      }

      $form->setInputFilter($this->loginFilter->getInputFilter());
      $form->setData($prg);

      $isValid = $form->isValid();
      if ($isValid === false) {
        $values = $form->getData();
        if ($values) {
            $values['password'] = null;
        }
        $form->setData($values);
        $form->isValid();
        $viewModel = new \Zend\View\Model\ViewModel();
        $viewModel->setVariables(array('form' => $form));
        //$viewModel->setTerminal(true);
        return $viewModel;
      }

      $login = $this->loginService->login($form->getData());
      //var_dump($login);die();

      if (isset($login['error']) && $login['error'] === false) {
        unset($prg['email'], $prg['password'], $prg['submit']);

        if (!isset($prg['client_id']) || empty($prg['client_id'])) {
          return $this->redirect()->toUrl(
            '/'
          );
        }
        //$prg['user_id'] = $login;

        //$prg['authorized'] = 'yes';

        $url = http_build_query($prg);

        $response = $this->redirect()->toUrl(
          '/oauth/authorize?'.$url,
          [
            'authorized' => 'yes'
          ]
        );
        return $response;
        //var_dump($response);die();
      }

      $viewModel = new \Zend\View\Model\ViewModel();
      $viewModel->setVariables(array('form' => $form, 'error' => $login['message']));
      //$viewModel->setTerminal(true);
      return $viewModel;
    }

    public function homeAction()
    {
      //se está logado, retorna para página principal
      if (!$this->loginService->getIdentity()) {

        return $this->redirect()->toUrl(
          '/login'
        );
      }


      $oauthParams = [
        'response_type' =>  $this->params()->fromQuery('response_type', null),
        'client_id' => $this->params()->fromQuery('client_id', null),
        'redirect_uri' => $this->params()->fromQuery('redirect_uri', null),
        'state' => $this->params()->fromQuery('state', null),
        'scope' => $this->params()->fromQuery('scope', null)
      ];
//var_dump($oauthParams);
      if ($oauthParams['response_type'] && $oauthParams['client_id']) {
        $url = http_build_query($oauthParams);

        $response = $this->redirect()->toUrl(
          '/oauth/authorize?'.$url,
          [
            'authorized' => 'yes'
          ]
        );
        return $response;
      }

      $identity = $this->loginService->getIdentity();

      $role = isset($identity['roles'][0]) ? $identity['roles'][0] : 'user';

      switch ($role) {
        case 'master':
        case 'admin':
          $layout = 'admin';
          break;
        case 'user':
        default:
          $layout = 'client';
      }

      $viewModel = new \Zend\View\Model\ViewModel();
      $viewModel->setVariables([
        'email' => $identity['email'],
        'layout' => $layout,
      ]);
      //$viewModel->setTerminal(true);
      return $viewModel;
    }

    public function logoutAction()
    {
      try {
        $this->loginService->logout();
      } catch(\Exception $e) {

      }

      return $this->redirect()->toRoute('login');
    }
}
