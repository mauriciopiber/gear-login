<?php

namespace Gear\Login\Filter;

use Zend\InputFilter\InputFilter;
//use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class LoginFilter extends InputFilter implements InputFilterInterface
{
    public function getInputFilter()
    {
        $this->add(array(
          'name'       => 'email',
          'required'   => true,
        ));

        $this->add(array(
          'name'       => 'password',
          'required'   => true,

        ));
        return $this;
    }
}
