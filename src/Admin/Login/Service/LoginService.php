<?php
namespace Gear\Login\Admin\Login\Service;

use Gear\Login\Admin\Login\Filter\LoginFilter;

class LoginService {
  protected $authService;

  public function __construct($authService, LoginFilter $loginFilter)
  {
    $this->authService = $authService;
    $this->loginFilter = $loginFilter;
  }

  public function getIdentity()
  {
    return $this->authService->getIdentity();
  }

  public function login($data)
  {

    if ($this->authService->getIdentity() !== null) {
      throw new \Exception('Already logged in');
    }

    $email = $data['email'];
    $password = $data['password'];

    // Authenticate with login/password.
    $authAdapter = $this->authService->getAdapter();
    $authAdapter->setEmail($email);
    $authAdapter->setPassword($password);
    $result = $this->authService->authenticate();
    //var_dump($result);

    if ($result->getCode() === 1) {
      return [
        'error' => false,
        'identity' => $result->getIdentity()
      ];
      //return $result->getIdentity();
    }

    return [
      'error' => true,
      'message' => implode($result->getMessages(), ' ')
    ];
    //var_dump($result);

    //return false;
  }

  public function logout()
  {
    // Allow to log out only when user is logged in.
    if ($this->authService->getIdentity()==null) {
        throw new \Exception('The user is not logged in');
    }

    // Remove identity from session.
    $this->authService->clearIdentity();
  }
}
