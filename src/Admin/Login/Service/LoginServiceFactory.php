<?php
namespace Gear\Login\Admin\Login\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Gear\Login\Admin\Login\Filter\LoginFilter;

class LoginServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new LoginService(
          $serviceLocator->get(AuthenticationService::class),
          $serviceLocator->get(LoginFilter::class)
        );
        return $factory;
    }


    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $service = $container->get(AuthenticationService::class);
      //die();
      $factory = new LoginService(
        $service,
        $container->get(LoginFilter::class)
      );
      return $factory;
    }
}
