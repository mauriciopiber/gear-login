<?php
namespace Gear\Login\Admin\Login\Filter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

class LoginFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new LoginFilter(
          $serviceLocator->get('Zend\Db\Adapter\Adapter')
        );
        return $factory;
    }


    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new LoginFilter(
        $container->get('Zend\Db\Adapter\Adapter')
      );
      return $factory;
    }
}
