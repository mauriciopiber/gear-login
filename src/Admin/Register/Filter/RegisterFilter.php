<?php
namespace Gear\Login\Admin\Register\Filter;

use Zend\InputFilter\InputFilter;

class RegisterFilter extends InputFilter
{
    public function __construct($dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function getInputFilter()
    {
        // $this->add([
        //   'name' => 'first_name',
        //   'required' => true,
        // ]);
        //
        // $this->add([
        //   'name' => 'last_name',
        //   'required' => true,
        // ]);

        $this->add(array(
            'name'       => 'email',
            'required'   => true,
            'validators' => array(
                $this->getNotEmptyValidator('E-mail'),
                $this->getEmailAddressValidator('E-mail'),
                $this->getRecordNoExistsValidator('oauth_users', 'email', 'E-mail')
            )
        ));

        $this->add(array(
            'name'       => 'password',
            'required'   => true,
            'validators' => array(
                $this->getNotEmptyValidator('Password'),
                $this->getStringLengthValidator('Password', 6, 20)
            ),
            'filters'   => array(
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(
            array(
                'name' => 'confirmPassword',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    $this->getNotEmptyValidator('Confirm Password'),
                    $this->getStringLengthValidator('Confirm Password', 6, 20),
                    $this->getIdenticalValidator('Confirm Password', 'password', 'Password')
                ),
            )
        );
        return $this;
    }


        public function getIdenticalValidator($entity, $compareField, $compareEntity)
        {
            $message = sprintf(
                // $this->getTranslate()->translate("%s and %s must be equals."),
                // $this->getTranslate()->translate($compareEntity),
                // $this->getTranslate()->translate($entity)
                "%s and %s must be equals.",
                $compareEntity,
                $entity
            );

            return array(
                'name'    => 'Identical',
                'options' => array(
                    'token' => $compareField,
                    'messages' => array(
                        \Zend\Validator\Identical::NOT_SAME      => $message,
                        \Zend\Validator\Identical::MISSING_TOKEN => $message,
                    )
                ),
                'break_chain_on_failure' => true
            );
        }

    public function getEmailAddressValidator($entity)
    {
        $message = sprintf(
            '%s filled is not a valid e-mail address, please inform a valid in the field.',
            $entity
        );

        return array(
            'name' => 'EmailAddress',
            'options' => array(
                'messages' => array(
                    \Zend\Validator\EmailAddress::INVALID            => $message,
                    \Zend\Validator\EmailAddress::INVALID_FORMAT     => $message,
                    \Zend\Validator\EmailAddress::INVALID_HOSTNAME   => $message,
                    \Zend\Validator\EmailAddress::INVALID_MX_RECORD  => $message,
                    \Zend\Validator\EmailAddress::INVALID_SEGMENT    => $message,
                    \Zend\Validator\EmailAddress::DOT_ATOM           => $message,
                    \Zend\Validator\EmailAddress::QUOTED_STRING      => $message,
                    \Zend\Validator\EmailAddress::INVALID_LOCAL_PART => $message,
                    \Zend\Validator\EmailAddress::LENGTH_EXCEEDED    => $message,
                ),
            ),
            'break_chain_on_failure' => true
        );
    }

    public function getStringLengthValidator($entity, $min = null, $max = null)
    {

        $options = [];
        $messages = [];
        if ($min != null && is_numeric($min) && $min > 0) {
            $options['min'] = $min;
            $messages[\Zend\Validator\StringLength::TOO_SHORT] =
                sprintf('%s', $entity)
                .' '
                .'should be at least %min% characters.';
        }

        if ($max != null && is_numeric($max) && $max > 0) {
            $options['max'] = $max;
            $messages[\Zend\Validator\StringLength::TOO_LONG] =
                sprintf('%s', $entity)
                .' '
                .'should be maximum %max% characters.';
        }

        return array(
            'name'    => 'StringLength',
            'options' => array_merge(
                $options,
                array(
                    'encoding' => 'UTF-8',
                    'messages' => array_merge(
                        $messages,
                        array(
                            \Zend\Validator\StringLength::INVALID   => sprintf('%s invalid.', $entity),
                        )
                    )
                )
            ),
        );
    }

    public function getNotEmptyValidator($entity)
    {
        return array(
            'name' => 'NotEmpty',
            'options' => array(
                'messages' => array(
                    \Zend\Validator\NotEmpty::IS_EMPTY =>
                    sprintf(
                        '%s is required, please inform in the field.',
                        $entity
                    )
                ),
            ),
            'break_chain_on_failure' => true
        );
    }

    public function getRecordNoExistsValidator($table, $field, $label)
    {

        $recordExists =   new \Zend\Validator\Db\NoRecordExists(
            [
                'table' => $table,
                'field' => $field,
                'adapter' => $this->dbAdapter,
                'options' => [
                    'translator' => $this->dbAdapter,
                ]
            ]
        );

        $recordExists->setMessages([
            \Zend\Validator\Db\RecordExists::ERROR_RECORD_FOUND =>
            sprintf(
                '%s already registered, please choose another',
                $label
            )
        ]);

        return $recordExists;
    }
}
