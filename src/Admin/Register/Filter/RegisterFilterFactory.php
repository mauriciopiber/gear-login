<?php
namespace Gear\Login\Admin\Register\Filter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;

class RegisterFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new RegisterFilter(
          $serviceLocator->get('Zend\Db\Adapter\Adapter')
        );
        return $factory;
    }


    /**
     * Create RegisterController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RegisterController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new RegisterFilter(
        $container->get('Zend\Db\Adapter\Adapter')
      );
      return $factory;
    }
}
