<?php
namespace Gear\Login\Admin\Register\Service;

use Gear\Login\User\Service\UserService;
use GearEmail\Service\EmailService;
use Gear\Login\UserRole\Service\UserRoleService;
use Gear\Login\Role\Service\RoleService;
use Exception;

class RegisterService {

  protected $userService;

  protected $emailService;

  const DEFAULT_ROLE = 'member';

  public function __construct(
    UserService $userService,
    EmailService $emailService,
    UserRoleService $userRoleService,
    RoleService $roleService
  ) {
    $this->userService = $userService;
    $this->emailService = $emailService;
    $this->userRoleService = $userRoleService;
    $this->roleService = $roleService;
  }

  public function getIdentity()
  {
    return $this->authService->getIdentity();
  }

  public function register($data)
  {
      //$data['state'] = 0;
      unset($data['submit']);

      $user = $this->userService->create($data);

      $email = $data['email'];


      $role = $this->roleService->findOneByRole(self::DEFAULT_ROLE);

      if (!isset($role['role_id']) || (bool) $role['role_id'] !== true) {
          throw new Exception('Role not found');
      }
      $roleId = $role['role_id'];

      $this->userRoleService->create([
          'user_id' => $user['id'],
          'role_id' => $roleId
      ]);

      //$this->sendActivationEmail($email);
      return true;
  }

  public function sendActivationEmail($email)
  {
      $subject = 'Ativar conta Pbr';
      $body = 'Obrigado por se cadastrar na Pbr Piber.Network';
      $this->emailService->forward($email, $subject, $body);
      return true;
  }
}
