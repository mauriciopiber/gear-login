<?php
namespace Gear\Login\Admin\Register\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Gear\Login\User\Service\UserService;
use GearEmail\Service\EmailService;
use Gear\Login\UserRole\Service\UserRoleService;
use Gear\Login\Role\Service\RoleService;

class RegisterServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $container)
    {
        $factory = new RegisterService(
          $serviceLocator->get(UserService::class),
          $serviceLocator->get(EmailService::class),
          $serviceLocator->get(UserRoleService::class),
          $serviceLocator->get(RoleService::class)
        );
        return $factory;
    }


    /**
     * Create RegisterController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RegisterController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new RegisterService(
        $container->get(UserService::class),
        $container->get(EmailService::class),
        $container->get(UserRoleService::class),
        $container->get(RoleService::class)
      );
      return $factory;
    }
}
