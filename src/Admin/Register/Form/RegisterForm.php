<?php
namespace Gear\Login\Admin\Register\Form;

 use Zend\Form\Form;

class RegisterForm extends Form
{
   public function __construct($name = null)
   {
      // we want to ignore the name passed
      parent::__construct('register');

      $this->add([
        'name' => 'first_name',
        'type' => 'Text',
        'options' => [
          'label' => 'First Name',
        ],
      ]);

      $this->add([
        'name' => 'last_name',
        'type' => 'Text',
        'options' => [
          'label' => 'Last Name',
        ],
      ]);

      $this->add([
        'name' => 'username',
        'type' => 'Text',
        'options' => [
          'label' => 'Username',
        ],
      ]);

      $this->add([
        'name' => 'email',
        'type' => 'Text',
        'options' => [
          'label' => 'E-mail',
        ],
      ]);
      $this->add([
        'name' => 'password',
        'type' => 'Password',
        'options' => [
          'label' => 'Password',
        ],
      ]);

      $this->add([
        'name' => 'confirmPassword',
        'type' => 'Password',
        'options' => [
          'label' => 'Confirm Password',
        ],
      ]);

      $this->add([
        'name' => 'submit',
        'type' => 'Submit',
        'attributes' => [
          'value' => 'Register',
          'id' => 'submitbutton',
        ],
      ]);
   }
}
