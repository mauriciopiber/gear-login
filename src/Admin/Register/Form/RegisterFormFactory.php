<?php
namespace Gear\Login\Admin\Register\Form;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

class RegisterFormFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new RegisterForm();
        return $factory;
    }


    /**
     * Create RegisterController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RegisterController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new RegisterForm();
      return $factory;
    }
}
