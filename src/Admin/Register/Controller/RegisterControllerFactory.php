<?php
namespace Gear\Login\Admin\Register\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Gear\Login\Admin\Register\Controller\RegisterController;
use Gear\Login\Auth\AuthService;
use Zend\Authentication\AuthenticationService;
use Gear\Login\Admin\Register\Service\RegisterService;
use Gear\Login\Admin\Register\Form\RegisterForm;
use Gear\Login\Admin\Register\Filter\RegisterFilter;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package Gear\Login/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class RegisterControllerFactory implements FactoryInterface
{
    /**
     * Create RegisterController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RegisterController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        $factory = new RegisterController(
          $container->get(AuthenticationService::class),
          $container->get(RegisterService::class),
          $container->get(RegisterForm::class),
          $container->get(RegisterFilter::class)
        );

        return $factory;
    }
}
