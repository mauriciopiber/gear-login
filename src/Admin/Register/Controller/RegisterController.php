<?php
namespace Gear\Login\Admin\Register\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Serializer\Adapter\Json as JsonAdapter;
use OAuth2\Request as OAuth2Request;
use OAuth2\Response as OAuth2Response;
use OAuth2\Server as OAuth2Server;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Http\PhpEnvironment\Response;
use Gear\Login\Admin\Register\Service\RegisterService;
use Gear\Login\Admin\Register\Form\RegisterForm;
use Gear\Login\Admin\Register\Filter\RegisterFilter;

class RegisterController extends AbstractActionController
{
    public function __construct(
      AuthenticationService $authService,
      RegisterService $service,
      RegisterForm $form,
      RegisterFilter $filter
    ) {
        $this->registerService = $service;
        $this->registerForm = $form;
        $this->registerFilter = $filter;
        $this->authService = $authService;
    }

    public function registerAction()
    {
      $this->layout('layout');


      if ($this->authService->getIdentity()) {
        return $this->redirect()->toRoute(
          '/home'
        );
      }

      $prg = $this->prg('/register', true);

      if ($prg instanceof Response) {
        return $prg;
      }

      $form = $this->registerForm;

      if ($prg === false) {

        $form->setData([]);
        $form->isValid();
        $viewModel = new ViewModel();
        $viewModel->setVariables(array('form' => $form));
        //$viewModel->setTerminal(true);
        return $viewModel;
      }

      $form->setInputFilter($this->registerFilter->getInputFilter());
      //$form->setData($this->getRequest()->getPost());
      $form->setData($prg);

      $isValid = $form->isValid();

      if ($isValid === false) {

        $viewModel = new ViewModel();
        $viewModel->setVariables(array('form' => $form));
        //$viewModel->setTerminal(true);
        return $viewModel;
      }
      /**
       * @TODO tratar erro
       */
      try {
        $register = $this->registerService->register($form->getData());
      } catch (\Exception $e) {
        var_dump($e);

      }


      return $this->redirect()->toRoute(
        'registration-complete'
      );
    }

    public function registrationCompleteAction() {

    }
}
