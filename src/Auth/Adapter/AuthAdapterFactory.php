<?php
namespace Gear\Login\Auth\Adapter;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Gear\Login\User\Service\UserService;

class AuthAdapterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new AuthAdapter(
          $serviceLocator->get(UserService::class)
        );
        return $factory;
    }


    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new AuthAdapter(
        $container->get(UserService::class)
      );
      return $factory;
    }
}
