<?php
namespace Gear\Login\Auth\Controller;

use Zend\ServiceManager\Factory\FactoryInterface;
use Gear\Login\Auth\Controller\AuthController;
use Zend\Authentication\AuthenticationService;
use Gear\Login\User\Service\UserService;

use Interop\Container\ContainerInterface;

/**
 * PHP Version 5
 *
 * @category Factory
 * @package Gear\Login/Controller
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class AuthControllerFactory implements FactoryInterface
{
    /**
     * Create AuthController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return AuthController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
        //$serviceLocator = $container->getServiceLocator();

        $factory = new AuthController(
          $container->get(AuthenticationService::class),
          $container->get(UserService::class)
        );
        //
        return $factory;
    }
}
