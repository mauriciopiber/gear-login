<?php
namespace Gear\Login\Auth\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Serializer\Adapter\Json as JsonAdapter;
use Zend\View\Model\JsonModel;
use Gear\Login\User\Service\UserService;

/**
 * @OA\Info(title="My First API", version="0.1")
 */

class AuthController extends AbstractRestfulController
{
    protected $jsonAdapter;

    public function __construct($authService, UserService $userService)
    {
      $this->authService = $authService;
      $this->userService = $userService;
    }

    public function getList()
    {
      return new JsonModel(['payload' => $this->authService->getIdentity()]);
    }

    public function update($id, $data)
    {
        $auth = $this->authService->getIdentity();

        if ((int)$auth['username'] !== (int)$id) {
            return new JsonModel([
                'error' => true,
                'messages' => ['Can\'t edit other user']
            ]);
        }

        //$data['']

        $update = $this->userService->update($id, $data);
        return new JsonModel($update);
    }

    public function changePasswordAction()
    {
        // $identity = $this->authService->getIdentity();
        //
        // $user = $this->userService->findOneByemail($identity['email']);
        //
        // // Now we need to calculate hash based on user-entered password and compare
        // // it with the password hash stored in database.
        // $bcrypt = new Bcrypt();
        // $bcrypt->setCost(14);
        // $passwordHash = $user['password'];
        //
        // if (!$bcrypt->verify($this->password, $passwordHash)) {
        //     return (new Response(null, ['Wrong password']))->parse();
        // }
        // var_dump($user);die();

        $identity = $this->authService->getIdentity();

        $password = $this->params()->fromPost('oldPassword');
        $newPassword = $this->params()->fromPost('newPassword');
        $confirmPassword = $this->params()->fromPost('confirmPassword');

        $changePassword = $this->userService->changePassword(
            $identity,
            $password,
            $newPassword,
            $confirmPassword
        );

        return new JsonModel($changePassword);
    }
}
