<?php
namespace Gear\Login\Auth\Acl;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Exception;

class AclAuth {
  const DEFAULT_PUBLIC = 'guest';

  public $acl = null;
  public $roles = null;
  public $rules = null;
  public $resources = null;

  public function __construct($authorization) {
    $this->authorization = $authorization;
  }

  public function getRole() {
    if (!$this->authorization->hasIdentity()) {
      return self::DEFAULT_PUBLIC;
    }

    $identity = $this->authorization->getIdentity();

    $roles = $identity['roles'];
    if (isset($roles[0])) {
      return $roles[0];
    }
    throw new Exception('Missing role for user');
  }

  public function isAllowed($resource) {
    $role = $this->getRole();
    return $this->isValid($role, $resource);
  }

  public function setRoles($roles) {
    $this->roles = $roles;
  }

  public function setRules($rules) {
    $this->rules = $rules;
  }

  public function setResources($resources) {
    $this->resources = $resources;
  }

  private function getRoles() {
    if ($this->roles) {
      foreach ($this->roles as $i => $role) {
        $this->acl->addRole(new Role($role['role']), isset($role['parent']) ? $role['parent'] : null);
      }
      return;
    }
  }

  private function getResource() {
    if ($this->resources) {
      foreach ($this->resources as $i => $resource) {
        $this->acl->addResource(new Resource($resource));
      }
      return;
    }
  }

  private function getRules() {
    if ($this->rules) {
      foreach ($this->rules as $i => $rule) {
        $this->acl->allow($rule['role'], $rule['resource']);
      }
    }
  }

  public function isValid($role, $rule) {
    if ($this->acl === null) {
      $this->acl = new Acl();
      $this->getRoles();
      $this->getResource();
      $this->getRules();
    }
    return $this->acl->isAllowed($role, $rule);
  }
}
