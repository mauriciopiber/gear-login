<?php
namespace Gear\Login\Auth\Acl;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Gear\Login\Auth\Acl\AclAuth;

class AclGuardFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $factory = new AclGuard(
          $serviceLocator->get(AclAuth::class)
        );
        return $factory;
    }


    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new AclGuard(
        $container->get(AclAuth::class)
      );
      return $factory;
    }
}
