<?php
namespace Gear\Login\Auth\Acl;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;

class AclAuthFactory implements FactoryInterface
{
    /**
     * Create LoginController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return LoginController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new AclAuth(
        $container->get(AuthenticationService::class)
      );

      $roles = require __DIR__.'/../../../config/rule/roles.php';
      $resources = require __DIR__.'/../../../config/rule/resources.php';
      $rules = require __DIR__.'/../../../config/rule/rules.php';

      $factory->setRoles($roles);
      $factory->setResources($resources);
      $factory->setRules($rules);

      return $factory;
    }
}
