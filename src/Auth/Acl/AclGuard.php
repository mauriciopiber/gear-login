<?php
namespace Gear\Login\Auth\Acl;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Gear\Login\Auth\Exception\UnAuthorizedException;

class AclGuard
{
    public function __construct($aclAuth)
    {
      $this->aclAuth = $aclAuth;
    }
    /**
     * {@inheritDoc}
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onRoute'), -1000);
    }

    public function isApiMethod($routeMatch) {

      return substr($routeMatch, 0, 3) === 'api';
    }

    public function apiRoute(MvcEvent $event)
    {
        $request = $event->getApplication()->getRequest();

        $method = $request->getMethod();

        if ($method !== 'GET') {
          return $method;
        }

        $id = $request->getQuery()->get('id');

        if ($id) {
          return $method;
        }

        return 'LIST';
        //var_dump($method);
        //return 'get';


        //$id = $this->getIdentificator();

    }

    /**
     * Event callback to be triggered on dispatch, causes application error triggering
     * in case of failed authorization check
     *
     * @param MvcEvent $event
     *
     * @return void
     */
    public function onRoute(MvcEvent $event)
    {
      $match      = $event->getRouteMatch();
      $routeName  = $match->getMatchedRouteName();

      $isApi = $this->isApiMethod($routeName);

      if ($isApi) {
        $routeName = $routeName . ':' . $this->apiRoute($event);
      }

      $isAllowed = $this->aclAuth->isAllowed($routeName);

      if ($isAllowed) {
        return;
      }

      $event->setError($isApi ? 'unauthorized-api' : 'unauthorized-action');
      $event->setParam('route', $routeName);
      //$event->setParam('identity', $this->aclAuth->getIdentity);
      $event->setParam('exception', new UnAuthorizedException('You are not authorized to access ' . $routeName));
      /* @var $app \Zend\Mvc\Application */
      $app = $event->getTarget();
      $eventManager = $app->getEventManager();
      $eventManager->setEventPrototype($event);
      $results = $eventManager->trigger(
          MvcEvent::EVENT_DISPATCH_ERROR,
          null,
          $event->getParams()
      );
      $return  = $results->last();
      if (! $return) {
          return $event->getResult();
      }
      return $return;
    }
}
