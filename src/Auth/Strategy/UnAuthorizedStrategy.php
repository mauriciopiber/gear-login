<?php
/**
 * BjyAuthorize Module (https://github.com/bjyoungblood/BjyAuthorize)
 *
 * @link https://github.com/bjyoungblood/BjyAuthorize for the canonical source repository
 * @license http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Gear\Login\Auth\Strategy;
use Gear\Login\Auth\Exception\UnAuthorizedException;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Response as HttpResponse;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\ResponseInterface as Response;
use Zend\View\Model\ViewModel;
use ZF\ApiProblem\ApiProblem;
use ZF\ApiProblem\ApiProblemResponse;
use ZF\ApiProblem\Exception\ProblemExceptionInterface;
/**
 * Dispatch error handler, catches exceptions related with authorization and
 * configures the application response accordingly.
 *
 * @author Ben Youngblood <bx.youngblood@gmail.com>
 */
class UnAuthorizedStrategy implements ListenerAggregateInterface
{
    /**
     * @var string
     */
    protected $template = 'error/403';
    /**
     * @var callable[] An array with callback functions or methods.
     */
    protected $listeners = array();

    /**
     * {@inheritDoc}
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), -5000);
    }
    /**
     * {@inheritDoc}
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    /**
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = (string)$template;
    }
    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }


    /**
     * Callback used when a dispatch error occurs. Modifies the
     * response object with an according error if the application
     * event contains an exception related with authorization.
     *
     * @param MvcEvent $event
     *
     * @return void
     */
    public function onDispatchError(MvcEvent $event)
    {
        $auth = $event->getApplication()->getServiceManager()->get(
          \Zend\Authentication\AuthenticationService::class
        );

        //var_dump($auth);die();
      //var_dump($event->getApplication()->getServiceManager());die();
        //$app = $event->getTarget();


        // Do nothing if the result is a response object
        $result = $event->getResult();
        $response = $event->getResponse();
        if ($result instanceof Response || ($response && !$response instanceof HttpResponse)) {
            return;
        }
        // Common view variables
        $viewVariables = array(
            'error' => $event->getParam('error'),
            'identity' => $event->getParam('identity'),
        );

        switch ($event->getError()) {
          case 'unauthorized-action':
          case 'unauthorized-api':
            $viewVariables['route'] = $event->getParam('route');
            break;

          default:
            return;
        }


        if ($event->getError() === 'unauthorized-action') {
            $model = new ViewModel($viewVariables);
            $response = $response ?: new HttpResponse();
            $model->setTemplate($this->getTemplate());
            $event->getViewModel()->addChild($model);
            $response->setStatusCode(403);
            $event->setResponse($response);
            return;
        }

        $apiError = new ApiProblemResponse(
            new ApiProblem(
                '403',
                'You must use HTTP Login to use this resource'
            )
        );

        return $apiError;
    }
}
