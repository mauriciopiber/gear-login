<?php
namespace Gear\Login\Role\Service;

use Gear\Rest\Service\AbstractRestService;

/**
 * PHP Version 5
 *
 * @category Service
 * @package Gear\Login/NutritionalMeal/Service
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class RoleService extends AbstractRestService
{
    public function findOneByRole($role)
    {
        return $this->repository->findOneByRole($role);
    }



}
