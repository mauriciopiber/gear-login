<?php
namespace Gear\Login\Role\Filter;

use Zend\InputFilter\InputFilter;
use Gear\Rest\Filter\RestFilterInterface;
use Zend\Validator\Db\RecordExists;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package Gear\Login//Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class RoleFilter extends InputFilter implements RestFilterInterface
{
    public function __construct($dbAdapter) {
        $this->dbAdapter = $dbAdapter;
        return $this;
    }
    /**
     * Create the Filter to Form Elements
     *
     * @return Filter
     *
     */
    public function getInputFilter()
    {
        $this->add([
            'name'       => 'role_id',
            'required'   => false,
        ]);

        $this->add([
            'name'       => 'role',
            'required'   => true,
        ]);

        $parentValidator = new RecordExists(
            [
                'table'   => 'acl_role',
                'field'   => 'role_id',
                'adapter' => $this->dbAdapter,
            ]
        );

        $this->add([
            'name'       => 'parent_id',
            'required'   => false,
            'validators' => [
                $parentValidator
            ]
        ]);

        return $this;
    }
}
