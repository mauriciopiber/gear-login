<?php
namespace Gear\Login\Role\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/*
SELECT     r.role_id   AS role_id,
           r.role      AS role,
           r.parent_id AS parent_id,
           rr.role     AS parent_role
FROM       acl_role      AS r
INNER JOIN acl_role      AS rr
ON         rr.role_id = r.role_id
ORDER BY   role_id DESC
LIMIT      10
offset     0

*/
/*
SELECT
  `r`.`role_id` AS `role_id`,
  `r`.`role` AS `role`,
  `r`.`parent_id` AS `parent_id`,
  `rr`.`role` AS `parent_role`
  FROM `acl_role` AS `r`
  INNER JOIN `acl_role` AS `rr` ON `rr`.`role_id` = `r`.`role_id`
  ORDER BY `r`.`role_id` DESC
  LIMIT 10
  OFFSET 0

*/
/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class RoleRepository extends AbstractRestRepository
{
    const TABLE = ['r' => 'acl_role'];
    const ID = 'role_id';

    const COLUMNS = ['role_id', 'role', 'parent_id'];//@TODO is default

    const JOIN = [
        [
            'table' => ['rr' => 'acl_role'],
            'link' => 'rr.role_id = r.parent_id',
            'columns' => ['parent_role' => 'role'],
            'type' => 'left'
        ]
    ];

    const FILTER = [
        'role_id' => 'r.role_id',
        'parent_id' => 'r.parent_id',
        'role' => 'r.role like ?',
        'parent_role' => 'rr.role'
    ];

    public function findOneByRole($role)
    {
        $select = $this->getSql()->select();
        $select->from(static::TABLE);

        $select = $this->selectColumn($select);
        $select = $this->selectJoin($select);

        $select->where(['role' => $role]);

        $results = $this->execute($select);
        $row = $results->current();
        $row[static::ID] = (int) $row[static::ID];
        return $row;


    }
}
