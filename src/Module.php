<?php
namespace Gear\Login;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;
use Zend\Validator\AbstractValidator;
use Zend\View\ViewEvent;
use Zend\View\Renderer\PhpRenderer;
use Zend\Http\Request as HttpRequest;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ModelInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Gear\Login\Auth\Acl\AclGuard;
use Gear\Login\Auth\Strategy\UnAuthorizedStrategy;

/**
 * PHP Version 5
 *
 * @category Module
 * @package Gear\Login
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class Module
{
    const LOCATION = __DIR__;

    /**
     * Config executed on Bootstrap
     *
     * @param MvcEvent $mvcEvent Mvc Event
     *
     * @return void
     */
    public function onBootstrap(MvcEvent $mvcEvent)
    {
        // attach the JSON view strategy
        $app      = $mvcEvent->getTarget();
        $locator  = $app->getServiceManager();
        // 
        // $aclGuard = $locator->get(AclGuard::class);
        // $aclGuard->attach($app->getEventManager());
        //
        // $aclStrategy = $locator->get(UnauthorizedStrategy::class);
        // $aclStrategy->attach($app->getEventManager());
        //
        // $view     = $locator->get('Zend\View\View');
        // $strategy = $locator->get('ViewJsonStrategy');
        // $strategy->attach($app->getEventManager());
        //$view->getEventManager()->attach($strategy, 100);

        //
        $headers = $mvcEvent->getResponse()->getHeaders();
        $headers->addHeaderLine('Access-Control-Allow-Origin', '*');
        $headers->addHeaderLine('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $headers->addHeaderLine('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Authorization');
    }



    public function shouldRender($event)
    {
        //var_dump($event->isError());
        //var_dump($event->propagationIsStopped());
        if (!$event->isError()) {
            return false;
        }

        // Check the accept headers for application/json
        $request = $event->getRequest();
        //var_dump($request instanceof HttpRequest);
        if (!$request instanceof HttpRequest) {
            return false;
        }

        $headers = $request->getHeaders();
        //var_dump($headers->has('Accept'));
        if (!$headers->has('Accept')) {
            return false;
        }

        // if we have a JsonModel in the result, then do nothing
        $currentModel = $event->getResult();
        //var_dump($currentModel instanceof JsonModel);
        if ($currentModel instanceof JsonModel) {
            return false;
        }

        return true;
    }

    public function toJson($event)
    {
      if (in_array($event->getResponse()->getStatusCode(), [200, 201])) {
        return true;
        //var_dump('trata erro');
      }

      $currentModel = $event->getResult();
      //var_dump($currentModel);
      $response = $event->getResponse();
      $model = new JsonModel([
          "httpStatus" => $response->getStatusCode(),
          "title" => $response->getReasonPhrase(),
      ]);

      // Find out what the error is
      $exception  = $currentModel->getVariable('exception');

      if ($currentModel instanceof ModelInterface && $currentModel->reason) {
          $model->detail = $this->exceptionHandler($currentModel);
      }



      if ($exception) {
          if ($exception->getCode()) {
              $event->getResponse()->setStatusCode($exception->getCode());
          }
          $model->detail = $exception->getMessage();

          // find the previous exceptions
          $messages = array();
          while ($exception = $exception->getPrevious()) {
              $messages[] = "* " . $exception->getMessage();
          };
          if (count($messages)) {
              $exceptionString = implode("n", $messages);
              $model->messages = $exceptionString;
          }
      }

      // set our new view model
      $model->setTerminal(true);
      $event->setResult($model);
      $event->setViewModel($model);
    }

    public function onRenderError($event)
    {

        if (false === $this->shouldRender($event)) {
          //var_dump('will not render error');
          return;
        }
        //die('ak');
        $currentModel = $event->getResult();
        //var_dump($currentModel);
        $response = $event->getResponse();
        $model = new JsonModel([
            "httpStatus" => $response->getStatusCode(),
            "title" => $response->getReasonPhrase(),
        ]);

        // Find out what the error is
        $exception  = $currentModel->getVariable('exception');

        if ($currentModel instanceof ModelInterface && $currentModel->reason) {
            $model->detail = $this->exceptionHandler($currentModel);
        }



        if ($exception) {
            if ($exception->getCode()) {
              //var_dump($exception->getCode());
                $event->getResponse()->setStatusCode($exception->getCode());
            }
            $model->detail = $exception->getMessage();

            // find the previous exceptions
            $messages = array();
            while ($exception = $exception->getPrevious()) {
                $messages[] = "* " . $exception->getMessage();
            };
            if (count($messages)) {
                $exceptionString = implode("n", $messages);
                $model->messages = $exceptionString;
            }
        }

        // set our new view model
        $model->setTerminal(true);
        $event->setResult($model);
        $event->setViewModel($model);
    }

    public function exceptionHandler(ModelInterface $model)
    {
        switch ($model->reason) {
            case 'error-controller-cannot-dispatch':
                return 'The requested controller was unable to dispatch the request.';
            case 'error-controller-not-found':
                return 'The requested controller could not be mapped to an existing controller class.';
            case 'error-controller-invalid':
                return 'The requested controller was not dispatchable.';
            case 'error-router-no-match':
                return 'The requested URL could not be matched by routing.';
            default:
                return $model->message;
                break;
        }
    }

    /**
     * Return default autoloader config
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/../src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    /**
     * Return module configurations
     *
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * Create Log Errors for exceptions.
     *
     * @codeCoverageIgnore
     *
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => [
                'Zend\Log\Logger' => function () {
                    $logger = new Logger;
                    $writer = new Stream(
                        __DIR__.'/../../../data/logs/'.date('Y-m-d-H-i').'-error.txt'
                    );
                    $logger->addWriter($writer);
                    return $logger;
                },
            ],
        );
    }

    /**
     * Return Class Location on filesystem.
     *
     * @return string
     */
    public function getLocation()
    {
        return __DIR__;
    }
}
