<?php
namespace Gear\Login\UserRequest\Filter;

use Zend\InputFilter\InputFilter;
use Gear\Rest\Filter\RestFilterInterface;
use Zend\Validator\Db\RecordExists;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package Gear\Login//Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRequestFilter extends InputFilter implements RestFilterInterface
{
    public function __construct($dbAdapter) {
        $this->dbAdapter = $dbAdapter;
        return $this;
    }
    /**
     * Create the Filter to Form Elements
     *
     * @return Filter
     *
     */
    public function getInputFilter()
    {
      $this->add(array(
        'name'       => 'user_request_id',
        'required'   => false,
      ));
      $this->add(array(
        'name'       => 'user_id',
        'required'   => true,
      ));
      $this->add(array(
        'name'       => 'user_request_type_id',
        'required'   => true,
      ));
      $this->add(array(
        'name'       => 'created',
        'required'   => true,
      ));
      $this->add(array(
        'name'       => 'expires',
        'required'   => true,
      ));
      $this->add(array(
        'name'       => 'uid',
        'required'   => true,
      ));


        return $this;
    }
}
