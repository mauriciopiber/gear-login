<?php
namespace Gear\Login\UserRequest\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRequestRepository extends AbstractRestRepository
{
    const TABLE = ['r' => 'user_request'];
    const ID = 'user_request_id';

    const COLUMNS = [
        'user_request_id',
        'user_id',
        'user_request_type_id',
        'uid',
        'created',
        'expires'
    ];

    const JOIN = [];

    const FILTER = [
        'user_request_id' => 'user_request_id',
    ];
}
