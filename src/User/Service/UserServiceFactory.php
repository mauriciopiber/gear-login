<?php
namespace Gear\Login\User\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Gear\Login\User\Service\UserService;
use Gear\Login\UserRole\Service\UserRoleService;
use Gear\Login\User\Repository\UserRepository;
use Gear\Login\User\Filter\UserFilter;

class UserServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $container)
    {
        $factory = new UserService(
            $container->get(UserFilter::class),
            $container->get(UserRepository::class),
            $container->get(UserRoleService::class)
        );
        return $factory;
    }


    /**
     * Create RegisterController
     *
     * @param ServiceLocatorInterface $controllerManager Controller Manager
     *
     * @return RegisterController
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = [])
    {
      $factory = new UserService(
          $container->get(UserFilter::class),
          $container->get(UserRepository::class),
          $container->get(UserRoleService::class)
      );
      return $factory;
    }
}
