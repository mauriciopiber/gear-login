<?php
namespace Gear\Login\User\Service;

use Gear\Rest\Service\AbstractRestService;
use Gear\Rest\Response\Response;
use Zend\Crypt\Password\Bcrypt;
use Gear\Login\UserRole\Service\UserRoleService;
use Gear\Login\User\Repository\UserRepository;
use Gear\Login\User\Filter\UserFilter;

/**
 * PHP Version 5
 *
 * @category Service
 * @package Gear\Login/User/Service
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserService extends AbstractRestService
{
    protected $bcrypt;

    public function __construct(
        UserFilter $userFilter,
        UserRepository $userRepository,
        UserRoleService $userRoleService
    ) {
        $this->filter = $userFilter;
        $this->repository = $userRepository;
        $this->userRole = $userRoleService;
    }

    public function selectByid($id)
    {
        $user = parent::selectById($id);

        if (isset($user['result']) && isset($user['result']['password'])) {
            unset($user['result']['password']);
        }

        return $user;
    }


    public function update($id, $data)
    {
        $result = $this->repository->selectById($id);
        if (!$result) {
          return [
            'error' => true,
            'messages' => ['Entity' => 'Not found']
          ];
        }

        $useEmailValidator = ($result['email'] !== $data['email']);

        $filter = $this->filter->getInputFilter($useEmailValidator);
        $filter->remove('password');
        $filter->remove('confirmPassword');

        //var_dump($filter->get('email')->getValidatorChain());die();

        $filter->setData($data);

        $isValid = $filter->isValid();
        //var_dump($)

        //var_dump($isValid);

        if ($isValid === false) {
          return [
            'error' => true,
            'messages' => $filter->getMessages()
          ];
        }

        $result = $this->repository->update($id, $filter->getValues());
        if ($result) {
          return [
            'error' => false,
            'id' => $result
          ];
        }
        return [
          'error' => true
        ];
    }


    // public function selectAll($query)
    // {
    //
    // }

    public function create($data)
    {

        $filter = $this->filter->getInputFilter();

        $filter->setData($data);

        $isValid = $filter->isValid();

        if ($isValid === false) {
          return (new Response(null, $filter->getMessages()))->parse();
        }
        $role = $data['role_id'];

        unset($data['confirmPassword'], $data['role_id']);


        $bcrypt = new Bcrypt;
        $bcrypt->setCost(14);
        $data['password'] = $bcrypt->create($data['password']);
        $data['email_verified'] = 0;

        $result = $this->repository->insert($data);

        if (!$result) {
            return (new Response)->parse();
        }

        $response = $this->userRole->create(
            [
                'user_id' => $result,
                'role_id' => $role
            ]
        );


        if (empty($response['messages'])) {
            return (new Response($result))->parse();
        }

        return (new Response(null, $response))->parse();
        //$response['id'] = $result;


    }

    public function findOneByEmail($email)
    {
      return $this->repository->findOneByEmail($email);
    }

    public function findRoleByUser($id) {
      return $this->repository->findRoleByUserId($id);
    }

    public function getBcrypt() {
        if (isset($this->bcrypt)) {
            return $this->bcrypt;
        }
        $bcrypt = new Bcrypt();
        $bcrypt->setCost(14);
        $this->bcrypt = $bcrypt;
        return $this->bcrypt;
    }

    public function setBcrypt(Bcrypt $bcrypt)
    {
        $this->bcrypt = $bcrypt;
        return $this;
    }

    public function changePassword(
        $profile,
        $oldPassword,
        $newPassword,
        $confirmPassword
    ) {

        $user = $this->findOneByEmail($profile['email']);
        $passwordHash = $user['password'];

        if (!$this->getBcrypt()->verify($oldPassword, $passwordHash)) {
            return (new Response(null, ['Wrong password']))->parse();
        }

        $filter = $this->filter->getChangePasswordFilter();
        $filter->setData([
            'password' => $newPassword,
            'confirmPassword' => $confirmPassword
        ]);

        $isValid = $filter->isValid();

        if ($isValid === false) {
          return (new Response(null, $filter->getMessages()))->parse();
        }

        $values = $filter->getValues();

        $result = $this->repository->update($profile['username'], [
            'password' => $values['password']
        ]);

        if ($result) {
          return [
            'error' => false,
            'id' => $result
          ];
        }
        return [
          'error' => true
        ];
    }
}
