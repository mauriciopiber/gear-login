<?php
namespace Gear\Login\User\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Gear\Login/User/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserRepository extends AbstractRestRepository
{
  const TABLE = ['u' => 'oauth_users'];
  const ID = 'username';
  //
  // const IGNORE = [
  //     'password'
  // ];

  const COLUMNS = [
    'username',
    'password',
    'email',
    'email_verified',
    'first_name',
    'last_name'
  ];

  const JOIN = [
      [
          'table' => ['aur' => 'acl_user_role'],
          'link' => 'aur.user_id = u.username',
          'columns' => [],
          'type' => 'left'
      ],
      [
          'table' => ['ar' => 'acl_role'],
          'link' => 'ar.role_id = aur.role_id',
          'columns' => ['role'],
          'type' => 'left'
      ]
  ];

  const FILTER = [
    'username' => 'username',
    'email' => 'email',
    'role' => 'ar.role_id'
  ];


  public function findRoleByUserId($id)
  {
    $select = $this->getSql()->select();
    $select->from(['ur' => 'acl_user_role']);
    $select->columns(['role_id', 'user_id']);
    $select->join(['r' => 'acl_role'], 'r.role_id = ur.role_id', ['role']);
    $select->where(['ur.user_id' => $id]);

    //echo $this->getSql()->getSqlstringForSqlObject($select); die ;

    $results = $this->execute($select);
    $row = $results->current();
    return [$row['role']];
  }

  public function findOneByEmail($email)
  {
    $select = $this->getSql()->select();
    $select->from(static::TABLE);

    $select = $this->selectColumn($select);
    $select = $this->selectJoin($select);

    $select->where(['email' => $email]);

    $results = $this->execute($select);
    $row = $results->current();
    $row[static::ID] = (int) $row[static::ID];
    return $row;
  }
}
