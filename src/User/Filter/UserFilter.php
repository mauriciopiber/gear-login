<?php
namespace Gear\Login\User\Filter;

use Zend\InputFilter\InputFilter;
use Gear\Rest\Filter\RestFilterInterface;
use Zend\Validator\Db\NoRecordExists;

/**
 * PHP Version 5
 *
 * @category Filter
 * @package Gear\Login/User/Filter
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class UserFilter extends InputFilter implements RestFilterInterface
{
  public function __construct($dbAdapter) {
      $this->dbAdapter = $dbAdapter;
      return $this;
  }
    /**
     * Create the Filter to Form Elements
     *
     * @return UserFilter
     *
     */
    public function getInputFilter($emailCheck = true)
    {

      $this->add([
        'name'       => 'password',
        'required'   => true,
      ]);

      $this->add([
        'name'       => 'confirmPassword',
        'required'   => true,
      ]);

      $email = [
          'name' => 'email',
          'required' => true,
      ];
      if ($emailCheck) {
          $emailValidator = new NoRecordExists(
            [
              'table'   => 'oauth_users',
              'field'   => 'email',
              'adapter' => $this->dbAdapter,
            ]
          );
          $email['validators'] = [$emailValidator];
      }

      $this->add($email);


      return $this;
    }

    public function getChangePasswordFilter()
    {
      $this->add([
        'name'       => 'password',
        'required'   => true,
      ]);

      $this->add([
        'name'       => 'confirmPassword',
        'required'   => true,
      ]);

      return $this;
    }

    public function getRegisterFilter()
    {
        return $this;
    }
}
