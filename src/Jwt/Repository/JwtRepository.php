<?php
namespace Gear\Login\Jwt\Repository;

use Gear\Rest\Repository\AbstractRestRepository;

/**
 * PHP Version 5
 *
 * @category Repository
 * @package Pbr\Food/Group/Repository
 * @author Mauricio Piber <mauriciopiber@gmail.com>
 * @license GPL3-0 http://www.gnu.org/licenses/gpl-3.0.en.html
 * @link http://pibernetwork.com
 */
class JwtRepository extends AbstractRestRepository
{
    const TABLE = ['c' => 'oauth_jwt'];
    const ID = 'client_id';

    const COLUMNS = ['client_id', 'subject', 'public_key'];

    const JOIN = [];

    const FILTER = [
        'client_id' => 'client_id'
    ];

}
