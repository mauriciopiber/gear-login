<?php
namespace Gear\Login\OAuth\Controller;

use ZF\OAuth2\Controller\AuthController;
use OAuth2\Request as OAuth2Request;
use OAuth2\Response as OAuth2Response;
use OAuth2\Server as OAuth2Server;
use Zend\Http\Request as HttpRequest;

class OAuthController extends AuthController {

  /**
   * Token Action (/oauth)
   */
  public function apiTokenAction()
  {
      $server = $this->getOAuth2Server($this->params('oauth'));

      if (! $server->verifyResourceRequest($this->getOAuth2Request())) {
          $response   = $server->getResponse();
          return $this->getApiProblemResponse($response);
      }

      $server->setConfig('access_lifetime', 3600*24*356*15);
      $server->setConfig('allow_public_clients', true);
      $request  = $this->getOAuth2Request();
      $response = new OAuth2Response();

      // validate the authorize request
      $isValid = $server->validateAuthorizeRequest($request, $response);

      if (! $isValid) {
          return $this->getErrorResponse($response);
      }


      try {
        $response = $server->handleUserInfoRequest($this->getOAuth2Request());
        if ($response->getStatusCode() !== 200) {
          return $this->getApiProblemResponse($response);
        }
      } catch (\Exception $e) {
        var_dump($e);
        die();
      }

      //$isAuthorized   = ($authorized === 'yes');
      $userIdProvider = $response->getParameter('sub');

      $server->handleAuthorizeRequest(
          $request,
          $response,
          true,
          $userIdProvider
      );

      $redirect = $response->getHttpHeader('Location');

      parse_str($redirect, $tokenSet);

      foreach ($tokenSet as $i => $v) {
        if (strpos($i, 'access_token') !== false) {
          $token['access_token'] = $tokenSet[$i];
        }
      }

      $httpResponse = $this->getResponse();
      $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
      $httpResponse->setContent(
          json_encode($token)
      );
      $httpResponse->setStatusCode(200);
      return $httpResponse;

      //var_dump($token);die();

      //var_dump($redirect);die()
      if (! empty($redirect)) {
          return $this->redirect()->toUrl($redirect);
      }

      return $this->getErrorResponse($response);

  }

  public function validateAction()
  {
    $server = $this->getOAuth2Server($this->params('oauth'));

    if (! $server->verifyResourceRequest($this->getOAuth2Request())) {
        $response   = $server->getResponse();
        return $this->getApiProblemResponse($response);
    }

    $httpResponse = $this->getResponse();
    $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
    $httpResponse->setContent(
        json_encode(['isValid' => true])
    );
    $httpResponse->setStatusCode(200);
    return $httpResponse;
  }

  public function userInfoAction()
  {
    $server = $this->getOAuth2Server($this->params('oauth'));
    // Handle a request for an OAuth2.0 Access Token and send the response to the client
    if (! $server->verifyResourceRequest($this->getOAuth2Request())) {
        $response   = $server->getResponse();
        return $this->getApiProblemResponse($response);
    }

    try {
      $response = $server->handleUserInfoRequest($this->getOAuth2Request());
      if ($response->getStatusCode() !== 200) {
        return $this->getApiProblemResponse($response);
      }
    } catch (\Exception $e) {
      var_dump($e);
    }


    return $this->setHttpResponse($response);
    //var_dump($response);
  }

  public function verifyScope($scope) {
    $server = $this->getOAuth2Server($this->params('oauth'));
    // Handle a request for an OAuth2.0 Access Token and send the response to the client
    if (! $server->verifyResourceRequest($this->getOAuth2Request(), null, $scope)) {
        $response   = $server->getResponse();
        return $this->getApiProblemResponse($response);
    }
    return true;
  }

  public function readAction()
  {
    $response = $this->verifyScope('read');
    if ($response !== true) {
      return $response;
    }


    $httpResponse = $this->getResponse();
    $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
    $httpResponse->setContent(
        json_encode(['success' => true, 'message' => 'read'])
    );
    $httpResponse->setStatusCode(200);
    return $httpResponse;
  }

  public function createAction() {
    $response = $this->verifyScope('create');
    if ($response !== true) {
      return $response;
    }

    $httpResponse = $this->getResponse();
    $httpResponse->getHeaders()->addHeaders(['Content-type' => 'application/json']);
    $httpResponse->setContent(
        json_encode(['success' => true, 'message' => 'create'])
    );
    $httpResponse->setStatusCode(200);
    return $httpResponse;
  }

  /**
   * Convert the OAuth2 response to a \Zend\Http\Response
   *
   * @param $response OAuth2Response
   * @return \Zend\Http\Response
   */
  private function setHttpResponse(OAuth2Response $response)
  {
      $httpResponse = $this->getResponse();
      $httpResponse->setStatusCode($response->getStatusCode());

      $headers = $httpResponse->getHeaders();
      $headers->addHeaders($response->getHttpHeaders());
      $headers->addHeaderLine('Content-type', 'application/json');

      $httpResponse->setContent($response->getResponseBody());
      return $httpResponse;
  }

  /**
   * Retrieve the OAuth2\Server instance.
   *
   * If not already created by the composed $serverFactory, that callable
   * is invoked with the provided $type as an argument, and the value
   * returned.
   *
   * @param string $type
   * @return OAuth2Server
   * @throws RuntimeException if the factory does not return an OAuth2Server instance.
   */
  private function getOAuth2Server($type)
  {
      if ($this->server instanceof OAuth2Server) {
          return $this->server;
      }


      $server = call_user_func($this->serverFactory, $type);

      if (! $server instanceof OAuth2Server) {
          throw new RuntimeException(sprintf(
              'OAuth2\Server factory did not return a valid instance; received %s',
              (is_object($server) ? get_class($server) : gettype($server))
          ));
      }

      $this->server = $server;
      return $server;
  }

}
