<?php
return [
    'db' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=gear_login;host=gear-login-mysql',
        'driver_options' => [
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ]
    ],
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'configuration' => 'orm_default',
                'eventmanager' => 'orm_default',
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => [
                    'host' => 'gear-login-mysql',
                    'port' => '3306',
                    'dbname' => 'gear_login',
                    'charset' => 'utf8'
                ]
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'metadata_cache' => 'filesystem',
                'query_cache' => 'filesystem',
                'result_cache' => 'filesystem',
                'hydration_cache' => 'filesystem',
            ]
        ],
    ],
    'phinx' => [
        'adapter' => 'mysql',
        'host' => 'gear-login-mysql',
        'name'=> 'gear_login'
    ]
];
