<?php
return [
    'db' => [
        'username' => 'root',
        'password' => 'gear',
    ],
    'doctrine' => [
        'connection' => [
            'orm_default' => [
               'params' => [
                    'user'     => 'root',
                    'password' => 'gear',
               ],
            ]
        ]
    ],
    'phinx' => [
        'user' => 'root',
        'pass' => 'gear',
    ],
    'webhost' => 'gear-login.gear.dev',
];
