<?php
return [
  'login',
  'home',
  'logout',
  'api/user:LIST',
  'api/user:GET',
  'api/user:POST',
  'api/user:DELETE',
  'api/user:PUT',
  'oauth/authorize',
  'oauth/token',
  'oauth/user',
  'oauth/revoke',
  'oauth/api-token',
  'oauth/validate',
  'oauth',
  'register'
];
