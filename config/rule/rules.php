<?php
return [
  [
    'resource' => 'login',
    'role' => 'guest'
  ],
  [
    'resource' => 'oauth/authorize',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'oauth/token',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'oauth/api-token',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'oauth/validate',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'oauth/revoke',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'oauth/user',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'oauth',
    'role' => 'guest' /**@TODO Change to secure */
  ],
  [
    'resource' => 'logout',
    'role' => 'member'
  ],
  [
    'resource' => 'register',
    'role' => 'guest'
  ],
  [
    'resource' => 'home',
    'role' => 'member'
  ],
  [
    'resource' => 'api/user:LIST',
    'role' => 'admin'
  ]
];
