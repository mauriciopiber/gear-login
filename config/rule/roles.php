<?php
return [
  [
    'role' => 'guest',
  ],
  [
    'role' => 'member',
    'parent' => 'guest'
  ],
  [
    'role' => 'admin',
    'parent' => 'member'
  ],
  [
    'role' => 'master',
    'parent' => 'admin'
  ]
];
