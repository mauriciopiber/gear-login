<?php
namespace Gear\Login;

use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;


$db                 = require 'ext/db.config.php';
$doctrine           = require 'ext/doctrine.config.php';
$route              = require 'ext/route.config.php';
$view               = require 'ext/view.config.php';
$servicemanager     = require 'ext/servicemanager.config.php';
$controller         = require 'ext/controller.config.php';
$controllerPlugins  = require 'ext/controller.plugin.config.php';
$cache              = require 'ext/cache.config.php';

$fixture            = array(
    __NAMESPACE__.'_fixture' => __DIR__ . '/../src/Fixture'
);
$gear = [
    'modules' => [
        'gear-login' => [
            'version' => '0.1.0',
            'acl' => true,
            'name' => __NAMESPACE__,
            'git' => 'git@bitbucket.org:mauriciopiber/gear-login.git',
            'environment' => [
                'DEVELOPMENT' => '',
                'TESTING' => '',
                'STAGING' => ''
            ]
        ]
    ]
];

return [
  'session_manager' => [
     // Session validators (used for security).
     'validators' => [
         RemoteAddr::class,
         HttpUserAgent::class,
     ]
 ],
  // Session configuration.
  'session_config' => [
      // Session cookie will expire in 1 hour.
      'cookie_lifetime' => 60*60*12,
      // Session data will be stored on server maximum for 30 days.
      'gc_maxlifetime'     => 60*60*24*30,
  ],

  // Session storage configuration.
  'session_storage' => [
      'type' => SessionArrayStorage::class
  ],

  // ...
    'bjyauthorize' => [
        'acl_key' => __NAMESPACE__
    ],
    'session_config' => [
        'save_path' => __DIR__.'/../data/session'
    ],

    'navigation' => [
      'default' => []
    ],
    'gear'               => $gear,
    'db'                 => $db,
    'doctrine'           => $doctrine,
    'view_manager'       => $view,
    'controllers'        => $controller,
    'router'             => $route,
    'service_manager'    => $servicemanager,
    'controller_plugins' => $controllerPlugins,
    'zf-oauth2' => [
      'user_id' => 'username',
        'db' => [
            'dsn'      => 'mysql:dbname=gear_login;host=gear-login-mysql', // for example "mysql:dbname=oauth2_db;host=localhost"
            'username' => 'root',
            'password' => 'gear',
        ],
        'storage' => 'ZF\OAuth2\Adapter\PdoAdapter', // service name for the OAuth2 storage adapter
        /**
         * These special OAuth2Server options are parsed outside the options array
         */
        'allow_implicit'  => true, // default (set to true when you need to support browser-based or mobile apps)
        'access_lifetime' => 3600, // default (set a value in seconds for access tokens lifetime)
        'enforce_state'   => true,  // default

        /**
         * These are all OAuth2Server options with their default values
         */
        'options' => [
            'issuer' => (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null),
            'use_jwt_access_tokens'             => false,
            'store_encrypted_token_string'      => true,
            'use_openid_connect'                => true,
            'id_lifetime'                       => 3600,
            'www_realm'                         => 'Service',
            'token_param_name'                  => 'access_token',
            'token_bearer_header_name'          => 'Bearer',
            'require_exact_redirect_uri'        => true,
            'allow_credentials_in_request_body' => true,
            'allow_public_clients'              => true,
            'always_issue_new_refresh_token'    => true,
            'unset_refresh_token_after_use'     => true,
        ],
  ],
];
