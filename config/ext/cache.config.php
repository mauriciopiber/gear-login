<?php
return [
    'memcached' => [
        'adapter' => [
            'options'  => [
                'namespace'  => 'Gear\LoginMEMCACHED',
            ]
        ],
    ]
];