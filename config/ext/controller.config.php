<?php return [
    'factories' => [
        'Gear\Login\Controller\Index' => 'Gear\Login\Controller\IndexControllerFactory',
        'Gear\Login\Admin\Login\Controller\Login' => 'Gear\Login\Admin\Login\Controller\LoginControllerFactory',
        'Gear\Login\Admin\Home\Controller\Home' => 'Gear\Login\Admin\Home\Controller\HomeControllerFactory',
        'Gear\Login\Admin\Register\Controller\Register' => 'Gear\Login\Admin\Register\Controller\RegisterControllerFactory',
        'Gear\Login\Admin\RecoveryPassword\Controller\RecoveryPassword' =>
            'Gear\Login\Admin\RecoveryPassword\Controller\RecoveryPasswordControllerFactory',
        'Gear\Login\Auth\Controller\Auth' => 'Gear\Login\Auth\Controller\AuthControllerFactory',
        'ZF\OAuth2\Controller\Auth' => \Gear\Login\OAuth\Controller\OAuthControllerFactory::class,
    ]
];
