<?php
return [
    'router_class' => 'Zend\Mvc\Router\Http\TranslatorAwareTreeRouteStack',
    'routes' => [
      'home' => [
        'type' => 'regex',
        'options' => [
            'regex' => '(/|/home|/user|/role|/scope|/client|/authorization-code|/access-token|/public-keys|/jwt|/refresh-token|/profile|/profile/edit|/profile/view|/profile/change-password)',
            'spec' => '/%',
            'defaults' => [
                'action' => 'home',
                'controller' => 'Gear\Login\Admin\Home\Controller\Home',
            ],
        ],
      ],
      'register' => [
          'type'    => 'Literal',
          'options' => [
              'route'    => '/register',
              'constraints' => [
                  'id'     => '[0-9]+',
              ],
              'defaults' => [
                'controller' => 'Gear\Login\Admin\Register\Controller\Register',
                'action' => 'register',
              ],
          ],
      ],
      'registration-complete' => [
          'type'    => 'Literal',
          'options' => [
              'route'    => '/registration-complete',
              'constraints' => [
                  'id'     => '[0-9]+',
              ],
              'defaults' => [
                'controller' => 'Gear\Login\Admin\Register\Controller\Register',
                'action' => 'registration-complete',
              ],
          ],
      ],
      'recovery-password' => [
        'type' => 'literal',
        'options' => [
            'route' => '/recovery-password',
            'defaults' => [
                'action' => 'recovery-password',
                'controller' => 'Gear\Login\Admin\RecoveryPassword\Controller\RecoveryPassword',
            ],
        ],
      ],
      'login' => [
        'type' => 'literal',
        'options' => [
            'route' => '/login',
            'defaults' => [
                'action' => 'login',
                'controller' => 'Gear\Login\Admin\Login\Controller\Login',
            ],
        ],
      ],
      'logout' => [
        'type' => 'literal',
        'options' => [
            'route' => '/logout',
            'defaults' => [
                'action' => 'logout',
                'controller' => 'Gear\Login\Admin\Login\Controller\Login',
            ],
        ],
      ],
      'oauth' => [
          'type' => 'literal',
          'options' => [
              'route'    => '/oauth',
              'defaults' => [
                  'controller' => 'ZF\OAuth2\Controller\Auth',
                  'action'     => 'token',
              ],
          ],
          'may_terminate' => true,
          'child_routes' => [
              'revoke' => [
                  'type' => 'literal',
                  'options' => [
                      'route' => '/revoke',
                      'defaults' => [
                          'action' => 'revoke',
                      ],
                  ],
              ],
              'validate' => [
                  'type' => 'literal',
                  'options' => [
                      'route' => '/validate',
                      'defaults' => [
                          'action' => 'validate',
                      ],
                  ],
              ],
              'api-token' => [
                  'type' => 'literal',
                  'options' => [
                      'route' => '/api-token',
                      'defaults' => [
                          'action' => 'api-token',
                      ],
                  ],
              ],
              'user' => [
                  'type' => 'literal',
                  'options' => [
                      'route' => '/user-info',
                      'defaults' => [
                          'action' => 'userinfo',
                      ],
                  ],
              ],
              'authorize' => [
                  'type' => 'literal',
                  'options' => [
                      'route' => '/authorize',
                      'defaults' => [
                          'action' => 'authorize',
                      ],
                  ],
              ],
          ],
      ],
        'api' => [
            'type' => 'literal',
            'options' => [
                'route' => '/gear-login/api',
            ],
            'may_terminate' => true,
            'child_routes' => [
              'login' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/login[/:id]',
                      'constraints' => [
                          'id'     => '[0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Admin\Login\Controller\Login',
                      ],
                  ],
              ],
              'register' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/register[/:id]',
                      'constraints' => [
                          'id'     => '[0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Register\Controller\Register',
                      ],
                  ],
              ],
              'jwt' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/jwt[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Jwt\Controller\JwtController',
                      ],
                  ],
              ],
              'public_keys' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/public-keys[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\PublicKeys\Controller\PublicKeysController',
                      ],
                  ],
              ],
              'role' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/role[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Role\Controller\RoleController',
                      ],
                  ],
              ],
              'refresh_token' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/refresh-token[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\RefreshToken\Controller\RefreshTokenController',
                      ],
                  ],
              ],
              'client' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/client[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Client\Controller\ClientController',
                      ],
                  ],
              ],
              'access_token' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/access-token[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\AccessToken\Controller\AccessTokenController',
                      ],
                  ],
              ],
              'authorization_code' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/authorization-code[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\AuthorizationCode\Controller\AuthorizationCodeController',
                      ],
                  ],
              ],
              'client-manager' => [
                  'type'    => 'literal',
                  'options' => [
                      'route'    => '/client/manager',
                      'defaults' => [
                          'controller' => 'Gear\Login\Client\Controller\Client',
                          'action' => 'manager'
                      ],
                  ],
              ],
              'user' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/user[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\User\Controller\UserController',
                      ],
                  ],
              ],
              'user-request' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/user-request[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\UserRequest\Controller\UserRequestController',
                      ],
                  ],
              ],
              'auth' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/auth[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Auth\Controller\Auth',
                      ],
                  ],
              ],
              'change-password' => [
                  'type' => 'literal',
                  'options' => [
                      'route' => '/auth/change-password',
                      'defaults' => [
                          'controller' => 'Gear\Login\Auth\Controller\Auth',
                          'action' => 'change-password'
                      ]
                  ]
              ],
              'scope' => [
                  'type'    => 'Segment',
                  'options' => [
                      'route'    => '/scope[/:id]',
                      'constraints' => [
                          'id'     => '[a-zA-Z0-9]+',
                      ],
                      'defaults' => [
                          'controller' => 'Gear\Login\Scope\Controller\ScopeController',
                      ],
                  ],
              ],
            ]
        ]
    ]
];
