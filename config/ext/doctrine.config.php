<?php
namespace Gear\Login;

return [
    'connection' => [
        'orm_default' => [
            'configuration' => 'orm_default',
            'eventmanager' => 'orm_default',
            'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
            'params' => [
                'host' => 'mysql',
                'port' => '3306',
                'dbname' => 'gear_login',
                'user' => 'root',
                'password' => 'gear',
                'charset' => 'utf8'
            ]
        ]
    ],
    'driver' => [
        'orm_default' => [
            'drivers' => [
                __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
            ]
        ],
        __NAMESPACE__ . '_driver' => [
            'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
            'paths' => [
                __DIR__ . '/../../src/Entity'
            ]
        ]
    ]
];
