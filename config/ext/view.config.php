<?php
return [
  'display_not_found_reason' => true,
  'display_exceptions' => true,
  'doctype' => 'HTML5',
  'not_found_template' => 'error/404',
  'exception_template' => 'error/index',
    'strategies' => [
        'ViewJsonStrategy',
    ],
  'template_map' => [
    'oauth/authorize'    => __DIR__ . '/../../view/zf/auth/authorize.phtml',
    'error/index' => __DIR__ . '/../../view/error/index.phtml',
    'layout/layout' => __DIR__ . '/../../view/layout/layout.phtml',
    'layout' => __DIR__.'/../../view/layout/layout.phtml'
  ],
  'template_path_stack' => [
      __DIR__ . '/../../view',
  ],
];
