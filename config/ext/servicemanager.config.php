<?php
use Gear\Login\OAuth\Factory;

return [
    'factories' => [
        'ZF\OAuth2\Service\OAuth2Server'  => Factory\OAuth2ServerFactory::class,
        'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        'Gear\Login\Service\UserService' => 'Gear\Login\Service\UserServiceFactory',
        'Gear\Login\Repository\UserRepository' => 'Gear\Login\Repository\UserRepositoryFactory',
        'Gear\Login\Service\AuthService' => 'Gear\Login\Service\AuthServiceFactory',
        'Gear\Login\Client\Filter\ClientFilter' => 'Gear\Login\Client\Filter\ClientFilterFactory',
        'Gear\Login\Client\Repository\ClientRepository' => 'Gear\Login\Client\Repository\ClientRepositoryFactory',
        'Gear\Login\Client\Service\ClientService' => 'Gear\Login\Client\Service\ClientServiceFactory',
        'Gear\Login\User\Filter\UserFilter' => 'Gear\Login\User\Filter\UserFilterFactory',
        'Gear\Login\User\Repository\UserRepository' => 'Gear\Login\User\Repository\UserRepositoryFactory',
        'Gear\Login\User\Service\UserService' => 'Gear\Login\User\Service\UserServiceFactory',
        'Gear\Login\Admin\Login\Service\LoginService' => 'Gear\Login\Admin\Login\Service\LoginServiceFactory',
        'Zend\Authentication\AuthenticationService' => 'Gear\Login\Auth\Service\AuthenticationServiceFactory',
        'Gear\Login\Auth\Adapter\AuthAdapter' => 'Gear\Login\Auth\Adapter\AuthAdapterFactory',
        'Gear\Login\Scope\Filter\ScopeFilter' => 'Gear\Login\Scope\Filter\ScopeFilterFactory',
        'Gear\Login\Scope\Repository\ScopeRepository' => 'Gear\Login\Scope\Repository\ScopeRepositoryFactory',
        'Gear\Login\Scope\Service\ScopeService' => 'Gear\Login\Scope\Service\ScopeServiceFactory',
        'Gear\Login\Admin\Login\Filter\LoginFilter' => 'Gear\Login\Admin\Login\Filter\LoginFilterFactory',
        'Gear\Login\Admin\Login\Form\LoginForm' => 'Gear\Login\Admin\Login\Form\LoginFormFactory',
        'Gear\Login\Admin\Register\Filter\RegisterFilter' => 'Gear\Login\Admin\Register\Filter\RegisterFilterFactory',
        'Gear\Login\Admin\Register\Form\RegisterForm' => 'Gear\Login\Admin\Register\Form\RegisterFormFactory',
        'Gear\Login\Admin\Register\Service\RegisterService' => 'Gear\Login\Admin\Register\Service\RegisterServiceFactory',
        'Gear\Login\Email\EmailService' => 'Gear\Login\Email\EmailServiceFactory',
        'Gear\Login\Auth\Acl\AclGuard' => 'Gear\Login\Auth\Acl\AclGuardFactory',
        'Gear\Login\Auth\Acl\AclAuth' => 'Gear\Login\Auth\Acl\AclAuthFactory',

    ],
    'invokables' => [
        'Gear\Login\Auth\Strategy\UnAuthorizedStrategy' => 'Gear\Login\Auth\Strategy\UnAuthorizedStrategy',
        'Gear\Login\Filter\RegisterFilter' => 'Gear\Login\Filter\RegisterFilter',
        'Gear\Login\Filter\LoginFilter' => 'Gear\Login\Filter\LoginFilter'
    ],
    'aliases' => [
        'translator' => 'MvcTranslator'
    ]
];
