<?php
return [
  'modules' =>
  [
      'ZF\ApiProblem',
      'ZF\ContentNegotiation',
      'ZF\OAuth2',
      'Zend\Router',
      'Zend\I18n',
      'Zend\Form',
      'Zend\Mvc\Plugin\Prg',
      'Zend\Session',
      'Gear\Rest',
      'GearEmail',
      'Gear\Login'
  ],
  'module_listener_options' =>
  [
    'module_paths' =>
    [
        '../.',
        './vendor',
    ],
    'config_glob_paths' =>
    [
        'config/autoload/{,*.}{global,local}.php',
    ],
  ],
]; ?>
